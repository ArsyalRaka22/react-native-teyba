import React, { useEffect } from "react"
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '@react-native-community/async-storage';
import SideMenu from "./SideMenu";
import SplashScreen from "../screens/SplashScreen";
import Home from "../screens/Home";
import { useDispatch, useSelector } from "react-redux";
import SignIn from "../screens/SignIn";
import Verifikasi from "../screens/Verifikasi";
import ProfileDetail from "../screens/ProfileDetail";
import Agen from "../screens/Agen";
import AgenStore from "../screens/AgenStore";
import PesanProduk from "../screens/PesanProduk";
import Checkout from "../screens/Checkout";
import DetailProduk from "../screens/DetailProduk";
import Pembayaran from "../screens/Pembayaran";
import Transaksi from "../screens/Transaksi";
import DaftarPesanan from "../screens/DaftarPesanan";
import DetailPesanan from "../screens/DetailPesanan";
import Profile from "../screens/Profile";
import KartuAnggota from "../screens/KartuAnggota";
import KelolaProduk from "../screens/KelolaProduk";
import Register from "../screens/Register";
import Address from "../screens/Address";
import AddAddress from "../screens/AddAddress";
import DetailTransaksi from "../screens/DetailTransaksi";
import HistoryPoint from "../screens/HistoryPoint";
import KelolaToko from "../screens/KelolaToko";
import SettingPembayaran from "../screens/SettingPembayaran";
import SettingPembayaranDetail from "../screens/SettingPembayaranDetail";
import ProfileToko from "../screens/ProfileToko";
import ReferralAgen from "../screens/ReferralAgen";
import CariProduk from "../screens/CariProduk";
import BannerDetail from "../screens/BannerDetail";
import SettingPengiriman from "../screens/SettingPengiriman";

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

// const HomeNavigator = () => {
//     return (
//         <Drawer.Navigator initialRouteName="Home" drawerContent={(props) => {
//             return <SideMenu {...props} />
//         }}>
//             <Drawer.Screen name="Home" component={Home} options={() => ({ headerShown: false })} />
//         </Drawer.Navigator>
//     );
// }

// const cardInterpolatorOptions = {
//     cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
// };

const HomeStackComponent = () => {
    return (
        <Stack.Navigator initialRouteName="HomeStack" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="HomeStack" component={Home} />
            <Stack.Screen name="DetailProduk" component={DetailProduk} />
            <Stack.Screen name="BannerDetail" component={BannerDetail} />
        </Stack.Navigator>
    );
}

const ProfileStack = () => {
    return (
        <Stack.Navigator initialRouteName="Profile" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Profile" component={Profile} />
            <Stack.Screen name="ProfileDetail" component={ProfileDetail} />
            <Stack.Screen name="KartuAnggota" component={KartuAnggota} />
            <Stack.Screen name="HistoryPoint" component={HistoryPoint} />
            <Stack.Screen name="ReferralAgen" component={ReferralAgen} />

            <Stack.Screen name="KelolaToko" component={KelolaToko} />
            <Stack.Screen name="KelolaProduk" component={KelolaProduk} />
            <Stack.Screen name="DaftarPesanan" component={DaftarPesanan} />
            <Stack.Screen name="SettingPembayaran" component={SettingPembayaran} />
            <Stack.Screen name="SettingPembayaranDetail" component={SettingPembayaranDetail} />
            <Stack.Screen name="DetailPesanan" component={DetailPesanan} />
            <Stack.Screen name="ProfileToko" component={ProfileToko} />
            <Stack.Screen name="SettingPengiriman" component={SettingPengiriman} />

        </Stack.Navigator>
    )
}

const AgenStack = () => {
    return (
        <Stack.Navigator initialRouteName="Agen" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Agen" component={Agen} />
            <Stack.Screen name="AgenStore" component={AgenStore} />
            <Stack.Screen name="PesanProduk" component={PesanProduk} />
            <Stack.Screen name="Checkout" component={Checkout} />
            <Stack.Screen name="Pembayaran" component={Pembayaran} />
            <Stack.Screen name="Address" component={Address} />
            <Stack.Screen name="AddAddress" component={AddAddress} />
            <Stack.Screen name="DetailProduk" component={DetailProduk} />
        </Stack.Navigator>
    )
}

const TransaksiStack = () => {
    return (
        <Stack.Navigator initialRouteName="Transaksi" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Transaksi" component={Transaksi} />
            <Stack.Screen name="DaftarPesanan" component={DaftarPesanan} />
            <Stack.Screen name="DetailPesanan" component={DetailPesanan} />
            <Stack.Screen name="DetailTransaksi" component={DetailTransaksi} />
            <Stack.Screen name="Pembayaran" component={Pembayaran} />
        </Stack.Navigator>
    )
}

const DashboardTab = () => {
    return (
        <Tab.Navigator tabBar={() => null}>
            <Tab.Screen name="Home" component={HomeStackComponent} options={() => ({ headerShown: false })} />
            <Tab.Screen name="ProfileStack" component={ProfileStack} options={() => ({ headerShown: false })} />
            <Tab.Screen name="AgenStack" component={AgenStack} options={() => ({ headerShown: false })} />
            <Tab.Screen name="TransaksiStack" component={TransaksiStack} options={() => ({ headerShown: false })} />
        </Tab.Navigator>
    )
}

const Auth = () => {
    return (
        <Stack.Navigator initialRouteName="SignInStack" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="SignIn" component={SignIn} />
            <Stack.Screen name="Verifikasi" component={Verifikasi} />
            <Stack.Screen name="Register" component={Register} />
        </Stack.Navigator>
    )
}

export default function AppContainer() {
    const user = useSelector((state) => state.user);
    const splash = useSelector((state) => state.splash);

    //console.log("SplashScreen", splash);

    useEffect(() => {
        console.log("User changed", user);

        AsyncStorage.getAllKeys().then((res) => {
            console.log('res', res)
        })

        AsyncStorage.getItem("persist:primary").then((res) => {
            console.log("res",res)
        })
    }, [user]);

    // useEffect(() => {
    //     console.log("splash changed", splash);
    // }, [splash]);

    if (splash === true) {
        return <SplashScreen />
    } else {

        if (user) {
            return (
                <NavigationContainer>
                    <Tab.Navigator tabBar={() => null}>
                        {/* <Tab.Screen name="Auth" component={Auth} options={() => ({ headerShown: false })} /> */}
                        <Tab.Screen name="Dashboard" component={DashboardTab} options={() => ({ headerShown: false })} />
                        <Tab.Screen name="CariProduk" component={CariProduk} options={() => ({ headerShown: false })} />
                    </Tab.Navigator>
                </NavigationContainer>
            )
        } else {
            return (
                <NavigationContainer>
                    <Tab.Navigator tabBar={() => null}>
                        <Tab.Screen name="Home" component={HomeStackComponent} options={() => ({ headerShown: false })} />
                        <Tab.Screen name="Auth" component={Auth} options={() => ({ headerShown: false })} />
                        <Tab.Screen name="CariProduk" component={CariProduk} options={() => ({ headerShown: false })} />
                        {/* <Tab.Screen name="Dashboard" component={DashboardTab} options={() => ({ headerShown: false })} /> */}
                    </Tab.Navigator>
                </NavigationContainer>
            )
        }
    }
}