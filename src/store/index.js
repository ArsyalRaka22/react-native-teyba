import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistCombineReducers } from 'redux-persist';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '@react-native-community/async-storage';
import { user, credential, splash, counter, cartRedux, agenRedux } from './reducers';

const config = {
    key: 'primary',
    storage: AsyncStorage,
    whitelist: ['user', 'credential', 'counter'],
};

const store = createStore(
    persistCombineReducers(config, { user, credential, splash,counter, cartRedux, agenRedux }),
    undefined,
    compose(applyMiddleware(thunk)),
);

export { store };