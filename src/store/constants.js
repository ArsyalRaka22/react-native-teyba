export const SET_USER = 'SET_USER';
export const SET_CREDENTIAL = 'SET_CREDENTIAL';
export const SET_SPLASH = 'SET_SPLASH';
export const SET_COUNTER = 'SET_COUNTER';
export const SET_CART = 'SET_CART';
export const SET_AGEN = 'SET_AGEN';
