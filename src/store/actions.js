import * as types from "./constants"

export const setUser = (payload) => (dispatch) => {
    dispatch({
        type: types.SET_USER,
        payload,
    });
};
export const setCredential = (payload) => (dispatch) => {
    dispatch({ type: types.SET_CREDENTIAL, payload });
};

export const setSplash = (payload) => (dispatch) => {
    dispatch({ type: types.SET_SPLASH, payload });
};
export const setCounter = (payload) => (dispatch) => {
    dispatch({ type: types.SET_COUNTER, payload });
};
export const setCartRedux = (payload) => (dispatch) => {
    dispatch({ type: types.SET_CART, payload });
};
export const setAgenRedux = (payload) => (dispatch) => {
    dispatch({ type: types.SET_AGEN, payload });
};