import * as types from "./constants.js"

const initialCredential = {
    email: '',
    password: '',
};

export function user(state = null, action) {
    if (action.type == types.SET_USER) {
        //console.log("reducer", action);
        return action.payload;
    }

    return state;
}
export function credential(state = initialCredential, action) {
    if (action.type == types.SET_CREDENTIAL) {
        return action.payload;
    }

    return state;
}
export function splash(state = true, action) {
    if (action.type == types.SET_SPLASH) {
        return action.payload;
    }

    return state;
}
export function counter(state = 0, action) {
    if (action.type == types.SET_COUNTER) {
        return action.payload;
    }
    return state;
}
export function cartRedux(state = [], action) {
    if (action.type == types.SET_CART) {
        return action.payload;
    }
    return state;
}
export function agenRedux(state = [], action) {
    if (action.type == types.SET_AGEN) {
        return action.payload;
    }
    return state;
}