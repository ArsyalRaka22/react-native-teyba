import axios from "axios";
import qs from 'qs';
import { Platform } from "react-native";
// import AppConfig from "../config/app";
import { store } from "../store";
import moment from 'moment';
import FormData from "form-data";
import AppConfig from "./AppConfig";

const request = () => {
	return axios.create({
		baseURL: AppConfig.BASE_URL,
		timeout: 10000,
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
		}
	});
}

const requestWithAuth = () => {
	let user = store.getState().user;

	console.log("Auth", user?.token);

	return axios.create({
		baseURL: AppConfig.BASE_URL,
		timeout: 5000,
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": "Token " + user?.token
		}
	});
}

export const HttpRequest = {
	registerDevice(name, token, device_id, user_id) {
		let data = {
			"name": name + "",
			"active": true,
			"device_id": device_id + "",
			"registration_id": token,
			"type": Platform.OS,
			"user": user_id
		};
		console.log("registerDevice", data)
		return requestWithAuth().post("/device-user/", data);
	},
	register(data) {
		return request().post("/v1/auth/register", data)
	},
	connection() {
		return request().get("/v1/banner/index")
	},
	signup(data) {
		return request().post("/api/v1/signup/", data);
	},
	banner() {
		return request().get("/v1/banner/index")
	},
	getProdukPusat() {
		return request().get("/v1/produk/produk-pusat")
	},
	login(phone) {
		return request().post("/v1/auth/login", {
			phone
		})
	},
	verifikasiOtp(phone, otp) {
		return request().post("/v1/auth/verifikasi-otp", {
			phone, otp
		})
	},
	agenList() {
		return request().get("/v1/agen/list")
	},
	listKota(q) {
		return request().post("/v1/address/list-kota", {
			q
		})
	},
	propinsi() {
		return request().get("/v1/address/propinsi")
	},
	kabupaten(propinsi_id) {
		console.log(propinsi_id)
		return request().post("/v1/address/kabupaten", {
			propinsi_id: propinsi_id
		})
	},
	kecamatan(kabupaten_id) {
		return request().post("/v1/address/kecamatan", {
			kabupaten_id
		})
	},
	desa(kecamatan_id) {
		return request().post("/v1/address/desa", {
			kecamatan_id
		})
	},
	profileSave(data) {
		return requestWithAuth().post("/v1/user/profile-save", data)
	},
	profileDetail() {
		return requestWithAuth().get("/v1/user/profile-detail")
	},
	me() {
		return requestWithAuth().get("/v1/user/me")
	},
	getAgenPusat() {
		return requestWithAuth().get("/v1/agen/pusat")
	},
	simpanKatalogProduk(data) {
		return requestWithAuth().post("/v1/produk/toggle-katalog-produk", data)
	},
	getKatalogAgen() {
		return requestWithAuth().get("/v1/produk/katalog-agen")
	},
	getProdukAgen(data) {
		return requestWithAuth().post("/v1/produk/produk-agen", data)
	},
	getAddressList() {
		return requestWithAuth().get("/v1/address/list")
	},
	saveAddress(data) {
		return requestWithAuth().post("/v1/address/simpan", data)
	},
	setPrimaryAddress(data) {
		return requestWithAuth().post("/v1/address/set-primary", data)
	},
	deleteAddress(data) {
		return requestWithAuth().post("/v1/address/delete", data)
	},
	getMetodePembayaran(data) {
		return requestWithAuth().post("/v1/transaksi/list-metode-pembayaran", data)
	},
	checkout(data) {
		return requestWithAuth().post("/v1/transaksi/checkout", data)
	},
	cekOngkir(data) {
		return requestWithAuth().post("/v1/transaksi/cek-ongkir", data)
	},
	getStatusTransaksi(data) {
		return requestWithAuth().post("/v1/transaksi/status-transaksi", data)
	},
	getRiwayatTransaksi(data) {
		return requestWithAuth().post("/v1/transaksi/riwayat-transaksi", data)
	},
	getDetailTransaksi(data) {
		return requestWithAuth().post("/v1/transaksi/detail-transaksi", data)
	},
	konfirmasiPembayaran(data) {
		return requestWithAuth().post("/v1/pembayaran/upload-bukti-transfer", data)
	},
	konfirmasiTerimaPesanan(data) {
		return requestWithAuth().post("/v1/transaksi/selesai", data)
	},
	historyPoint(data) {
		return requestWithAuth().post("/v1/point/history", data)
	},
	daftarPembayaranSaya() {
		return requestWithAuth().post("/v1/pembayaran/daftar-pembayaran-saya")
	},
	simpanBankPembayaran(data) {
		return requestWithAuth().post("/v1/pembayaran/simpan-bank-pembayaran", data)
	},
	getProdukKategori() { 
		return requestWithAuth().post("/v1/home/kategori-produk")
	},
	getProdukByKategori(data) { 
		return requestWithAuth().post("/v1/home/produk-by-kategori", data)
	},
	updateProfileToko(data) { 
		return requestWithAuth().post("/v1/agen/update-profile-toko", data)
	},
	getProfileToko() { 
		return requestWithAuth().post("/v1/agen/profile-toko")
	},
	terimaPesananAgen(data) { 
		return requestWithAuth().post("/v1/transaksi/terima-pesanan-agen", data)
	},
	terimaPembayaranAgen(data) { 
		return requestWithAuth().post("/v1/transaksi/terima-pembayaran-agen", data)
	},
	batalkanPesanan(data) {
		return requestWithAuth().post("/v1/transaksi/batalkan-pesanan", data)
	},
	tolakPesanan(data) {
		return requestWithAuth().post("/v1/transaksi/tolak-pesanan", data)
	},
	getReferral() { 
		return requestWithAuth().post("/v1/agen/data-referral")
	},
	pengirimanList() { 
		return requestWithAuth().get("/v1/pengiriman/user-courier")
	},
	simpanPengirimanUser(data) { 
		return requestWithAuth().post("/v1/pengiriman/user-courier-simpan", data)
	},

	uploadFile(data) {
		// let form = new FormData()
		// form.append("file", data)
		// form.append("path", "pembayaran")
		let form = FormDataConverter.convert(data)
		return requestWithAuth().post("/v1/utils/upload-file", form)
	},

	uploadImage(data) { 
		return requestWithAuth().post("/v1/utils/upload-image", data)
	}
};

export const FormDataConverter = {
	convert(data) {
		let form_data = new FormData();

		for (let key in data) {
			form_data.append(key, data[key]);
		}

		return form_data;
	}
};

export const HttpResponse = {
	processMessage(msg, alternateMessage = "Error processing data") {
		if (msg) {
			let data = msg.data;
			let messages = [];
			Object.keys(data).forEach((key) => {
				let arr = data[key];
				if (Array.isArray(arr)) {
					messages.push(key + " - " + arr.join(" "));
				} else {
					messages.push(key + " - " + arr);
				}
			});
			if (messages.length == 0) {
				return alternateMessage;
			}
			return messages.join(" ");
		}
		return alternateMessage;
	}
};