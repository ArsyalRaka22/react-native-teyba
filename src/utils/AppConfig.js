import color from "./color";

export default {
    // BASE_URL: "http://192.168.5.1/yii2-teyba-apps/api/web",
    BASE_URL: "https://api.teyba.aplikasitrial.com/",
    DEBUG_MODE: true,
    font: {
        size: {
            mini : 10,
            small: 12,
            default: 14,
            medium: 16,
            large: 18,
            exLarge: 20
        },
        family: {
            regular: "Montserrat Regular",
            medium: "Montserrat Medium",
            bold: "Montserrat Bold",
            semiBold: "Montserrat SemiBold",
            thin: "Montserrat Thin"
        }
    },
    pesanan: {
        BELUM_DIBAYAR_IMG: require("../assets/status-pesanan/belum-dibayar.png"),
        DITERIMA: require("../assets/status-pesanan/diterima.png"),
        PESANAN_DIPROSES: require("../assets/status-pesanan/pesanan-diproses.png"),
        PESANAN_MASUK: require("../assets/status-pesanan/pesanan-masuk.png"),
        SELESAI: require("../assets/status-pesanan/selesai.png"),
        SUDAH_DIBAYAR : require("../assets/status-pesanan/sudah-dibayar.png")
    },
    BACKGROUND_IMG: require("../assets/background.png"),
    USER_IMG: require("../assets/user.png"),

    PROFILE_USER: require("../assets/icon/profile-user.png"),
    PROFILE_PESANAN: require("../assets/icon/profile-pesanan.png"),
    PROFILE_KELOLA_PRODUK: require("../assets/icon/profile-kelola-produk.png"),
    PROFILE_KODE_REFERRAL: require("../assets/icon/profile-kode-referral.png"),
    PROFILE_JUMLAH_POINT: require("../assets/icon/profile-jumlah-point.png"),
    PROFILE_HISTORY_POINT: require("../assets/icon/profile-history-point.png"),
    PROFILE_KARTU_ANGGOTA: require("../assets/icon/profile-kartu-anggota.png"),
    PROFILE_LOGOUT: require("../assets/icon/profile-logout.png"),
    
    KARTU_ANGGOTA_IMG: require("../assets/kartu-anggota.png"),
    
    CHECKBOX_IMG : require("../assets/icon/checkbox.png"),
    CHECKBOX_CHECKED_IMG : require("../assets/icon/checkbox-checked.png"),
}