/**
 * 
 *   Kabuli : #733c3c
 *   Biryani : #107d67
 *   Bukhori : #df5126
 *   Mandhi : #1e3362
 * 
 */

export default {
    // primary: "#8F00FF",
    primary: "#107d67",
    white: "#FFFFFF",
    black: "#000000",
    gray: "#E0E0E0",
    grayText: "#9E9E9E",
    danger: "#e74c3c",
    header: "#2E2E30",
    border: '#ccc',
    text: "#000000",
    blackOpacity: "rgba(0,0,0,0.7)",
    green: "#00C369"
}