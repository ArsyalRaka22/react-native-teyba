import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, Image, Animated } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';
import Timer from 'react-native-timer';
import LinearGradient from 'react-native-linear-gradient';

const SCREEN_WIDTH = Dimensions.get("window").width;
const PER_ITEM_MARGIN = 10;
const PEEK_MARGIN = 15;

export default class BannerSlider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            banner: [],
            currentSliderIndex: 0,
        };
    }

    componentDidMount() {
        this.getData()
        Timer.setInterval("slider", () => {
            let { banner, currentSliderIndex } = this.state;
            if (banner.length != 0) {
                this.setState({ currentSliderIndex: (currentSliderIndex + 1) % banner.length }, () => {
                    const offsets = banner.map((item, index) => {
                        if (index == 0) {
                            return 0;
                        }
                        return index * (SCREEN_WIDTH - PEEK_MARGIN * 2 - PER_ITEM_MARGIN);
                    });

                    if (this.sliderRef) {
                        this.sliderRef.scrollTo({ x: offsets[this.state.currentSliderIndex], animated: true });
                    }
                });
            }
        }, 3000);
    }

    componentWillUnmount() {
        Timer.clearInterval("slider");
    }

    async getData() {
        try {
            let res = await HttpRequest.banner()
            if (res.data.data) {
                this.setState({
                    banner: res.data.data
                })
            }
        } catch (error) {
            console.log('err', error, error.response)
        }
    }

    render() {
        const { navigation } = this.props;
        let { banner } = this.state
        const offsets = banner.map((item, index) => {
            if (index == 0) {
                return 0;
            }
            return index * (SCREEN_WIDTH - PEEK_MARGIN * 2 - PER_ITEM_MARGIN);
        });
        return (
            <View style={styles.main}>
                {AppConfig.DEBUG_MODE == true && (
                    banner.length > 0 && (
                        <>
                            <ScrollView
                                ref={(ref) => {
                                    this.sliderRef = ref;
                                }}
                                horizontal={true}
                                pagingEnabled={true}
                                snapToOffsets={offsets}
                                showsHorizontalScrollIndicator={false}
                                decelerationRate='fast'
                            >
                                {
                                    banner.map((item, index) => {
                                        const bannerHeight = (SCREEN_WIDTH - (2 * PEEK_MARGIN + 2 * PER_ITEM_MARGIN)) * 600 / 1024;
                                        return (
                                            <>
                                                {index == 0 && <View style={{ width: PEEK_MARGIN }} />}
                                                <View style={{ width: PER_ITEM_MARGIN }} />
                                                <TouchableOpacity key={index} style={{ width: SCREEN_WIDTH - (2 * PEEK_MARGIN + 2 * PER_ITEM_MARGIN) }}
                                                    activeOpacity={0.3}
                                                    onPress={() => {
                                                        navigation.navigate("BannerDetail", { item })
                                                    }}>
                                                    <Image source={{ uri: item.image }} style={{ width: '100%', height: bannerHeight, borderRadius: 10 }} resizeMode="cover" />
                                                    <LinearGradient colors={['#00000000', '#00000000', '#000000FF']} style={styles.sliderGradient}>
                                                        <Text style={styles.sliderText}>{item.judul}</Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                                {index == banner.length - 1 && <View style={{ width: PER_ITEM_MARGIN }} />}
                                                {index == banner.length - 1 && <View style={{ width: PEEK_MARGIN }} />}
                                            </>
                                        );
                                    })
                                }
                            </ScrollView>
                        </>
                    )
                )}
            </View >
        )
    }
}



const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
    main: { marginTop: 16, marginBottom: 10 },
    container: { flex: 1, backgroundColor: 'white' },
    child: { width, justifyContent: 'center' },
    text: { fontSize: 12, textAlign: 'center' },
    sliderGradient: { position: 'absolute', width: "100%", height: "100%", borderRadius: 10, justifyContent: "flex-end", paddingHorizontal: 20, paddingVertical: 15 },
    sliderText: { fontSize: 17, color: color.white, },
});