import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

function ProdukDefault(props) {
    let item = props.item
    return (
        <TouchableOpacity activeOpacity={0.6} {...props} style={styles.container}>
            <Image style={styles.image} source={{ uri: item.gambar }} />
            {item?.tipe == "karton" && (
                <Text style={styles.tipe}>{item.tipe}</Text>
            )}
            <View style={{ padding: 10 }}>
                <Text style={styles.title}>{item.nama.substring(0, 80)}...</Text>
                {/* <Text style={styles.description}>{item.deskripsi_pendek}</Text> */}
            </View>
        </TouchableOpacity>
    )
}

const RADIUS = 12
const styles = StyleSheet.create({
    container: {
        backgroundColor: color.white,
        elevation: 2,
        borderRadius: RADIUS,
    },
    image: {
        width: '100%',
        height: 200,
        borderTopRightRadius: RADIUS,
        borderTopLeftRadius : RADIUS
    },
    title: {
        fontSize: AppConfig.font.size.default,
        fontFamily : AppConfig.font.family.semiBold,
        color: color.black,
    },
    description: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        marginTop : 5
    },
    tipe: {
        backgroundColor: color.green,
        color: color.white,
        fontFamily: AppConfig.font.family.semiBold,
        textAlign: 'center',
        paddingHorizontal: 8,
        paddingVertical: 2,
        borderRadius: 5,
        textAlignVertical: 'center',
        position: 'absolute', right: 5, top: 5
    }
})

export default React.memo(ProdukDefault)