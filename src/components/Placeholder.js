import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, FlatList } from 'react-native';
import Skeleton from 'skeleton-rn';

const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},]

const Home = () => {
    return (
        <Skeleton />
    )
}

const HistoryPoint = () => {
    const height = 15
    return (
        data.map((i, point) => {
            return (
                <View key={point} style={{ marginVertical: 5 }}>
                    <View style={{ alignItems: 'flex-end', marginBottom: 10 }}>
                        <Skeleton style={{ height: height, width: 70, marginBottom: 5, borderRadius: 3 }} />
                    </View>
                    <Skeleton style={{ height: height, width: 120, marginBottom: 5, borderRadius: 3 }} />
                    <Skeleton style={{ height: height, borderRadius: 3 }} />

                    <Skeleton style={{ height: 3, marginTop: 20 }} />
                </View>
            )
        })
    )
}

const Transaksi = () => {
    const height = 15
    return (
        data.map((i, transaksi) => {
            return (
                <View key={transaksi} style={{ marginVertical: 5 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Skeleton style={{ height: 80, width: 80, borderRadius: 5, }} />
                        <View style={{ flex: 1, marginLeft: 10 }}>
                            <Skeleton style={{ height: height, width: 120, marginBottom: 5, borderRadius: 3 }} />
                            <Skeleton style={{ height: height, borderRadius: 3, marginBottom: 5, }} />
                            <Skeleton style={{ height: height, width: 70, borderRadius: 3 }} />
                        </View>
                    </View>

                    <Skeleton style={{ height: 3, marginTop: 20 }} />
                </View>
            )
        })
    )
}

const ReferralAgen = () => {
    const height = 15
    return (
        data.map((i, refAgen) => {
            return (
                <View key={refAgen} style={{ marginVertical: 5 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Skeleton style={{ height: 60, width: 60, borderRadius: 100, }} />
                        <View style={{ flex: 1, marginLeft: 10 }}>
                            <Skeleton style={{ height: height, width: 120, marginBottom: 5, borderRadius: 3 }} />
                            <Skeleton style={{ height: height, borderRadius: 3 }} />
                        </View>
                    </View>

                    <Skeleton style={{ height: 3, marginTop: 20 }} />
                </View>
            )
        })
    )
}

const Agen = () => {
    return (
        data.map((i, agent) => {
            return (
                <View key={agent}>
                    <Skeleton style={{ height: 120, borderRadius: 10, marginVertical: 5 }} />
                </View>
            )
        })
    )
}

const KategoriProduk = () => {
    return (
        <ScrollView horizontal={true}>
            {data.map((i, kategori) => {
                return (
                    <View key={kategori}>
                        <Skeleton style={{ height: 25, padding: 10, width: 80, borderRadius: 3, marginRight: 10 }} />
                    </View>
                )
            })}
        </ScrollView>
    )
}

const DaftarProduk = () => {
    return (
        <ScrollView horizontal={true}>
            {data.map((i, produk) => {
                return (
                    <View key={produk}>
                        <Skeleton style={{ height: 200, flex: 1, margin: 6, borderRadius: 10 }} />
                    </View>
                )
            })}
        </ScrollView>
    )
}

const DaftarPesanan = () => {
    return (
        data.map((i, pesanan) => {
            return (
                <View key={pesanan}>
                    <Skeleton style={{ height: 15, width: 90, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, width: 120, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, flex: 1, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, width: 160, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, width: 200, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 3, marginVertical: 20 }} />
                </View>
            )
        })
    )
}

const SettingPembayaran = () => {
    return (
        data.map((i, key) => {
            return (
                <Skeleton key={key} style={{ height: 120, borderRadius: 8, margin: 6 }} />
            )
        })
    )
}

const DaftarAlamat = () => {
    return (
        data.map((i, key) => {
            return (
                <View key={key}>
                    <Skeleton style={{ height: 15, width: 90, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, width: 120, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, flex: 1, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 15, width: 160, marginTop: 8, borderRadius: 3 }} />
                    <Skeleton style={{ height: 3, marginVertical: 20 }} />
                </View>
            )
        })
    )
}


export default {
    Home,
    HistoryPoint,
    ReferralAgen,
    Transaksi,
    Agen,
    KategoriProduk,
    DaftarProduk,
    DaftarPesanan,
    SettingPembayaran,
    DaftarAlamat
}