import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

function TabHeader(props) {

    // let selected = props.selected
    let items = props.items ?? []

    const onChangeSelected = (data) => {
        if (props.onChangeSelected) {
            props.onChangeSelected(data)
        }
    }
    return (
        <View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {items.map((item, index) => {
                    if (props?.selected?.label == item.label) {
                        return (
                            <TouchableOpacity activeOpacity={0.6} onPress={() => {
                                onChangeSelected(item)
                            }} key={index}>
                                <Text style={styles.itemActive}>{item.label}</Text>
                            </TouchableOpacity>
                        )
                    } else {
                        return (
                            <TouchableOpacity activeOpacity={0.6} onPress={() => {
                                onChangeSelected(item)
                            }} key={index}>
                                <Text style={styles.item}>{item.label}</Text>
                            </TouchableOpacity>
                        )
                    }
                })}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {

    },
    item: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 7,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.grayText,
        flex : 1,
        marginRight: 8
    },
    itemActive: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: color.gray,
        borderRadius: 7,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.primary,
        flex : 1,
        marginRight: 8
    }
})

export default React.memo(TabHeader)