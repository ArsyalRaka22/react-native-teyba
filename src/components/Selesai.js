import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Modal, Image, Dimensions } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

function SimpleModal(props) {

    const [visible, setVisible] = useState(props.visible ?? false)
    return (
        <Modal
            animationType='slide'
            visible={visible}
            transparent={true}
            {...props}
        >
            <TouchableOpacity style={styles.main} activeOpacity={0.8} onPress={props.onRequestClose}>
                {props.children}
            </TouchableOpacity>
        </Modal>
    )
}

function SelesaiPemesanan(props) {
    return (
        <SimpleModal {...props}>
            <View style={styles.content}>
                <Image resizeMode='contain' source={require('../assets/store.png')} style={styles.image} />
                <Text style={styles.title}>TERIMA KASIH SUDAH MEMESAN!</Text>
                <Text style={styles.description}>Admin akan memeriksa pesanan dan akan menentukan biaya pengiriman. Silahkan lihat riwayat transaksi aktif.</Text>

                <View style={{ marginTop: 20 }}>
                    <TouchableOpacity activeOpacity={0.6}>
                        <Text style={styles.button}>Lihat Transaksi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.6}>
                        <Text style={styles.buttonOutline}>Belanja Lagi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SimpleModal>
    )
}

function SelesaiPembayaran(props) {
    return (
        <SimpleModal {...props}>
            <View style={styles.content}>
                <Image resizeMode='contain' source={require('../assets/wallet.png')} style={styles.image} />
                <Text style={styles.title}>TERIMA KASIH SUDAH MEMBAYAR!</Text>
                <Text style={styles.description}>Admin akan memeriksa pembayaran anda. Silahkan lihat riwayat transaksi aktif.</Text>

                <View style={{ marginTop: 20 }}>
                    <TouchableOpacity activeOpacity={0.6}>
                        <Text style={styles.button}>Lihat Transaksi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.6}>
                        <Text style={styles.buttonOutline}>Belanja Lagi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SimpleModal>
    )
}

function ModalDefault(props) {
    return (
        <SimpleModal {...props}>
            <View style={[styles.contentDefault, props.contentStyle]}>
                {props.children}
            </View>
        </SimpleModal>
    )
}

const { width, height } = Dimensions.get("screen")
const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.7)",
        // alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        backgroundColor: color.white,
        margin: 20,
        borderRadius: 8,
        padding: 20,
        alignItems: 'center'
    },
    contentDefault: {
        backgroundColor: color.white,
        margin: 20,
        borderRadius: 8,
        padding: 20,
        // alignItems: 'center'
    },
    image: {
        width: 120,
        height: 120,
        marginBottom: 20
    },
    title: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        textAlign: 'center'
    },
    description: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        textAlign: 'center',
        paddingHorizontal: 60,
        marginTop: 5
    },
    button: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 10,
        marginBottom: 10,
        fontFamily: AppConfig.font.family.semiBold,
        paddingVertical: 10,
        paddingHorizontal: 20,
        color: color.white,
        backgroundColor: color.primary,
        textAlign: 'center',
        width: width / 2
    },
    buttonOutline: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 10,
        marginBottom: 10,
        fontFamily: AppConfig.font.family.semiBold,
        paddingVertical: 10,
        paddingHorizontal: 20,
        color: color.primary,
        textAlign: 'center',
        width: width / 2
    }
})

export { SelesaiPemesanan, SelesaiPembayaran, SimpleModal, ModalDefault }