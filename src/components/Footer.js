import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';

function FooterItem(props) {
    const { totalPrice, totalItem, labelButton, children, isLoading = false } = props
    return (
        <View style={styles.footer}>
            {children}
            {!children && (
                <View style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 10 }}>
                    <Text style={styles.totalLabel}>Total</Text>
                    <Text style={styles.totalPrice}>{totalPrice}  |  {totalItem}</Text>
                </View>
            )}
            <TouchableOpacity disabled={isLoading} {...props} activeOpacity={0.6} style={styles.buttonPesan}>
                {!isLoading && (
                    <>
                        <Text style={styles.buttonPesanLabel}>{labelButton}</Text>
                        <Ionicons name='md-arrow-forward-circle' size={20} color={color.white} />
                    </>
                )}

                {isLoading && (
                    <ActivityIndicator size={"small"} color={color.white} />
                )}

            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    footer: {
        backgroundColor: color.white,
        elevation: 2,
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonPesan: {
        backgroundColor: color.primary,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        minWidth: 150,
        height: '100%',
        justifyContent: 'center'
    },
    totalLabel: {
        color: color.black,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular
    },
    totalPrice: {
        color: color.black,
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold
    },
    buttonPesanLabel: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.white,
        marginRight: 8
    }
})

export { FooterItem }