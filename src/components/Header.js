import React, { Component, useState } from "react";
import { StatusBar, Image, TouchableOpacity, ActivityIndicator, View, Text, StyleSheet, TextInput, ScrollView } from 'react-native';
import color from "../utils/color";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AppConfig from "../utils/AppConfig";
import { useNavigation } from "@react-navigation/native";

function Header({ searchable = true }, props) {
    const [search, setSearch] = useState('');
    const navigation = useNavigation()
    return (
        <>
            <View style={styles.header}>
                <View>
                    <Text style={styles.title}>Nasi Arab No.1</Text>
                    <Text style={styles.title}>Di Indonesia</Text>
                </View>

                <View style={styles.searchContainer}>
                    <Ionicons name="search-outline" size={20} color={color.gray} />
                    {searchable == false && (
                        <TouchableOpacity>
                            <View
                                style={styles.searchInput}
                            >
                                <Text style={{ color: color.grayText }}>Cari "Nasi Kebuli"...</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                    {searchable && (
                        <TextInput
                            style={styles.searchInput}
                            autoFocus={true}
                            placeholder="Cari Nasi Kebuli..."
                            onChangeText={(e) => {
                                if (props.onChange) {
                                    props.onChange(e)
                                }
                            }}
                        />
                    )}
                </View>
            </View>
        </>
    );
}

function HeaderBack(props) {
    const { title } = props
    let theme = props.theme ?? 'default'

    let background = color.primary
    let text = color.white
    let icon = color.white
    if (theme != "default") {
        background = color.white
        text = color.black
        icon = color.black
    }

    return (
        <>
            {/* <StatusBar backgroundColor={background}  /> */}
            <View style={[styles.header, { backgroundColor: background }]}>
                <TouchableOpacity activeOpacity={0.6} {...props}>
                    <SimpleLineIcons name="arrow-left" size={20} color={icon} />
                </TouchableOpacity>
                <Text style={[styles.headerBackTitle, { color: text }]}>{title}</Text>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: color.primary,
        paddingHorizontal: 15,
        paddingVertical: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontFamily: AppConfig.font.family.bold,
        fontSize: AppConfig.font.size.default,
        color: color.white
    },
    searchContainer: {
        backgroundColor: color.white,
        borderRadius: 8,
        paddingHorizontal: 10,
        flex: 1,
        marginLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchInput: {
        paddingVertical: 5,
        marginLeft: 5,
        flex: 1,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerBackTitle: {
        fontSize: AppConfig.font.size.medium,
        color: color.white,
        fontFamily: AppConfig.font.family.bold,
        marginLeft: 10
    }
})

export { Header, HeaderBack }