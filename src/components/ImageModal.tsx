import React from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity } from 'react-native'
import color from '../utils/color';
import { WebView } from 'react-native-webview';
import { HeaderBack } from './Header';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export type Props = {
    visible?: boolean,
    onRequestClose?: any,
    title: string,
    uri: string
}

function ImageModal(props: Props) {
    return (
        <Modal
            animationType='slide'
            transparent={true}
            visible={props.visible}
            onRequestClose={props.onRequestClose}
        >

            <View style={styles.mainModal}>
                <WebView style={{ flex: 1 }} source={{ uri: props.uri }} />
                <View style={styles.floatingContainer}>
                    {props.title && (
                        <Text style={styles.title}>{props.title}</Text>
                    )}
                    <TouchableOpacity style={styles.closeIcon} onPress={props.onRequestClose} activeOpacity={0.6}>
                        <MaterialCommunityIcons name="close" size={30} color={color.white} />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    mainModal: {
        flex: 1,
        backgroundColor: color.blackOpacity
    },
    floatingContainer: {
        position: 'absolute',
        margin: 15,
        flexDirection: 'row', alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        color: color.white,
        fontSize: 16,
        fontWeight : 'bold',
        flex : 1
    },
    closeIcon: {
        // position: 'absolute',
        // top: 15, right: 15
    }
})

export default ImageModal