import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import color from '../utils/color';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AppConfig from '../utils/AppConfig';
import { HttpRequest } from '../utils/http';

function Checkbox(props) {
    let selected = props.selected ?? false
    return (
        <View style={[styles.checkbox, { borderColor: selected ? color.primary : color.grayText }]}>
            {selected && (
                <View style={styles.dot} />
            )}
            {!selected && (
                <View style={[styles.dot, { backgroundColor: "transparent" }]} />
            )}
        </View>
    )
}
React.memo(Checkbox)

function MetodePembayaran(props) {

    let { metodePembayaran } = props
    const [selectedPembayaran, setSelectedPembayaran] = useState("")
    const [selectedItem, setSelectedItem] = useState("")
    const [visible, setVisible] = useState(false)

    useMemo(() => {
        if (props.onChange) {
            props.onChange(selectedItem)
        }
    }, [selectedItem])

    return (
        <View style={styles.main}>
            <TouchableOpacity onPress={() => {
                setVisible(!visible)
            }} activeOpacity={0.6} style={styles.dropDown}>
                <Text style={styles.dropDownLabel}>Pilih Metode Pembayaran</Text>
                <SimpleLineIcons name={visible ? "arrow-down" : "arrow-right"} size={15} color={color.black} />
            </TouchableOpacity>

            {visible && (
                <View style={styles.dropDownContent}>
                    {metodePembayaran.map((item, index) => {
                        return (
                            <TouchableOpacity key={index} onPress={() => {
                                setSelectedItem(item)
                                setSelectedPembayaran(item.nama)
                            }} activeOpacity={0.6} style={[styles.dropDownItem , {borderBottomColor: item.nama == selectedPembayaran ? color.primary : color.gray}]}>
                                <View>
                                    {item.nama == selectedPembayaran && (
                                        <Text style={[styles.dropDownItemLabel, { color: color.primary, borderBottomColor: color.primary, fontWeight: 'bold'}]}>{item.nama}</Text>  
                                    )}
                                    {item.nama != selectedPembayaran && (
                                        <Text style={styles.dropDownItemLabel}>{item.nama}</Text>
                                    )}
                                </View>
                                <Checkbox
                                    selected={item.nama == selectedPembayaran}
                                />
                            </TouchableOpacity>
                        )
                    })}
                </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        marginVertical: 10
    },
    dropDown: {
        borderWidth: 1,
        borderColor: color.primary,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    },
    dropDownLabel: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
        color: color.black
    },
    dropDownContent: {
        borderWidth: 1,
        borderColor: color.grayText,
        borderRadius: 8,
        padding: 15,
        marginTop: 8
    },
    dropDownItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: color.gray,
        borderBottomWidth: 1,
        paddingBottom: 15,
        marginBottom: 10,
    },
    dropDownItemLabel: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
    },
    checkbox: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    dot: {
        padding: 5,
        backgroundColor: color.primary,
        borderRadius: 100,
        margin: 2
    }
})

export default MetodePembayaran