import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function ProdukStore(props) {

    // console.log("render produk 1")
    const [qty, setQty] = useState(0)

    const onIncrease = useCallback(() => {
        setQty((prev) => prev + 1);
    }, [])

    const onDecrease = useCallback(() => {
        setQty((prev) => prev - 1);
    }, [])

    useEffect(() => {
        if (props.onChange) {
            props.onChange(qty)
        }
    }, [qty])

    return (
        <View style={styles.container}>
            <TouchableOpacity activeOpacity={0.6} onPress={() => props.gotoDetail()}>
                <Image style={styles.image} source={{ uri: props.gambar }} />
                {props?.tipe == "karton" && (
                    <Text style={styles.tipe}>{props.tipe}</Text>
                )}
                <View style={{ padding: 10 }}>
                    <Text style={styles.title}>{props.nama.substring(0, 50) + "..." ?? "-"}</Text>
                    <Text style={styles.price}>{props.harga ?? "Rp.0"}</Text>
                </View>
            </TouchableOpacity>

            {qty == 0 && (
                <TouchableOpacity onPress={() => onIncrease()}>
                    <Text style={styles.button}>Beli</Text>
                </TouchableOpacity>
            )}

            {qty > 0 && (
                <View style={styles.counterContainer}>
                    <TouchableOpacity activeOpacity={0.6} onPress={() => onDecrease()}>
                        <MaterialCommunityIcons name='minus-circle-outline' color={color.primary} size={25} />
                    </TouchableOpacity>
                    <Text style={styles.qty}>{qty}</Text>
                    <TouchableOpacity activeOpacity={0.6} onPress={() => onIncrease()}>
                        <MaterialCommunityIcons name='plus-circle' color={color.primary} size={25} />
                    </TouchableOpacity>
                </View>
            )}
        </View>
    )
}

const RADIUS = 12
const styles = StyleSheet.create({
    container: {
        backgroundColor: color.white,
        elevation: 2,
        borderRadius: RADIUS,
    },
    image: {
        width: '100%',
        height: 200,
        borderTopRightRadius: RADIUS,
        borderTopLeftRadius: RADIUS
    },
    title: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black,
    },
    price: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        marginTop: 5,
    },
    description: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        marginTop: 5
    },
    button: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 10,
        margin: 8,
        fontFamily: AppConfig.font.family.semiBold,
        paddingVertical: 8,
        color: color.primary,
        textAlign: 'center'
    },
    counterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10
    },
    qty: {
        color: color.black,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold
    },
    tipe: {
        backgroundColor: color.green,
        color: color.white,
        fontFamily: AppConfig.font.family.semiBold,
        textAlign: 'center',
        paddingHorizontal: 8, 
        paddingVertical: 2,
        borderRadius: 5,
        textAlignVertical:'center',
        position: 'absolute', right: 5, top: 5
    }
})

export default React.memo(ProdukStore)