import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Toast from './Toast';

function ProdukCart(props) {

    console.log("render produk 1")
    const [qty, setQty] = useState(props.qty ?? 0)
    const [variant, setVariant] = useState(props?.variant ?? [])
    // const [hideItemKosong, setHideItemKosong] = useState(props.hideItemKosong ?? false)

    const onIncrease = useCallback(() => {
        setQty((prev) => prev + 1)
    }, [])

    const onDecrease = useCallback(() => {
        setQty((prev) => prev - 1)
    }, [])

    useEffect(() => {
        if (props.onChange) {
            props.onChange(qty)

            if (props.item.tipe == "karton") {
                console.log("ini karton")
                variant.forEach((i) => {
                    i.qty = 0
                })
                setVariant([...variant])
            }
        }
    }, [qty])

    const onIncreaseVariant = useCallback((item) => {

        if (checkMaxPcs() == false) {
            return false
        }

        let index = variant.findIndex((i) => i.id === item.id)
        if (index != -1) {
            let qty = item.qty ?? 0
            qty += 1
            item.qty = qty
            variant[index] = item
            setVariant([...variant])
        }
    })

    const onDecreaseVariant = useCallback((item) => {

        let index = variant.findIndex((i) => i.id === item.id)
        if (index != -1) {
            let qty = item.qty ?? 0
            if (qty > 0) {
                qty -= 1
                item.qty = qty
                variant[index] = item
                setVariant([...variant])
            }
        }
    })

    const onChangeInputQtyVariant = useCallback((e, item) => {
        let index = variant.findIndex((i) => i.id === item.id)
        if (index != -1) {
            item.qty = Number(e)
            variant[index] = item
            setVariant([...variant])
        }
    })

    const checkMaxPcs = useCallback(() => {
        let total = 0
        variant.forEach(element => {
            total += element.qty ?? 0
        });

        if (total >= (props.item.max_pcs * qty)) {
            Toast.showError("Anda sudah mencapai maksimal " + props.item.max_pcs + " pcs")
            return false
        }
    })

    useEffect(() => {
        if (props.onChangeVariant) {
            props.onChangeVariant(variant)
        }
    }, [variant])

    let showButton = props.showButton ?? true
    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <Image style={styles.image} source={{ uri: props?.gambar }} />
                <View style={{ marginLeft: 10 }}>
                    <Text style={styles.title}>{props.nama ?? "-"}</Text>
                    <Text style={styles.price}>{props.harga ?? "Rp.0"}</Text>
                    {props?.item?.tipe == "karton" && (
                        <Text style={styles.max_pcs}>Max PCS {props?.item?.max_pcs}</Text>
                    )}
                </View>
            </View>

            {showButton && (
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <View style={{ width: 100 }}>
                        <View style={styles.counterContainer}>
                            <TouchableOpacity activeOpacity={0.6} onPress={() => onDecrease()}>
                                <MaterialCommunityIcons name='minus-circle-outline' color={color.primary} size={25} />
                            </TouchableOpacity>
                            <Text style={styles.qty}>{qty}</Text>
                            <TouchableOpacity activeOpacity={0.6} onPress={() => onIncrease()}>
                                <MaterialCommunityIcons name='plus-circle' color={color.primary} size={25} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )}

            {variant?.length > 0 && (
                <View style={{ marginTop: 10 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Text style={{ fontFamily: AppConfig.font.family.bold, fontSize : 16 }}>Variant</Text>
                        <View style={{flex : 1, borderBottomColor : color.gray, borderBottomWidth : 1, marginLeft: 8}} />
                    </View>

                    {variant.map((i, key) => {
                        return (
                            <View key={key} style={{ marginTop: 8, borderBottomColor: color.gray, borderBottomWidth: 1, paddingBottom: 5, paddingTop: 8 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                    <Image style={styles.image} source={{ uri: i?.gambar }} />
                                    <View style={{ marginLeft: 10 }}>
                                        {showButton && (
                                            <Text style={styles.title}>{i.nama ?? "-"}</Text>
                                        )}
                                        {!showButton && (
                                            <>
                                                <Text style={styles.title}>{i.nama ?? "-"}</Text>
                                                <Text style={styles.title}>x {i?.qty ?? 0}</Text>
                                            </>
                                        )}
                                    </View>
                                </View>

                                {showButton && (
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                        <View style={{ width: 100 }}>
                                            <View style={styles.counterContainer}>
                                                <TouchableOpacity activeOpacity={0.6} onPress={() => onDecreaseVariant(i)}>
                                                    <MaterialCommunityIcons name='minus-circle-outline' color={color.primary} size={25} />
                                                </TouchableOpacity>
                                                <TextInput onChangeText={(e) => onChangeInputQtyVariant(e, i)} keyboardType='numeric' value={i?.qty?.toString() ?? "0"} style={styles.qtyVariant} />
                                                <TouchableOpacity activeOpacity={0.6} onPress={() => onIncreaseVariant(i)}>
                                                    <MaterialCommunityIcons name='plus-circle' color={color.primary} size={25} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                )}
                            </View>
                        )
                    })}
                </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderBottomColor: color.border,
        borderBottomWidth: 1,
        // paddingBottom: 10,
        padding: 10,
        borderRadius : 8,
        marginBottom: 10,
        backgroundColor: color.white,
        elevation:2
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: color.primary
    },
    title: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black,
        marginRight: 100
    },
    price: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        marginTop: 5,
    },
    description: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        marginTop: 5
    },
    button: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 10,
        margin: 8,
        fontFamily: AppConfig.font.family.semiBold,
        paddingVertical: 8,
        color: color.primary,
        textAlign: 'center'
    },
    counterContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    qty: {
        color: color.black,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
    },
    qtyVariant: {
        color: color.black,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        borderRadius: 3,
        flex: 1,
        textAlign: 'center',
        backgroundColor: color.white, paddingVertical: 0,
        marginHorizontal: 3,
        borderWidth : 0.5
    },
    max_pcs: {
        fontFamily: AppConfig.font.family.medium,
        marginTop: 5,
        color : color.text
    }
})

export default React.memo(ProdukCart)