import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Rupiah from './Rupiah';
import NoData from './NoData';

interface Props {
    onChange?: any,
    selected?: Selected,
    data: any,
    isLoading: boolean
}

interface Selected {
    service?: string,
    price?: any;
    code?: string,
    name?: string,
    cost?: Cost
}

interface Price {
    value: number,
    etd: string,
    note: string
}

interface Courier {
    name: string,
    code: string
}

interface Cost {
    description?: string,
    service: string,
    price: Price
}

function Checkbox(props) {
    let selected = props.selected ?? false
    return (
        <View style={[styles.checkbox, { borderColor: selected ? color.primary : color.grayText }]}>
            {selected && (
                <View style={styles.dot} />
            )}
            {!selected && (
                <View style={[styles.dot, { backgroundColor: "transparent" }]} />
            )}
        </View>
    )
}
React.memo(Checkbox)

function PilihPengiriman(props: Props) {
    const [selected, setSelected] = useState<Selected>({})
    const [visible, setVisible] = useState<Boolean>(false)

    const setKurir = useCallback((courier: Courier, cost?: Cost): void => {
        // console.log(courier, cost)
        setSelected({
            code: courier.code,
            name: courier.name,
            service: cost?.service,
            cost: cost
        })
    },[])

    useEffect(() => {
        setSelected({})
    }, [props.data])

    useEffect(() => {
        if (selected?.code != undefined) {
            if (props.onChange) {
                props.onChange(selected)
            }
        }
    }, [selected])

    return (
        <View style={styles.main}>
            <TouchableOpacity disabled={props.isLoading} onPress={() => setVisible(!visible)} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.label}>Pilih Pengiriman</Text>
                </View>

                {!props.isLoading && (
                    <SimpleLineIcons name={visible ? "arrow-down" : "arrow-right"} size={15} color={color.black} />
                )}

                {props.isLoading && (
                    <View style={{ alignItems: 'flex-start' }}>
                        <ActivityIndicator size={"small"} color={color.primary} />
                    </View>
                )}
            </TouchableOpacity>

            {!props.isLoading && (
                visible && (
                    <View style={{ marginTop: 10 }}>
                        <View style={[styles.seperator, { marginBottom: 8 }]} />

                        {props.data.length == 0 && (
                            <NoData>Kurir pengiriman tidak tersedia</NoData>
                        )}

                        {props.data && (
                            props.data.map((item, index) => {
                                return (
                                    <View key={index} style={{ marginVertical: 5 }}>
                                        <Text style={styles.courierName}>{item.name ?? "-"}</Text>

                                        {item.costs && (
                                            item.costs.map((i, x) => {
                                                return (
                                                    <TouchableOpacity key={x} activeOpacity={0.6} onPress={() => {
                                                        let courier: Courier = {
                                                            code: item.code,
                                                            name: item.name
                                                        }
                                                        let price: Cost = {
                                                            service: i.service,
                                                            price: {
                                                                etd: i.price.etd,
                                                                value: i.price.value,
                                                                note: i.price.note
                                                            }
                                                        }
                                                        setKurir(courier, price)
                                                    }} style={[styles.serviceContainer, { borderBottomColor: i.service == selected.service ? color.primary : color.border }]}>
                                                        <View>
                                                            <Text style={{ color: i.service == selected.service ? color.primary : color.text }}>{i?.service ?? "-"}</Text>
                                                            <Text style={{ color: i.service == selected.service ? color.primary : color.text }}>{Rupiah.format(i?.price?.value) ?? "-"}</Text>
                                                            <Text style={{ color: i.service == selected.service ? color.primary : color.text }}>Estimasi tiba : {i?.price?.etd ?? "-"}</Text>
                                                        </View>
                                                        <Checkbox
                                                            selected={i.service == selected.service}
                                                        />
                                                    </TouchableOpacity>
                                                )
                                            })
                                        )}

                                        {item.costs.length == 0 && (
                                            <View style={[styles.serviceContainer, { borderBottomColor: color.border }]}>
                                                <Text>Pengiriman ini sedang tidak tersedia</Text>
                                            </View>
                                        )}
                                    </View>
                                )
                            })
                        )}
                    </View>
                )
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        borderWidth: 1,
        borderColor: color.primary,
        borderRadius: 8,
        padding: 15,
        marginTop: 10
    },
    label: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
        color: color.black
    },
    seperator: {
        width: '100%',
        height: 1,
        backgroundColor: color.primary
    },
    serviceContainer: {
        marginLeft: 15,
        marginTop: 8,
        borderBottomColor: color.primary,
        borderBottomWidth: 1,
        paddingBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    checkbox: {
        borderWidth: 1.5,
        borderColor: color.primary,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    dot: {
        padding: 5,
        backgroundColor: color.primary,
        borderRadius: 100,
        margin: 2
    },
    courierName: {
        fontSize: 14,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black
    }
})

// export default PilihPengiriman
export default React.memo(PilihPengiriman)