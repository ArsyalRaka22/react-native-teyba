import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { useSelector } from 'react-redux';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

export default function TabBottom(props) {

    const menus = [
        {
            label: "Home",
            page: "Home",
            icon: require('../assets/icon/home.png'),
            iconActive : require('../assets/icon/home-active.png')
        },
        {
            label: "Agen Kami",
            page: "AgenStack",
            icon: require('../assets/icon/shop.png'),
            iconActive: require('../assets/icon/shop-active.png')
        },
        {
            label: "Transaksi",
            page: "TransaksiStack",
            icon: require('../assets/icon/transaksi.png'),
            iconActive: require('../assets/icon/transaksi-active.png')
        },
        {
            label: "Profile",
            page: "ProfileStack",
            icon: require('../assets/icon/user.png'),
            iconActive: require('../assets/icon/user-active.png')
        },
    ]
    const selected = props.selected

    const navigation = useNavigation()
    const user = useSelector((state) => state.user)
    const gotoPage = (page) => {
        if (user) {
            navigation.navigate(page)
        } else {
            navigation.navigate("Auth")
        }
    }
    return (
        <View style={styles.main}>
            {menus.map((item, index) => {
                return (
                    <TouchableOpacity onPress={() => gotoPage(item.page)} activeOpacity={0.6} key={index} style={styles.item}>
                        {selected == index && (
                            <>
                                <Image tintColor={color.primary} style={styles.icon} source={item.iconActive} />
                                <Text style={styles.labelActive}>{item.label}</Text>
                            </>
                        )}
                        {selected != index && (
                            <>
                                <Image style={styles.icon} source={item.icon} />
                                <Text style={styles.label}>{item.label}</Text>
                            </>
                        )}
                    </TouchableOpacity>
                )
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        backgroundColor: color.white,
        elevation: 2,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    item: {
        flex : 1,
        alignItems : 'center'
    },
    icon: {
        width: 25,
        height: 25,
        marginBottom : 3
    },
    label: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        color: '#9E9E9E'
    },
    labelActive: {
        fontSize: AppConfig.font.size.small,
        color: color.primary,
        fontFamily: AppConfig.font.family.bold
    },
})