import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

export default function InputLabel(props){
    return (
        <View style={props.containerStyle}>
            {props.label && (
                <Text style={styles.label}>{props.label}</Text>
            )}
            <TextInput
                {...props}
                style={[styles.input, props.inputStyle]}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection : 'row'
    },
    label: {
        fontSize: AppConfig.font.size.medium,
        fontFamily : AppConfig.font.family.semiBold,
        color : color.black
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#C6C6C6',
        paddingVertical: 3,
        color: color.black
    }
})