import { useNavigation } from '@react-navigation/native';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Dimensions, RefreshControl, ActivityIndicator } from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ProdukStore from '../components/ProdukStore';
import { FooterItem } from '../components/Footer';
import { HttpRequest } from '../utils/http';
import Rupiah from '../components/Rupiah';
import { useDispatch } from 'react-redux';
import { setCartRedux } from '../store/actions';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';

function AgenStore({ route, navigation }) {

    const dispatch = useDispatch()
    const [produk, setProduk] = useState([])
    const [cart, setCart] = useState([])
    const [refresing, setRefresing] = useState(false)
    const [isLoading, setIsLoading] = useState(true)

    const item = route.params.item
    useEffect(() => {
        console.log(item)
        getProdukAgen()
    }, [])

    const getProdukAgen = useCallback(async () => {
        try {
            let res = await HttpRequest.getProdukAgen({ agen_id: item.id })
            console.log("res", res)
            setProduk(res.data.data)
            setIsLoading(false)
        } catch (error) {
            console.log('err', error, error.response)
            setIsLoading(false)
        }
    },[])

    const carting = useCallback((i) => {
        console.log("carting")
        let index = cart.findIndex((item) => item.id == i.id)
        if (index == -1) {
            if (i.qty != 0) {
                cart.push(i)
                setCart([...cart])
            }
        } else {
            if (i.qty != 0) {
                cart[index] = i
                setCart([...cart])
            } else {
                cart.splice(index, 1)
                setCart([...cart])
            }
        }
    }, [])

    const getTotalBelanja = useMemo(() => {
        let total = 0
        cart.forEach((i) => {
            if (i.qty != 0) {
                total += i.qty * i.harga_jual
            }
        })

        return total
    }, [cart])

    useEffect(() => {
        console.log("cart", cart)
    }, [cart])

    const { width, height } = Dimensions.get('screen')
    const data = [{}, {}, {}, {}, {}, {}, {}, {},]
    return (
        <View style={styles.main}>
            <HeaderBack
                onPress={() => {
                    navigation.goBack()
                }}
                title="Agen"
            />


            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refresing}
                        onRefresh={() => getProdukAgen()}
                    />
                }
            >

                <View style={styles.item}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.type}>{item.store_label}</Text>
                    </View>
                    <Text style={styles.name}>{item.store_name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Ionicons name='location-sharp' size={20} color={color.grayText} />
                        <Text>{item.store_city}</Text>
                    </View>
                </View>

                <View style={{ padding: 10 }}>

                    {isLoading && (                        
                        <Placeholder.DaftarProduk />
                    )}

                    {!isLoading && (
                        <>
                            {produk.length == 0 && (
                                <NoData>Data Produk Tidak Ditemukan</NoData>
                            )}

                            <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap' }}>
                                {produk.map((i, index) => {
                                    return (
                                        <View key={index} style={{ width: (width / 2) - 20, margin: 5 }}>
                                            <ProdukStore
                                                nama={i.nama}
                                                gambar={i.gambar}
                                                tipe={i.tipe}
                                                harga={Rupiah.format(i.harga_jual, false)}
                                                onChange={(e) => {
                                                    i.qty = e
                                                    carting(i)
                                                }}
                                                gotoDetail={() =>
                                                    navigation.navigate("DetailProduk", { item: i })
                                                }
                                            />
                                        </View>
                                    )
                                })}
                            </View>
                        </>
                    )}
                </View>
            </ScrollView>

            {cart.length > 0 && (
                <FooterItem
                    totalItem={cart.length + " Item"}
                    totalPrice={Rupiah.format(getTotalBelanja,false)}
                    labelButton="PESAN"
                    onPress={() => {
                        console.log("cart sebelum", cart)
                        dispatch(setCartRedux([...cart]))
                        navigation.navigate("PesanProduk", {cart})
                    }}
                />
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    item: {
        padding: 10,
        borderColor: '#E0E0E0',
        marginBottom: 10
        // borderRadius: 12,
        // borderWidth: 1.5,
    },
    type: {
        backgroundColor: '#E9CCFF',
        borderRadius: 100,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: color.black,
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.medium,
    },
    name: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        marginHorizontal: 3,
    },
})

export default AgenStore