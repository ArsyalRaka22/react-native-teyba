import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView,Image } from 'react-native';
import { HeaderBack } from '../components/Header';
import color from '../utils/color';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Button from '../components/Button';
import InputLabel from '../components/InputLabel';
import { HttpRequest } from '../utils/http';
import Toast from '../components/Toast';

export default function SettingPembayaranDetail({ navigation, route }) {

    const [noRekening, setNoRekening] = useState('')
    const [atasNama, setAtasNama] = useState('')

    useEffect(() => {
        console.log("route", route.params)
        if (route.params.item.data) {
            setAtasNama(route.params.item.data.atas_nama)
            setNoRekening(route.params.item.data.nomor_rekening)
        }
    }, [])

    const simpan = useCallback(async () => {
        try {

            if (noRekening == "") {
                Toast.showError("Nomor Rekening Harus di isi")
                return
            }
            if (atasNama == "") {
                Toast.showError("Atas Nama Harus di isi")
                return
            }

            let param = {
                bank_id: route.params.item.bank_id,
                user_pembayaran_id: route?.params?.item?.data?.user_pembayaran_id,
                atas_nama: atasNama,
                no_rekening: noRekening
            }
            let res = await HttpRequest.simpanBankPembayaran(param)
            Toast.showSuccess(res.data.message, 4000)
            navigation.goBack()
            console.log('res', res)
        } catch (error) {
            console.log("err", error, error.response)
        }
    })

    return (
        <View style={styles.main}>

            <HeaderBack
                theme="light"
                title="Setting Pembayaran"
                onPress={() => navigation.goBack()}
            />

            <ScrollView>
                <View style={{ padding: 15 }}>

                    <View style={{marginBottom : 20}}>
                        {route?.params?.item?.bank_gambar != null && (
                            <Image resizeMode='contain' source={{ uri: route?.params?.item?.bank_gambar }} style={{ width: 100, height: 50 }} />
                        )}
                        <Text style={{ fontSize: 16 }}>{route?.params?.item?.bank_nama}</Text>
                        <Text style={{ fontSize: 13 }}>{route?.params?.item?.bank_keterangan}</Text>
                    </View>

                    <InputLabel
                        label="Nomor Rekening"
                        placeholder="123xxxxx"
                        containerStyle={styles.input}
                        keyboardType="number"
                        value={noRekening}
                        onChangeText={(e) => setNoRekening(e)}
                    />
                    <InputLabel
                        label="Atas Nama"
                        placeholder="Atas Nama.."
                        containerStyle={styles.input}
                        value={atasNama}
                        onChangeText={(e) => setAtasNama(e)}
                    />
                </View>
            </ScrollView>

            <Button
                onPress={() => simpan()}
                style={{margin: 15}}
            >Simpan</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    card: {
        backgroundColor: color.white,
        elevation: 2,
        borderRadius: 5,
    },
    seperator: {
        backgroundColor: color.gray,
        height: 1,
        borderRadius: 100,
        width: '100%'
    },
    input: {
        marginBottom : 15
    }
})