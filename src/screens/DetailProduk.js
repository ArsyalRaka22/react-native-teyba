import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Dimensions } from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import RenderHtml from 'react-native-render-html';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { useState } from 'react';
import { useSelector } from 'react-redux';

function DetailProduk({ navigation, route }) {

    const item = route.params?.item
    const [slider, setSlider] = useState()
    // const { width } = useWindowDimensions();
    let { width, height } = Dimensions.get('screen')
    const user = useSelector((state) => state.user)

    useEffect(() => {
        console.log(item)

        let gallery = []
        gallery.push(item.gambar)

        if (item?.gallery?.length > 0) {
            item?.gallery?.forEach(element => {
                gallery.push(element.path)
            });
        }

        setSlider(gallery)
    }, [])

    const gotoPage = () => {
        if (user) {
            navigation.navigate("AgenStack")
        } else {
            navigation.navigate("Auth")
        }
    }

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Detail Produk"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView>
                <SwiperFlatList
                    autoplay={false}
                    data={slider}
                    renderItem={({ item }) => (
                        <View style={{ height: 300, width: width }}>
                            <Image resizeMode='cover' style={{ width: width, height: 300 }} source={{ uri: item }} />
                        </View>
                    )}
                />

                <View style={{ padding: 15 }}>
                    <Text style={styles.title}>{item?.nama}</Text>

                    <RenderHtml
                        contentWidth={width - 30}
                        source={{ html: item?.deskripsi }}
                    />

                    {item?.variant?.length > 0 && (
                        <View style={{ marginTop: 10, backgroundColor: color.white, elevation: 2, padding: 8, borderRadius: 8 }}>

                            {item.variant.map((i, index) => {
                                return (
                                    <>
                                        <View key={index} style={styles.variant}>
                                            <Image source={{ uri: i.gambar }} style={{ width: 80, height: 80, borderRadius: 8, borderWidth: 1, borderColor: color.primary }} />
                                            <View style={{ flex: 1 }}>
                                                <Text style={{ fontFamily: AppConfig.font.family.regular, marginLeft: 8, }}>{i.nama}</Text>
                                            </View>

                                        </View>
                                        <View style={styles.line} />
                                    </>
                                )
                            })}
                        </View>
                    )}
                </View>
            </ScrollView>
            <TouchableOpacity activeOpacity={0.8} onPress={() => gotoPage(item.page)} style={styles.btnPesan}>
                <Text style={styles.txtPesan}>Pesan</Text>
            </TouchableOpacity>
        </View>
    )
}

let { width, height } = Dimensions.get('screen')
let jumlahRow = 2
const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    image: {
        width: '100%',
        height: 300
    },
    title: {
        fontSize: AppConfig.font.size.medium,
        color: color.black,
        fontFamily: AppConfig.font.family.bold
    },
    description: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        marginTop: 10
    },
    variant: {
        flexDirection: 'row',
    },
    line: {
        borderBottomColor: color.gray,
        borderBottomWidth: 1,
        marginVertical: 8
    },
    btnPesan: {
        backgroundColor: color.primary, justifyContent: "center", marginHorizontal: 20, borderRadius: 10, alignItems: 'center', paddingVertical: 15
    },
    txtPesan: {
        color: color.white,
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
    }
})
export default DetailProduk