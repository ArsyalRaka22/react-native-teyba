import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, RefreshControl, ActivityIndicator } from 'react-native';
import { HeaderBack } from '../components/Header';
import color from '../utils/color';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { HttpRequest } from '../utils/http';
import Feather from 'react-native-vector-icons/Feather'
import { useIsFocused } from '@react-navigation/native';
import Placeholder from '../components/Placeholder';

export default function SettingPembayaran({ navigation }) {

    const [dataPembayaran, setDataPembayaran] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const isFocus = useIsFocused()

    const getDataPembayaranSaya = useCallback(async () => {
        setIsLoading(true)
        try {
            let res = await HttpRequest.daftarPembayaranSaya()
            console.log("res", res)
            setDataPembayaran(res.data.data)
            setIsLoading(false)
        } catch (error) {
            console.log('err', error, error.response)
            setIsLoading(false)
        }
    })

    useEffect(() => {

        if (isFocus) {
            getDataPembayaranSaya()
        }
    }, [isFocus])

    return (
        <View style={styles.main}>

            <HeaderBack
                theme="light"
                title="Setting Pembayaran"
                onPress={() => navigation.goBack()}
            />

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={isLoading}
                        onRefresh={() => getDataPembayaranSaya()}
                    />
                }
            >
                {isLoading && (
                    <View style={{padding : 10}}>
                        <Placeholder.SettingPembayaran />
                    </View>
                )}

                {!isLoading && (
                    <View style={{ padding: 15 }}>

                        {dataPembayaran?.map((item, index) => {
                            return (
                                <View key={index} style={styles.card}>
                                    <View style={{ padding: 15 }}>
                                        {item.bank_gambar != null && (
                                            <Image resizeMode='contain' source={{ uri: item.bank_gambar }} style={{ width: 100, height: 50 }} />
                                        )}
                                        <Text style={{ fontSize: 16 }}>{item.bank_nama}</Text>
                                        <Text style={{ fontSize: 13 }}>{item.bank_keterangan}</Text>
                                    </View>

                                    {item?.bank_is_cod == false && (
                                        <>
                                            <View style={styles.seperator} />

                                            {item.data == null && (
                                                <TouchableOpacity onPress={() => {
                                                    navigation.navigate("SettingPembayaranDetail", { item })
                                                }} activeOpacity={0.4} style={{ padding: 15, flexDirection: 'row', alignItems: 'center' }}>

                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Feather name='alert-triangle' size={18} />
                                                        <Text style={{ marginLeft: 10 }}>Belum disetting</Text>
                                                    </View>

                                                    <MaterialIcons name='keyboard-arrow-right' size={20} />
                                                </TouchableOpacity>
                                            )}

                                            {item.data != null && (
                                                <TouchableOpacity onPress={() => {
                                                    navigation.navigate("SettingPembayaranDetail", { item })
                                                }} activeOpacity={0.4} style={{ padding: 15, flexDirection: 'row', alignItems: 'center' }}>

                                                    <View style={{ flex: 1 }}>
                                                        <Text style={styles.bold}>{item?.data?.atas_nama}</Text>
                                                        <Text style={styles.bold}>{item?.data?.nomor_rekening}</Text>
                                                    </View>

                                                    <MaterialIcons name='keyboard-arrow-right' size={20} />
                                                </TouchableOpacity>
                                            )}
                                        </>
                                    )}
                                </View>
                            )
                        })}
                    </View>
                )}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    card: {
        backgroundColor: color.white,
        elevation: 2,
        borderRadius: 5,
        marginBottom: 15
    },
    seperator: {
        backgroundColor: color.gray,
        height: 1,
        borderRadius: 100,
        width: '100%'
    },
    bold: {
        fontSize: 13,
        fontWeight: 'bold'
    }
})