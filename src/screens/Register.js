import { useNavigation } from '@react-navigation/native';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, Image, StyleSheet, Modal } from 'react-native';
import InputLabel from '../components/InputLabel';
import TabBottom from '../components/TabBottom';
import color from '../utils/color';
import Octicons from 'react-native-vector-icons/Octicons'
import AppConfig from '../utils/AppConfig';
import { iteratorSymbol } from 'immer/dist/internal';
import Constant from '../utils/Constant';
import SimpleModal from '../components/SimpleModal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { HttpRequest } from '../utils/http';
import Toast from '../components/Toast';
import Button from '../components/Button';

function Register() {

    const navigation = useNavigation()
    const [gender, setGender] = useState({})
    const [selectedRole, setSelectedRole] = useState({})
    const [dataKota, setDataKota] = useState([])
    const [selectedKota, setSelectedKota] = useState(null)
    const [cariKota, setCariKota] = useState("")
    const [modalKota, setModalKota] = useState(false)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [identitas, setIdentitas] = useState('')
    const [address, setAddress] = useState('')
    const [isSubmitting, setIsSubmitting] = useState(false)

    const _gender = [
        { id: 1, name: "Laki-Laki" },
        { id: 2, name: "Perempuan" },
    ]
    const _role = [
        { id: Constant.Role.RESELLER, name: "Reseller" },
        { id: Constant.Role.AGEN, name: "Agen" },
    ]
    const getDataKota = useCallback(() => {
        console.log("search", cariKota)
        HttpRequest.listKota(cariKota).then((res) => {
            console.log("res list kota", res)
            setDataKota(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })

    useMemo(() => {
        if (cariKota.length >= 3) {
            getDataKota()
        }
    }, [cariKota])

    useEffect(() => {
        getDataKota()
    }, [])
    const gotoHome = () => {

        setIsSubmitting(true)

        let errors = []
        if (name == "") {
            errors.push("Lengkapi isian nama")
        }
        if (email == "") {
            errors.push("Lengkapi isian email")
        }
        if (phone == "") {
            errors.push("Lengkapi isian Nomor Hp")
        }
        if (gender == "") {
            errors.push("Lengkapi pilihan jenis kelamin")
        }
        if (identitas == "") {
            errors.push("Lengkapi isian nomor identitas")
        }
        // if (selectedRole == "" || selectedRole == null || selectedRole == {}) {
        //     errors.push("Lengkapi jenis pendaftaran pengguna Agen/Reseller")
        // }

        if (errors.length == 0) {
            let data = {
                name, email, phone,
                // role_id: selectedRole?.id,
                jenis_kelamin: gender?.id,
                no_identitas: identitas,
                alamat_toko: address
            }

            HttpRequest.register(data).then((res) => {
                console.log("res", res)
                Toast.showSuccess(res.data.message, 3000)
                navigation.goBack()
                setIsSubmitting(false)
            }).catch((err) => {
                console.log("err register", err, err.response)
                Toast.showError("Kegagalan Sistem, Pendaftaran tidak berhasil\n\n" + err.response?.data?.message, 4000)
                setIsSubmitting(false)
            })   
        } else {
            let messages = errors.join("\n")
            Toast.showError(messages, errors.length * 1000)
            setIsSubmitting(false)
        }

        // navigation.popToTop()
        // navigation.navigate("Dashboard")
    }
    return (
        <View style={styles.main}>
            <View style={[styles.row, { padding: 20 }]}>
                <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.goBack()}>
                    <Image resizeMode='contain' source={require('../assets/icon/arrow-left.png')} style={{ width: 25, height: 25 }} />
                </TouchableOpacity>
                <Text style={[styles.bold, { fontSize: 18, marginLeft: 8, color: color.black }]}>Lengkapi Data Pendaftaran</Text>
            </View>

            <Modal
                animationType='slide'
                visible={modalKota}
                transparent={false}
                onRequestClose={() => {
                    setModalKota(false)
                }}
            >

                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15, backgroundColor: color.white, elevation: 2 }}>
                        <TouchableOpacity activeOpacity={0.6}>
                            <MaterialCommunityIcons name='arrow-left' size={20} color={color.black} />
                        </TouchableOpacity>
                        <Text style={{ color: color.black, fontSize: 16, marginHorizontal: 10 }}>Pilih Kota</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: color.white, elevation: 2, borderRadius: 5, padding: 5, marginHorizontal: 20, marginVertical: 10 }}>
                        <MaterialIcons name='search' size={20} color={color.black} />
                        <TextInput
                            style={{ paddingVertical: 0 }}
                            placeholder="Cari Kota.."
                            value={cariKota}
                            onChangeText={(e) => setCariKota(e)}
                        />
                    </View>

                    <ScrollView>
                        {dataKota.map((i, key) => {
                            return (
                                <TouchableOpacity onPress={() => setSelectedKota(i)} key={key} style={{ borderBottomColor: color.gray, borderBottomWidth: 1.5, padding: 15 }}>
                                    <Text style={{ color: color.black }}>{i.nama}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>

                    {/* <TouchableOpacity activeOpacity={0.6} style={{padding: 15, backgroundColor: color.primary, margin: 20, borderRadius: 5, elevation: 2}}>
                        <Text style={{color: color.white, textAlign: 'center', fontWeight: 'bold'}}>Pilih Kota</Text>
                    </TouchableOpacity> */}
                </View>
            </Modal>

            <ScrollView>
                <View style={styles.container}>
                    <InputLabel
                        label="Nama"
                        placeholder="Nama kamu"
                        value={name}
                        onChangeText={(e) => setName(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Email"
                        value={email}
                        keyboardType="email-address"
                        placeholder="Email kamu"
                        onChangeText={(e) => setEmail(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Nomor Hp"
                        value={phone}
                        keyboardType="numeric"
                        onChangeText={(e) => setPhone(e)}
                        placeholder="Nomor Handphone"
                        containerStyle={styles.input}
                    />

                    <View style={{ marginBottom: 15 }}>
                        <Text style={styles.label}>{"Jenis Kelamin"}</Text>
                        {_gender.map((i) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        setGender(i)
                                    }}
                                    activeOpacity={0.6}
                                    style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                    <Octicons name={i.id == gender.id ? "check-circle-fill" : "circle"} size={15} color={i.id == gender.id ? color.primary : color.black} style={{ marginRight: 5 }} />
                                    <Text>{i.name}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </View>

                    {/* <InputLabel
                        label="Tanggal Lahir"
                        containerStyle={styles.input}
                    /> */}
                    <InputLabel
                        label="Nomor Identitas"
                        value={identitas}
                        keyboardType="numeric"
                        onChangeText={(e) => setIdentitas(e)}
                        placeholder="Nomor identitas KTP/SIM"
                        containerStyle={styles.input}
                    />

                    {/* <View style={{ marginBottom: 15 }}>
                        <Text style={styles.label}>{"Daftar Sebagai"}</Text>
                        {_role.map((i) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        setSelectedRole(i)
                                    }}
                                    activeOpacity={0.6}
                                    style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                    <Octicons name={i.id == selectedRole.id ? "check-circle-fill" : "circle"} size={15} color={i.id == selectedRole.id ? color.primary : color.black} style={{ marginRight: 5 }} />
                                    <Text>{i.name}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </View> */}

                    <View>
                        <View style={{ marginBottom: 15 }}>
                            <InputLabel
                                label="Alamat Lengkap"
                                placeholder="Alamat lengkap kamu"
                                value={address}
                                onChangeText={(e) => setAddress(e)}
                                inputStyle={styles.inputTextArea}
                                multiLine={true}
                                numberOfLines={5}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
            {/* <TouchableOpacity onPress={() => gotoHome()} activeOpacity={0.6} style={styles.btn}>
                <Text style={{ color: color.white, fontSize: 16, fontWeight: 'bold' }}>Daftar</Text>
            </TouchableOpacity> */}

            <Button style={{margin:20}} loading={isSubmitting} onPress={() => gotoHome()}>
                Daftar
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    label: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black
    },
    container: {
        marginVertical: 40,
        marginHorizontal: 20
    },
    input: {
        marginBottom: 20
    },
    bold: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputPhoneNumber: {
        borderBottomWidth: 1,
        borderBottomColor: '#C4C4C4',
        flex: 1,
        paddingVertical: 3,
        marginBottom: 5,
        fontSize: 16
    },
    btn: {
        paddingHorizontal: 15,
        paddingVertical: 12,
        backgroundColor: color.primary,
        alignItems: 'center',
        borderRadius: 8,
        marginTop: 20,
        marginBottom: 15,
        marginHorizontal: 20
    },
    inputTextArea: {
        borderWidth: 1,
        borderColor: '#C6C6C6',
        paddingVertical: 3,
        color: color.black,
        marginTop: 5,
        borderRadius: 5
    }
})

export default Register