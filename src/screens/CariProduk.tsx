import React , {useState}from 'react'
import { View, Text } from 'react-native'
import { Header } from '../components/Header'

const CariProduk = () => {
    const [search, setSearch] = useState('')
    return (
        <View>
            <Header
                searchable={true}
                onChange={(e) => setSearch(e)}
            />

            <Text>{search ?? '-'}</Text>
        </View>
    )
}

export default CariProduk