import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList, ActivityIndicator, ScrollView } from 'react-native';
import { HeaderBack } from '../components/Header';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

export default function HistoryPoint({ navigation }) {

    const [history, setHistory] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [page, setPage] = useState(1)

    const getHistory = useCallback(async () => {
        setIsLoading(true)
        try {
            let res = await HttpRequest.historyPoint()
            if (res?.data?.data?.length != 0) {
                setHistory(history.concat(res.data.data))
            }
            console.log("res", res)
            setIsLoading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    })

    useEffect(() => {
        getHistory()
    }, [page])

    useEffect(() => {
        getHistory()
    }, [])

    return (
        <View style={styles.main}>
            <HeaderBack
                title="History Point"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            {isLoading && (
                <ScrollView>
                    <View style={{ padding: 15 }}>
                        <Placeholder.HistoryPoint />
                    </View>
                </ScrollView>
            )}

            {!isLoading && (
                history.length == 0 && (
                    <NoData>
                        Belum ada history point
                    </NoData>
                )
            )}

            {!isLoading && (
                <FlatList
                    data={history}
                    // onEndReached={() => setPage(page + 1)}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.item}>
                                <Text style={styles.tanggal}>{item.tanggal}</Text>
                                <View style={{ marginTop: 5 }}>
                                    <Text style={{ fontSize: 20 }}>{item.jumlah
                                    }</Text>
                                    <Text>{item.keterangan}</Text>
                                </View>
                            </View>
                        )
                    }}
                />
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        color: color.white
    },
    item: {
        borderBottomColor: color.border,
        borderBottomWidth: 1,
        padding: 15,
        // flexDirection : 'row'
    },
    tanggal: {
        textAlign: 'right',
        fontFamily: AppConfig.font.family.regular,
        fontSize: 12
    }
})