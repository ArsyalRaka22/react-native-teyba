import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Alert, ActivityIndicator } from 'react-native';
import { HeaderBack } from '../components/Header';
import { SelesaiPembayaran, SelesaiPemesanan } from '../components/Selesai';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProdukCart from '../components/ProdukCart';
import MetodePembayaran from '../components/MetodePembayaran';
import { FooterItem } from '../components/Footer';
import InputLabel from '../components/InputLabel';
import { HttpRequest } from '../utils/http';
import Rupiah from '../components/Rupiah';
import Toast from '../components/Toast';
import ImageModal from '../components/ImageModal';

function ItemRow(props) {
    let { label1, label2 } = props
    return (
        <View style={styles.rowItem}>
            <Text style={[styles.rowItemRegular, { flex: 1 }]}>{label1 ?? "-"}</Text>
            <Text style={[styles.rowItemRegular, { flex: 1, textAlign: 'right' }]}>{label2 ?? "-"}</Text>
        </View>
    )
}

function DetailPesanan({ navigation, route }) {

    const [detailTransaksi, setDetailTransaki] = useState(null)
    const [ongkir, setOngkir] = useState([0])
    const [modalImage, setModalImage] = useState(false)
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        console.log(route.params)
        getDetailTransaksi()
    }, [])

    const getDetailTransaksi = useCallback(async () => {
        try {
            setIsLoading(true)
            let res = await HttpRequest.getDetailTransaksi({ id: route.params?.transaksi_id, is_penjual: true })
            console.log("res getDetailTransaksi", res)
            setDetailTransaki(res.data.data)
            setIsLoading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    })

    const getCamelcaseString = (string) => {
        string = string.charAt(0).toUpperCase() + string.slice(1)
        return string.replace("_", " ")
    }

    const terimaPesanan = useCallback(async () => {
        try {

            // if (detailTransaksi?.transaksi?.is_cod != 1) {
            //     if (ongkir == "" || ongkir == 0) {
            //         Toast.showError("Mohon isi onkos kirim terlebih dahulu")
            //         return
            //     }
            // }

            let param = {
                transaksi_id: detailTransaksi?.transaksi?.id,
                // ongkos_kirim: ongkir
            }
            let res = await HttpRequest.terimaPesananAgen(param)
            Toast.showSuccess(res.data.message, 5000)
            navigation.goBack()
            console.log("terimaPesanan", res)
        } catch (error) {
            console.log('err', error, error.response)
            Toast.showError(error.response?.data?.message)
        }
    })

    const getTotalItem = useMemo(() => {
        let total = 0
        detailTransaksi?.details?.forEach((i) => {
            if (i.qty != 0) {
                total += i.jumlah
            }
        })

        return total
    }, [detailTransaksi])

    const actionTerimaPembayaranAgen = useCallback(async () => {
        try {
            let param = { transaksi_id: detailTransaksi?.transaksi?.id }
            let res = await HttpRequest.terimaPembayaranAgen(param)
            console.log("res", res)
            Toast.showSuccess(res.data.message)
            navigation.goBack()
        } catch (error) {
            console.log('err', error, error.response)
            Toast.showError(error.response?.data?.message)
        }
    })

    const actionTolakPesanan = useCallback(async () => {
        try {
            let param = { transaksi_id: detailTransaksi?.transaksi?.id }
            let res = await HttpRequest.tolakPesanan(param)
            Toast.showSuccess(res.data.message)
            navigation.goBack()
        } catch (error) {
            console.log("err", error, error.response)
            Toast.showError(error?.response?.data?.message ?? "Kesalahan sistem")
        }
    },[])

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Detail Pesanan"
                theme="light"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ImageModal
                visible={modalImage}
                onRequestClose={() => setModalImage(false)}
                uri={detailTransaksi?.bukti_pembayaran}
                title={"Pembayaran - " + detailTransaksi?.transaksi?.no_transaksi}
            />

            
            {isLoading && (
                <ActivityIndicator size={"small"} color={color.primary} />
            )}

            <ScrollView>

                {!isLoading && (
                    <View style={{ padding: 15 }}>

                        <View style={{ marginBottom: 10 }}>
                            <Text style={styles.title}>Status Pesanan</Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {detailTransaksi?.status?.image && (
                                    <Image style={styles.imageStatus} resizeMode="contain" source={{ uri: detailTransaksi?.status?.image }} />
                                )}
                                {detailTransaksi?.status?.image == null && (
                                    <Text style={{ color: detailTransaksi?.status?.color, marginVertical: 10 }}>{detailTransaksi?.status.label}</Text>
                                )}

                                {detailTransaksi?.transaksi?.is_cod == 1 && (
                                    <Text style={{ marginLeft: 10, backgroundColor: color.green, paddingHorizontal: 8, paddingVertical: 2, borderRadius: 100, color: color.white }}>COD</Text>
                                )}
                            </View>
                        </View>

                        <View style={{ marginBottom: 5 }}>
                            {detailTransaksi?.details?.map((item, index) => {
                                return (
                                    <ProdukCart
                                        key={index}
                                        gambar={item.gambar}
                                        showButton={false}
                                        nama={item.produk}
                                        harga={Rupiah.format(item.harga_jual, false) + " x " + item.jumlah}
                                    />
                                )
                            })}
                        </View>

                        {detailTransaksi?.transaksi?.is_dropshipper == 1 && (
                            <View style={{marginBottom : 10}}>
                                <Text style={[styles.title, { marginVertical: 15 }]}>Dropshipper</Text>
                                <ItemRow label1={"Nama Pengirim"} label2={detailTransaksi?.transaksi?.dropshipper_name ?? "-"} />
                                <ItemRow label1={"Telepon Pengirim"} label2={detailTransaksi?.transaksi?.dropshipper_phone ?? "-"} />
                                <View style={{ height: 1, backgroundColor: color.border }} />
                            </View>
                        )}

                        <Text style={[styles.title, { marginVertical: 6 }]}>Detail Pemesan</Text>
                        {/* <View style={{ borderRadius: 5, borderColor: color.gray, padding: 15, borderWidth: 1 }}> */}

                        {detailTransaksi != null && (
                            Object.keys(detailTransaksi?.address)?.map((item, index) => {
                                return (
                                    <ItemRow key={index} label1={getCamelcaseString(item)} label2={detailTransaksi?.address[item]} />
                                )
                            })
                        )}
                        {/* </View> */}

                        <View style={{ height: 1, backgroundColor: color.border }} />
                        <Text style={[styles.title, { marginVertical: 15 }]}>Pembayaran</Text>
                        <View style={{}}>

                            {detailTransaksi != null && (
                                Object.keys(detailTransaksi?.pembayaran)?.map((item, index) => {
                                    return (
                                        <ItemRow key={index} label1={getCamelcaseString(item)} label2={detailTransaksi?.pembayaran[item]} />
                                    )
                                })
                            )}
                        </View>

                        <View style={{ height: 1, backgroundColor: color.border }} />

                        {detailTransaksi?.transaksi?.is_cod != 1 && (
                            detailTransaksi?.pengiriman && (
                                <>
                                    <Text style={[styles.title, { marginVertical: 15 }]}>Pengiriman</Text>
                                    <View style={{}}>
                                        {detailTransaksi != null && (
                                            Object.keys(detailTransaksi?.pengiriman)?.map((item, index) => {
                                                if (item == "ongkos_kirim") {
                                                    return (
                                                        <ItemRow key={index} label1={getCamelcaseString(item)} label2={Rupiah.format(detailTransaksi?.pengiriman[item] ?? 0, false)} />
                                                    )
                                                } else {
                                                    return (
                                                        <ItemRow key={index} label1={getCamelcaseString(item)} label2={detailTransaksi?.pengiriman[item ?? 0]} />
                                                    )
                                                }
                                            })
                                        )}
                                    </View>

                                    <View style={{ height: 1, backgroundColor: color.border }} />
                                </>
                            )
                        )}

                        {/* {detailTransaksi?.status?.id == 1 && detailTransaksi?.transaksi?.is_cod != 1 && (
                        <>
                            <Text style={[styles.title, { marginVertical: 15 }]}>Atur Ongkos Kirim</Text>
                            <InputLabel
                                placeholder="Rp."
                                containerStyle={{ marginBottom: 20 }}
                                value={ongkir}
                                onChangeText={(e) => setOngkir(e)}
                                keyboardType="numeric"
                                inputStyle={{
                                    borderWidth: 1,
                                    borderColor: '#C6C6C6',
                                    paddingVertical: 5,
                                    color: color.black,
                                    borderRadius: 8,
                                    padding: 10
                                }}
                            />
                        </>
                    )} */}


                        <View style={{ marginVertical: 15 }}>
                            <View style={styles.rowItem}>
                                <Text style={styles.rowItemRegular}>{getTotalItem} Item</Text>
                                <Text style={styles.rowItemRegular}>{Rupiah.format(detailTransaksi?.transaksi?.total_transaksi ?? 0, false)}</Text>
                            </View>
                            <View style={styles.rowItem}>
                                <Text style={styles.rowItemRegular}>Ongkos Kirim</Text>
                                <Text style={styles.rowItemRegular}>{Rupiah.format(detailTransaksi?.transaksi?.ongkos_kirim ?? 0, false)}</Text>
                            </View>
                            <View style={styles.rowItem}>
                                <Text style={styles.rowItemBold}>Total</Text>
                                <Text style={styles.rowItemBold}>{Rupiah.format(detailTransaksi?.transaksi?.total_tagihan ?? 0, false)}</Text>
                            </View>
                        </View>

                        {detailTransaksi?.bukti_pembayaran != null && (
                            <>
                                <Text style={[styles.title, { marginVertical: 15 }]}>Bukti Pembayaran</Text>
                                <TouchableOpacity activeOpacity={0.6} onPress={() => setModalImage(true)}>
                                    <Image source={{ uri: detailTransaksi?.bukti_pembayaran }} style={{ width: '100%', height: 250 }} />
                                </TouchableOpacity>
                            </>
                        )}

                    </View>
                )}
            </ScrollView>

            <View style={styles.border} />

            {detailTransaksi?.status?.id == 1 && detailTransaksi?.transaksi?.is_cod != 1 && (
                <TouchableOpacity onPress={() => {
                    Alert.alert("Terima Pesanan", "Apakah anda yakin menerima pesanan ini?", [
                        {
                            text: "tidak",
                            style: 'cancel'
                        },
                        {
                            text: "Ya",
                            onPress: () => {
                                terimaPesanan()
                            }
                        }
                    ], { cancelable: true })
                }} activeOpacity={0.6}>
                    <Text style={styles.button}>Terima Pesanan</Text>
                </TouchableOpacity>
            )}

            {detailTransaksi?.status?.id == 1 && detailTransaksi?.transaksi?.is_cod == 1 && (
                <TouchableOpacity onPress={() => {
                    Alert.alert("Terima Pesanan", "Apakah anda yakin menerima pesanan COD ini?", [
                        {
                            text: "tidak",
                            style: 'cancel'
                        },
                        {
                            text: "Ya",
                            onPress: () => {
                                terimaPesanan()
                            }
                        }
                    ], { cancelable: true })
                }} activeOpacity={0.6}>
                    <Text style={styles.button}>Terima Pesanan COD</Text>
                </TouchableOpacity>
            )}

            {detailTransaksi?.status?.id == 1 && (
                <TouchableOpacity onPress={() => {
                    Alert.alert("Tolak Pesanan", "Apakah anda yakin menolak pesanan ini?", [
                        {
                            text: "tidak",
                            style: 'cancel'
                        },
                        {
                            text: "Ya",
                            onPress: () => {
                                actionTolakPesanan()
                            }
                        }
                    ], { cancelable: true })
                }} activeOpacity={0.6}>
                    <Text style={[styles.button, {backgroundColor : color.danger, marginTop: 0}]}>Tolak Pesanan</Text>
                </TouchableOpacity>
            )}

            {detailTransaksi?.status?.id == 2 && detailTransaksi?.bukti_pembayaran != null && (
                <TouchableOpacity onPress={() => {
                    Alert.alert("Konfirmasi Terima Pembayaran", "Apakah anda yakin sudah menerima pembayaran ini sebesar " + Rupiah.format(detailTransaksi?.transaksi?.total_tagihan ?? 0, false), [
                        {
                            text: "tidak",
                            style: 'cancel'
                        },
                        {
                            text: "Ya",
                            onPress: () => {
                                actionTerimaPembayaranAgen()
                            }
                        }
                    ], { cancelable: false })
                }} activeOpacity={0.6}>
                    <Text style={styles.button}>Konfirmasi Terima Pembayaran</Text>
                </TouchableOpacity>
            )}
        </View >
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    title: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },
    imageStatus: {
        width: 100,
        height: 40
    },
    address: {
        marginLeft: 5, paddingRight: 25,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    rowItemRegular: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        color: color.black
    },
    rowItemBold: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },
    border: {
        borderTopColor: color.gray,
        borderTopWidth: 1
    },
    button: {
        padding: 15,
        backgroundColor: color.primary,
        color: color.white,
        textAlign: 'center',
        borderRadius: 8,
        // margin: 15,
        marginHorizontal: 15,
        marginVertical : 5,
        fontFamily: AppConfig.font.family.semiBold
    }
})

export default DetailPesanan