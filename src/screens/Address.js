import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, RefreshControl, Alert } from 'react-native';
import Button from '../components/Button';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Toast from '../components/Toast';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';
import { useIsFocused } from '@react-navigation/native';

export default function Address({ navigation }) {

    const [address, setAddress] = useState([])
    const [refresing, setRefresing] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const isFocus = useIsFocused()

    const getAddressList = useCallback(async () => {
        setIsLoading(true)
        try {
            let res = await HttpRequest.getAddressList()
            setAddress(res.data.data)
            setIsLoading(false)
            console.log("res", res)
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    })

    const setPrimary = useCallback(async (id) => {
        setIsLoading(true)
        try {
            let res = await HttpRequest.setPrimaryAddress({ id: id })
            Toast.showSuccess(res.data.message, 3000)
            getAddressList()
            console.log("res", res)
        } catch (error) {
            console.log("error", error)
            setIsLoading(false)
            Toast.showError(error.response.data.message, 3000)
        }
    })

    const deleteAddress = useCallback(async (id) => {
        try {
            let res = await HttpRequest.deleteAddress({ id: id })
            console.log("res", res)
            Toast.showSuccess(res.data?.message)
            getAddressList()
        } catch (error) {
            console.log("err", error, error.response)
            Toast.showError(error.response?.data?.message)
        }
    })

    useEffect(() => {
        getAddressList()
    }, [])
    useEffect(() => {
        if (isFocus) {
            getAddressList()
        }
    }, [isFocus])
    return (
        <View style={styles.main}>
            <HeaderBack
                title="Daftar Alamat"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refresing}
                        onRefresh={() => getAddressList()}
                    />
                }
            >

                {isLoading && (
                    <View style={{ padding: 10 }}>
                        <Placeholder.DaftarAlamat />
                    </View>
                )}

                {!isLoading && (
                    <>
                        {address.length == 0 && (
                            <NoData>Tidak ada data alamat</NoData>
                        )}
                        {address.map((item, index) => {
                            return (
                                <View key={index} style={{ padding: 15, borderBottomColor: color.grayText, borderBottomWidth: 1 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <Text style={styles.label}>{item?.label}</Text>
                                        {item?.is_primary == 1 && (
                                            <MaterialIcons name='check-circle' size={20} color={color.primary} />
                                        )}
                                        {item?.is_primary == 0 && (
                                            <TouchableOpacity onPress={() => {
                                                Alert.alert("Alamat Utama", "Apakah anda yakin ingin mengganti alamat utama anda?", [
                                                    {
                                                        text: "Tidak",
                                                        style : 'cancel'
                                                    },
                                                    {
                                                        text: "Ya",
                                                        onPress: () => {
                                                            setPrimary(item.id)
                                                        }
                                                    }
                                                ], {cancelable : true})
                                            }}>
                                                <MaterialCommunityIcons name='circle-outline' size={20} color={color.primary} />
                                            </TouchableOpacity>
                                        )}
                                    </View>

                                    <View style={{ marginRight: 10, marginTop: 10 }}>
                                        <Text>{item.penerima}, {item.hp_penerima}</Text>
                                        <Text>{item.alamat_lengkap}, RT.{item.rt_rw.split("/")[0]} RW.{item.rt_rw.split("/")[1]}</Text>
                                        <Text>{item?.wilayahPropinsi?.nama}, {item.wilayahKabupaten?.nama}, {item?.wilayahKecamatan?.nama}, {item?.wilayahDesa?.nama}, {item.kode_pos}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginTop: 5 }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                Alert.alert("Hapus Alamat", "Apakah anda yakin akan menghapus alamat ini?", [
                                                    {
                                                        text: "tidak",
                                                        style: 'cancel'
                                                    },
                                                    {
                                                        text: "ya",
                                                        onPress: () => {
                                                            deleteAddress(item?.id)
                                                        }
                                                    }
                                                ], { cancelable: true })
                                            }}
                                            style={{ backgroundColor: color.danger, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 4, elevation: 2 }}>
                                            <Text style={{ color: color.white, }}>Hapus</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })}
                    </>
                )}

            </ScrollView>

            <Button onPress={() => navigation.navigate("AddAddress")} style={{ margin: 15 }}>Tambah</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    label: {
        fontFamily: AppConfig.font.family.medium,
        color: color.black
    }
})