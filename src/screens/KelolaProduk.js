import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Modal, Dimensions, ActivityIndicator , RefreshControl} from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import InputLabel from '../components/InputLabel';
import { HttpRequest } from '../utils/http';
import Toast from '../components/Toast';
import Button from '../components/Button';
import Shimmer from 'react-native-shimmer';
import Placeholder from '../components/Placeholder';

function Checkbox(props) {
    let img = AppConfig.CHECKBOX_IMG
    if (props.checked) {
        img = AppConfig.CHECKBOX_CHECKED_IMG
    }
    return (
        <TouchableOpacity {...props} activeOpacity={0.6} style={styles.rowItem}>
            <Image style={styles.checkboxImg} source={img} />
            {props.label && (
                <Text style={styles.checkboxLabel}>{props.label}</Text>
            )}
        </TouchableOpacity>
    )
}

function KelolaProduk({ navigation }) {

    const [editHarga, setEditHarga] = useState(false)
    const [katalog, setKatalog] = useState([])
    const [selectedProduk, setSelectedProduk] = useState({})
    const [hargaJual, setHargaJual] = useState(0)
    const [showAll, setShowAll] = useState(false)
    const [loading, setLoading] = useState(true)

    const getKatalogAgen = useCallback(async () => {
        setLoading(true)
        try {
            let res = await HttpRequest.getKatalogAgen()
            setKatalog(res.data.data)
            setLoading(false)
            console.log("res", res)
        } catch (error) {
            console.log('err', error, error.response)
            setLoading(false)
        }
    })

    const simpanKatalogProduk = useCallback(async () => {
        try {
            let data = katalog
            let res = await HttpRequest.simpanKatalogProduk({ produks: data })
            console.log("res", res)
            Toast.showSuccess(res.data.message)
            setEditHarga(false)
            getKatalogAgen()
        } catch (error) {
            console.log("err", error, error.response)
        }
    })

    // useEffect(() => {

    //     [...katalog].map((i) => {
    //         i.is_show = !showAll
    //     })
    //     setKatalog(katalog)
    // }, [showAll])

    useEffect(() => {
        console.log("katalog", katalog)
        // simpanKatalogProduk()
    }, [katalog])

    useEffect(() => {
        getKatalogAgen()
    }, [])


    const {width, height} = Dimensions.get('screen')
    return (
        <View style={styles.main}>
            <HeaderBack
                title="Kelola Produk"
                theme="light"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <Modal
                animationType='slide'
                visible={editHarga}
                transparent={true}
                onRequestClose={() => setEditHarga(false)}
            >
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <TouchableOpacity style={{ flex: 1, backgroundColor: color.blackOpacity }} activeOpacity={0.8} onPress={() => setEditHarga(false)}>
                        <View style={{ flex: 1, }} />
                    </TouchableOpacity>
                    <View style={{ height: height / 2, padding: 15, backgroundColor: color.white }}>
                        <View style={styles.rowItem}>
                            <TouchableOpacity activeOpacity={0.6} onPress={() => setEditHarga(false)}>
                                <MaterialCommunityIcons name='close' size={25} color={color.black} />
                            </TouchableOpacity>
                            <Text style={{ fontFamily: AppConfig.font.family.bold, color: color.black, marginLeft: 6, fontSize: AppConfig.font.size.medium }}>Atur Harga Jual</Text>
                        </View>

                        <ScrollView>
                            <Text style={{ marginVertical: 25, fontFamily: AppConfig.font.family.bold, color: color.black, }}>Detail Produk</Text>
                            <View style={styles.rowItemBetween}>
                                <Text style={styles.textDefault}>Nama Produk</Text>
                                <Text style={[styles.textDefault, { width: 200, textAlign: "right" }]}>{selectedProduk?.nama}</Text>
                            </View>
                            <View style={[styles.rowItemBetween, {marginTop : 8}]}>
                                <Text style={styles.textDefault}>Harga Grosir</Text>
                                <Text style={styles.textDefault}>{selectedProduk?.harga_grosir_rupiah}</Text>
                            </View>

                            <InputLabel
                                containerStyle={{ marginTop: 15 }}
                                label="Harga Jual"
                                placeholder="Rp."
                                value={hargaJual}
                                onChangeText={(e) => setHargaJual(e)}
                                keyboardType="numeric"
                            />
                        </ScrollView>

                        <Button onPress={() => {
                            let index = katalog.findIndex((i) => i.id === selectedProduk.id)
                            if (index != -1) {
                                katalog[index].harga_jual = parseInt(hargaJual)
                                console.log(katalog)
                                simpanKatalogProduk()
                            }
                        }} style={{marginVertical : 15}}>Simpan</Button>
                    </View>
                </View>
            </Modal>

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={loading}
                        onRefresh={() => getKatalogAgen()}
                    />
                }
            >

                {loading && (
                    <View style={{ padding: 15 }}>
                        <Placeholder.Transaksi />
                    </View>
                )}

                {!loading && (
                    <>
                        <Text style={styles.headerInfo}>
                            Centang untuk menampilkan produk pada katalog
                        </Text>

                        <View style={{ padding: 15 }}>
                            {/* <Checkbox
                        label="Tampilkan Semua"
                        checked={showAll}
                        onPress={() => setShowAll(!showAll)}
                    /> */}
                        </View>

                        {katalog.map((item, index) => {
                            return (
                                <View key={index} style={styles.itemContainer}>
                                    <View style={styles.rowItem}>
                                        <Checkbox
                                            checked={item.is_show}
                                            onPress={() => {
                                                katalog[index].is_show = !item.is_show
                                                setKatalog([...katalog])
                                                simpanKatalogProduk()
                                            }}
                                        />
                                        <View style={styles.katalogContainer}>
                                            <Image style={styles.katalogImg} source={{ uri: item.gambar }} />

                                            <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'column', height: 80 }}>
                                                <Text style={styles.productName}>{item.nama}</Text>

                                                <View style={styles.rowItem}>
                                                    <View style={{ flex: 1 }}>
                                                        <Text style={styles.textNormal}>Harga Grosir</Text>
                                                        <Text style={styles.textNormal}>{item.harga_grosir_rupiah}</Text>
                                                    </View>
                                                    <View style={{ alignItems: 'flex-end' }}>
                                                        <Text style={styles.textNormal}>Harga Jual</Text>
                                                        {item.harga_jual == 0 && (
                                                            <TouchableOpacity activeOpacity={0.6} onPress={() => {
                                                                setSelectedProduk(item)
                                                                setHargaJual(item.harga_jual)
                                                                setEditHarga(true)
                                                            }}>
                                                                <Text style={[styles.textBold, { color: color.primary }]}>Edit Harga</Text>
                                                            </TouchableOpacity>
                                                        )}
                                                        {item.harga_jual != 0 && (
                                                            <TouchableOpacity activeOpacity={0.6} onPress={() => {
                                                                setSelectedProduk(item)
                                                                setHargaJual(item.harga_jual)
                                                                setEditHarga(true)
                                                            }}>
                                                                <Text style={styles.textBold}>{item.harga_jual_rupiah}</Text>
                                                            </TouchableOpacity>
                                                        )}
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            )
                        })}
                    </>
                )}

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    headerInfo: {
        color: color.black,
        fontFamily: AppConfig.font.family.regular,
        padding: 15,
        backgroundColor: '#E9CCFF'
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rowItemBetween: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textDefault: {
        color: color.black,
        fontFamily: AppConfig.font.family.regular,
        marginBottom: 5
    },
    checkboxImg: {
        width: 20,
        height: 20
    },
    checkboxLabel: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.default,
        marginHorizontal: 8
    },
    katalogContainer: {
        flex: 1, marginLeft: 8,
        flexDirection: 'row', alignItems: 'center'
    },
    katalogImg: {
        width: 80,
        height: 80,
        borderRadius: 8,
        marginRight: 8
    },
    productName: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
        color: color.black
    },
    textNormal: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
    },
    textBold: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.semiBold,
    },
    itemContainer: {
        borderBottomColor: color.border,
        borderBottomWidth: 1,
        padding: 15,
    }
})

export default KelolaProduk