import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TextInput,TouchableOpacity, Image, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import Button from '../components/Button';
import Toast from '../components/Toast';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

function SignIn(props) {

    const user = useSelector((state) => state.user)
    const navigation = useNavigation()
    const [phone, setPhone] = useState("")
    const [isLoading, setIsloading] = useState(false)

    useEffect(() => {
        if (user?.token) {
            navigation.popToTop()
            navigation.navigate("Dashboard")
        }
        console.log("user data", user)
    }, [])
    
    const actionLogin = useCallback(() => {

        if (phone == "") {
            Toast.showError("Nomor Hp Tidak Boleh Kosong", 3000)
            return
        }

        setIsloading(true)
        HttpRequest.login(phone).then((res) => {
            console.log("res", res)

            setIsloading(false)
            navigation.navigate("Verifikasi", { phone })
        }).catch((err) => {
            console.log("err", err, err.response)
            setIsloading(false)
            Toast.showError(err.response?.data?.message, 3000)
        })
    })

    return (
        <View style={styles.main}>
            <View style={styles.container}>
                <Image source={require("../assets/logo.png")} style={{ width: 150, height: 150 }} />

                <Text style={[styles.bold, { fontSize: 16, color: color.black }]}>Masuk menggunakan nomor hp</Text>
                <Text style={{ fontSize: 12, color: color.black }}>Masukkan nomor hp aktif kamu</Text>

                <Text style={{ fontSize: 12, color: color.black, fontWeight: 'bold', marginTop: 20 }}>Nomor Hp</Text>
                <View style={[styles.row, {}]}>
                    <Image resizeMode='contain' source={require("../assets/icon/kode-negara.png")} style={{ width: 60, height: 30 }} />
                    <TextInput
                        placeholder='8xxxxxx'
                        style={styles.inputPhoneNumber}
                        value={phone}
                        onChangeText={(phone) => setPhone(phone)}
                        keyboardType="number-pad"
                    />
                </View>

                <Button loading={isLoading} onPress={() => actionLogin()} style={{marginVertical : 15}}>Masuk</Button>

                <Text style={{ fontSize: 12, color: '#616161', marginRight: 80 }}>Dengan masuk, Anda menyetujui Persyaratan Layanan dan Kebijakan Privasi kami.</Text>

                <TouchableOpacity activeOpacity={0.6} onPress={() => {
                    navigation.navigate("Register")
                }}>
                    <Text style={styles.registerText}>Belum punya akun?</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    container: {
        marginVertical: 40,
        marginHorizontal: 20
    },
    bold: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputPhoneNumber: {
        borderBottomWidth: 1,
        borderBottomColor: '#C4C4C4',
        marginLeft: 5,
        flex: 1,
        paddingVertical: 3,
        marginBottom: 5,
        fontSize : 16
    },
    btn: {
        paddingHorizontal: 15,
        paddingVertical: 12,
        backgroundColor: color.primary,
        alignItems: 'center',
        borderRadius: 8,
        marginTop: 20,
        marginBottom : 15
    },
    registerText: {
        textDecorationLine: 'underline',
        marginTop: 15
    }
})

export default SignIn