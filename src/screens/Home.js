import moment from 'moment';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    StyleSheet,
    Dimensions,
    ActivityIndicator,
    RefreshControl
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import Shimmer from 'react-native-shimmer';
import { useDispatch, useSelector } from 'react-redux';
import Skeleton from 'skeleton-rn';
import BannerSlider from '../components/BannerSlider';
import ComponentHome from '../components/ComponentHome';
import FloatingCounter from '../components/FloatingCounter';
import { Header } from '../components/Header';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';
import ProdukDefault from '../components/ProdukDefault';
import TabBottom from '../components/TabBottom';
import TabHeader from '../components/TabHeader';
import { setCounter } from '../store/actions';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';


const { width, height } = Dimensions.get('screen')
export default function Home({ navigation }) {

    const user = useSelector((state) => state.user)
    const [produk, setProduk] = useState([])
    const [kategori, setKategori] = useState([])
    const [kategoriSelected, setKategoriSelected] = useState(null)
    const [isLoading, setLoading] = useState(true)

    const getProdukKategori = useCallback(async () => {
        setLoading(true)
        try {
            let res = await HttpRequest.getProdukKategori()
            console.log("res", res)
            setKategori(res.data.data)
            setKategoriSelected(res.data.data[0])
            setLoading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setLoading(false)
        }
    })

    const getProdukByKategori = useCallback(async () => {
        try {
            let param = { kategori_id: kategoriSelected?.id }
            console.log('param', param)
            let res = await HttpRequest.getProdukByKategori(param)
            setProduk(res.data.data)
            setLoading(false)
            console.log("res", res)
        } catch (error) {
            console.log('err', error, error.response)
        }
    })

    useEffect(() => {
        getProdukByKategori()
    }, [kategoriSelected])

    useEffect(() => {
        getProdukKategori()
    }, [])

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Header />

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={isLoading}
                        onRefresh={() => getProdukKategori()}
                    />
                }
            >
                <BannerSlider navigation={navigation} />
                <View style={{ padding: 10 }}>

                    {isLoading && (
                        <>
                            <Placeholder.KategoriProduk />
                            <View style={{ marginTop: 10 }}>
                                <Placeholder.DaftarProduk />
                            </View>
                        </>
                    )}

                    {!isLoading && (
                        <>
                            <View style={{ marginHorizontal: 3 }}>
                                <Text style={styles.title}>Produk Kami</Text>

                                <View style={{ marginVertical: 10 }}>
                                    <TabHeader
                                        items={kategori}
                                        selected={kategoriSelected}
                                        onChangeSelected={(e) => {
                                            setKategoriSelected(e)
                                            setLoading(true)
                                            console.log("e", e)
                                        }}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap' }}>
                                {produk.length == 0 && (
                                    <NoData>Produk tidak ditemukan</NoData>
                                )}
                                {produk.map((i, index) => {
                                    return (
                                        <View key={index} style={{ width: (width / 2) - 20, margin: 5 }}>
                                            <ProdukDefault
                                                item={i}
                                                onPress={() => {
                                                    navigation.navigate("DetailProduk", { item: i })
                                                }}
                                            />
                                        </View>
                                    )
                                })}
                            </View>
                        </>
                    )}
                </View>
            </ScrollView>

            <TabBottom
                selected={0}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#00FF00',
        padding: 10,
        marginTop: 10,
    },
    header: {
        backgroundColor: color.primary,
        paddingHorizontal: 15,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontSize: AppConfig.font.size.exLarge,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    }
});