import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, StyleSheet,Alert } from 'react-native';
import { useDispatch } from 'react-redux';
import Button from '../components/Button';
import { setUser } from '../store/actions';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

function Verifikasi({route, navigation}) {

    const dispatch = useDispatch()
    const [otp, setOtp] = useState("")
    const [isLoading, setIsloading] = useState(false)

    const actionVerifikasi = () => {
        let phone = route.params?.phone

        if (otp == "") {
            Alert.alert("Otp", "Mohon lengkapi kode otp")
            return
        }

        setIsloading(true)
        HttpRequest.verifikasiOtp(phone, otp).then((res) => {

            console.log("res", res)
            let data = res.data.data
            dispatch(setUser(data))
            setIsloading(false)

            // navigation.popToTop()
            // navigation.navigate("Dashboard")

            // navigation.navigate("ProfileDetail")
        }).catch((err) => {
            setIsloading(false)
            console.log("err", err, err.response)
            Alert.alert("Error", err.response?.data?.message)
        })
    }

    return (
        <View style={styles.main}>
            <View style={styles.container}>
                <Image source={require("../assets/logo.png")} style={{ width: 150, height: 150 }} />

                <Text style={[styles.bold, { fontSize: 16, color: color.black }]}>Verifikasi</Text>
                <Text style={{ fontSize: 12, color: color.black }}>Masukkan kode yang dikirimkan ke nomor kamu.</Text>

                <Text style={{ fontSize: 12, color: color.black, fontWeight: 'bold', marginTop: 20 }}>6 digit kode verifikasi</Text>
                <View style={[styles.row, {}]}>
                    <TextInput
                        placeholder='xxxxxx'
                        style={styles.inputPhoneNumber}
                        value={otp}
                        onChangeText={(otp) => setOtp(otp)}
                        keyboardType="number-pad"
                    />

                    {/* <Text style={{ color: color.primary, marginLeft : 10}}>05:07</Text> */}
                </View>

                <Button loading={isLoading} onPress={() => actionVerifikasi()} style={{marginVertical: 10}}>
                    Verifikasi
                </Button>
                {/* <TouchableOpacity onPress={() => actionVerifikasi()} activeOpacity={0.6} style={styles.btn}>
                    <Text style={{ color: color.white, fontSize: 16, fontWeight: 'bold' }}>Verifikasi</Text>
                </TouchableOpacity> */}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    container: {
        marginVertical: 40,
        marginHorizontal: 20
    },
    bold: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputPhoneNumber: {
        borderBottomWidth: 1,
        borderBottomColor: '#C4C4C4',
        flex: 1,
        paddingVertical: 3,
        marginBottom: 5,
        fontSize: 16
    },
    btn: {
        paddingHorizontal: 15,
        paddingVertical: 12,
        backgroundColor: color.primary,
        alignItems: 'center',
        borderRadius: 8,
        marginTop: 20,
        marginBottom: 15
    }
})

export default Verifikasi