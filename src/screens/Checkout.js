import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image } from 'react-native';
import { HeaderBack } from '../components/Header';
import { SelesaiPembayaran, SelesaiPemesanan } from '../components/Selesai';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ProdukCart from '../components/ProdukCart';
import MetodePembayaran from '../components/MetodePembayaran';
import { FooterItem } from '../components/Footer';
import { useSelector } from 'react-redux';
import Rupiah from '../components/Rupiah';
import { HttpRequest } from '../utils/http';
import { useIsFocused } from '@react-navigation/native';
import Toast from '../components/Toast';
import PilihKurir from '../components/PilihKurir';
import PilihPengiriman from '../components/PilihPengiriman';
import { ActivityIndicator } from 'react-native';
import InputLabel from '../components/InputLabel';

function Checkout({ navigation }) {

    const cartRedux = useSelector((state) => state.cartRedux)
    const [cart, setCart] = useState(cartRedux)
    const [address, setAddress] = useState("-")
    const [metodePembayaran, setMetodePembayaran] = useState([])
    const [isFetchingPembayaran, setFetchingPembayaran] = useState(true)
    const [isFetchingOngkir, setIsFetchingOngkir] = useState(false)
    const [selectedPembayaran, setSelectedPembayaran] = useState(null)
    const [kurirPengiriman, setKurirPengiriman] = useState(null)
    const [selectedKurir, setSelectedKurir] = useState(null)
    const [isLoadingCheckout, setIsLoadingCheckout] = useState(false)
    const [fetchingAddress, setFetchingAddress] = useState(true)
    const [isDropshipper, setIsDropshipper] = useState(false)
    const [dropshipperName, setDropshipperName] = useState('')
    const [dropshipperPhone, setDropshipperPhone] = useState('')
    const isFocused = useIsFocused()
    const agenRedux = useSelector((state) => state.agenRedux)

    useEffect(() => {
        console.log("agenRedux", agenRedux)
        console.log("cartRedux", cartRedux)

        // if (cartRedux) {
        //     let cart = []
        //     cartRedux.forEach((i) => {
        //         let obj = {...i}

        //         if (i.variant.length > 0) {
        //             let variantDipilih = i.variant.map((v) => v.qty > 0)
        //             i.variant = variantDipilih
        //         }

        //         cart.push(obj)
        //     })

        //     setCart([...cart])
        // }

        getMetodePembayaran()
        getAddressList()
    }, [])

    useEffect(() => {
        cekOngkir()
    }, [address])

    useEffect(() => {

        if (selectedPembayaran?.is_code == "1") {
            let kurir = null
            setSelectedKurir(kurir)
        }
    }, [selectedPembayaran])

    useEffect(() => {
        if (isFocused) {
            getAddressList()
        }
    }, [isFocused])

    const getTotalBelanja = useMemo(() => {
        let total = 0
        cart.forEach((i) => {
            if (i.qty != 0) {
                total += i.qty * i.harga_jual
            }
        })

        return total
    })

    const getTotalItem = useMemo(() => {
        let total = 0
        cart.forEach((i) => {
            if (i.qty != 0) {
                total += i.qty
            }
        })

        return total
    })

    const getAddressList = useCallback(async () => {
        try {
            setFetchingAddress(true)
            let res = await HttpRequest.getAddressList()
            if (res.data.data) {
                let primary = res.data.data.filter((i) => i.is_primary === 1)
                // console.log(primary)
                setAddress(primary[0])
                setFetchingAddress(false)
            }
        } catch (error) {
            console.log("err", error)
            setFetchingAddress(false)
        }
    }, [])

    const getMetodePembayaran = useCallback(async () => {
        try {
            let param = {
                agen_id: agenRedux?.id
            }

            let res = await HttpRequest.getMetodePembayaran(param)
            console.log("res getMetodePembayaran", res)
            setMetodePembayaran(res.data.data)
            setFetchingPembayaran(false)
        } catch (error) {
            setFetchingPembayaran(false)
            console.log("err", error, error.response)
        }
    }, [])

    const getParamCheckout = useMemo(() => {
        let details = []
        cart.forEach((i) => {
            i.subtotal_point_agen = i.point_agen * i.qty
            i.subtotal_point_reseller = i.point_reseller * i.qty
            i.subtotal = i.qty * i.harga_jual
            delete i.deskripsi
            details.push(i)
        })

        let subsidi = kurirPengiriman?.subsidi
        if (kurirPengiriman?.subsidi >= selectedKurir?.cost?.price?.value) {
            subsidi = selectedKurir?.cost?.price?.value
        }

        console.log("dropshipe", isDropshipper)

        let param = {
            seller_id: agenRedux?.id,
            address_id: address?.id,
            pembayaran_id: selectedPembayaran?.id,
            details: details,
            kurir: selectedKurir,
            subsidi: subsidi,
            is_dropshipper: isDropshipper ? 1 : 0,
            dropshipper_name: isDropshipper ? dropshipperName : "",
            dropshipper_phone: isDropshipper ? dropshipperPhone : ""
        }
        return param;
    }, [address, selectedPembayaran, selectedKurir, isDropshipper, dropshipperName, dropshipperPhone])

    const checkout = useCallback(async () => {
        try {

            let param = getParamCheckout
            console.log("param checkout", JSON.stringify(param))

            if (address?.id == undefined || selectedPembayaran?.id == null) {
                Toast.showError("Mohon pilih alamat utama anda terlebih dahulu", 5000)
                return
            }
            if (selectedPembayaran?.id == undefined || selectedPembayaran?.id == null) {
                Toast.showError("Mohon pilih metode pembayaran terlebih dahulu", 5000)
                return
            }

            if (selectedPembayaran?.is_code != "1") {
                if (selectedKurir?.service == undefined || selectedKurir?.service == null) {
                    Toast.showError("Mohon pilih metode pengiriman terlebih dahulu", 5000)
                    return
                }
            }

            if (isDropshipper) {
                if (dropshipperName == "") {
                    Toast.showError("Nama Pengirim tidak boleh kosong", 3000)
                    return
                }
                if (dropshipperPhone == "") {
                    Toast.showError("Nomor Telepon Pengirim tidak boleh kosong", 3000)
                    return
                }
            }

            setIsLoadingCheckout(true)
            let res = await HttpRequest.checkout(param)
            console.log("res", res)
            Toast.showSuccess(res.data.message, 5000)
            setIsLoadingCheckout(false)
            navigation.popToTop()
            navigation.navigate("Transaksi")
        } catch (error) {
            setIsLoadingCheckout(false)
            console.log("err", error, error.response)
            Toast.showError(error.response.data.message, 4000)
        }
    })

    const cekOngkir = useCallback(async () => {

        if (!address) {
            return false
        }
        if (address == "-") {
            return false
        }

        try {
            // let param = getParamCheckout

            let details = []
            cart.forEach((i) => {
                let obj = {
                    berat: i.berat,
                    qty: i.qty,
                    id: i.id
                }
                details.push(obj)
            })

            let param = {
                seller_id: agenRedux.id,
                address_id: address.id,
                details: details,
            }

            // console.log("getParamCheckout", getParamCheckout, address)
            setIsFetchingOngkir(true)
            setKurirPengiriman({ ...{} })
            setSelectedKurir(null)
            let res = await HttpRequest.cekOngkir(param)
            console.log("res cek ongkir", res)
            setKurirPengiriman(res.data.data)
            setIsFetchingOngkir(false)
            // console.log("res cek ongkir", JSON.stringify(res.data.data))
        } catch (error) {
            console.log("err", error, error.response)
            setIsFetchingOngkir(false)
            Toast.showError(error.response.data.message, 4000)
        }
    }, [address])

    const getSubsidi = useCallback(() => {
        let ongkir = selectedKurir?.cost?.price?.value ?? 0
        let subsidi = kurirPengiriman?.subsidi

        if (ongkir == 0) {
            return ongkir
        }
        if (subsidi >= ongkir) {
            return ongkir
        } else {
            return subsidi
        }
    })

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Checkout Pesanan"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView>
                <View style={{ padding: 15 }}>

                    {/* <View style={{ marginBottom: 10 }}>
                        <Text style={styles.title}>Status Pesanan</Text>
                        <Image style={styles.imageStatus} resizeMode="contain" source={AppConfig.pesanan.BELUM_DIBAYAR_IMG} />
                    </View> */}

                    <View style={{ marginBottom: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: color.white, elevation: 2, padding: 10, borderRadius: 8 }}>
                        <View style={{ flex: 1, marginRight: 15 }}>
                            <Text style={[styles.title, { marginBottom: 10 }]}>Alamat Pengiriman</Text>

                            {fetchingAddress && (
                                <ActivityIndicator size={18} color={color.primary} />
                            )}
                            {!fetchingAddress && (
                                <>
                                    {!address && (
                                        <TouchableOpacity onPress={() => {
                                            navigation.navigate("Address")
                                        }} activeOpacity={0.6} style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: 5 }}>
                                            <Ionicons name='location-sharp' size={20} color={color.primary} />
                                            <Text style={styles.address}>Alamat belum di set, ketuk untuk setting alamat</Text>
                                        </TouchableOpacity>
                                    )}
                                    {address && (
                                        <TouchableOpacity onPress={() => {
                                            navigation.navigate("Address")
                                        }} activeOpacity={0.6} style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: 5 }}>
                                            <Ionicons name='location-sharp' size={20} color={color.primary} />
                                            <Text style={styles.address}>{address?.alamat_lengkap} {address?.wilayahKecamatan?.nama}, {address?.wilayahKabupaten?.nama}, {address?.wilayahPropinsi?.nama} {address?.kode_pos}</Text>
                                        </TouchableOpacity>
                                    )}
                                </>
                            )}
                        </View>
                        <MaterialIcons size={20} color={color.black} name='keyboard-arrow-right' />
                    </View>

                    <View style={{ marginTop: 10 }}>
                        {cart?.map((item, index) => {
                            return (
                                <ProdukCart
                                    key={index}
                                    nama={item.nama}
                                    gambar={item.gambar}
                                    harga={Rupiah.format(item.harga_jual, false) + " x " + item.qty}
                                    showButton={false}
                                    variant={item.variant}
                                />
                            )
                        })}
                    </View>

                    <View style={{ marginTop: 15, backgroundColor: color.white, elevation: 2, padding: 10, borderRadius: 8 }}>
                        <Text style={styles.title}>Metode Pembayaran</Text>
                        <MetodePembayaran
                            metodePembayaran={metodePembayaran ?? []}
                            onChange={(e) => {
                                // console.log("e", e)
                                setSelectedPembayaran(e)
                            }}
                        />
                    </View>

                    {selectedPembayaran?.is_code != "1" && (
                        <View style={{ marginTop: 15, backgroundColor: color.white, elevation: 2, padding: 10, borderRadius: 8 }}>
                            <Text style={styles.title}>Pengiriman</Text>
                            <PilihPengiriman
                                data={kurirPengiriman?.kurir ?? []}
                                isLoading={isFetchingOngkir}
                                onChange={(e) => {
                                    // console.log('e', e)
                                    setSelectedKurir(e)
                                }}
                            />
                        </View>
                    )}

                    <View style={{ backgroundColor: color.white, elevation: 2, padding: 10, borderRadius: 8, marginTop: 15 }}>
                        <TouchableOpacity onPress={() => {
                            setIsDropshipper(!isDropshipper)
                        }} style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
                            <MaterialIcons name={isDropshipper ? 'check-box' : 'check-box-outline-blank'} size={20} color={isDropshipper ? color.primary : color.black} style={{ marginRight: 8 }} />
                            <Text style={styles.title}>Dropshipper</Text>
                        </TouchableOpacity>
                        {isDropshipper && (
                            <View>
                                <InputLabel
                                    value={dropshipperName}
                                    onChangeText={(e) => setDropshipperName(e)}
                                    label="Nama Pengirim"
                                    placeholder="Nama Pengirim..."
                                    containerStyle={{ marginBottom: 10 }}
                                />
                                <InputLabel
                                    value={dropshipperPhone}
                                    onChangeText={(e) => setDropshipperPhone(e)}
                                    label="Telepon Pengirim"
                                    placeholder="Telepon Pengirim..."
                                    keyboardType="numeric"
                                    containerStyle={{ marginBottom: 10 }}
                                />
                            </View>
                        )}
                    </View>

                    <View style={{ marginTop: 15, backgroundColor: color.white, elevation: 2, padding: 10, borderRadius: 8 }}>
                        <Text style={[styles.title, { marginBottom: 5 }]}>Detail Pesanan</Text>

                        <View style={styles.rowItem}>
                            <Text style={styles.rowItemRegular}>{getTotalItem} Item</Text>
                            <Text style={styles.rowItemRegular}>{Rupiah.format(getTotalBelanja, false)}</Text>
                        </View>
                        <View style={styles.rowItem}>
                            <Text style={styles.rowItemRegular}>Ongkos Kirim</Text>

                            {selectedPembayaran?.is_code == "1" && (
                                <Text style={styles.rowItemRegular}>*menunggu konfirmasi</Text>
                            )}
                            {selectedPembayaran?.is_code != "1" && (
                                <>
                                    {!selectedKurir && (
                                        <Text style={styles.rowItemRegular}>*menunggu konfirmasi</Text>
                                    )}
                                    {selectedKurir != null && (
                                        <>
                                            {isFetchingOngkir == false && (
                                                <Text style={styles.rowItemRegular}>{Rupiah.format(selectedKurir?.cost?.price?.value ?? 0, false)}</Text>
                                            )}
                                            {isFetchingOngkir && (
                                                <Text style={styles.rowItemRegular}>{"-"}</Text>
                                            )}
                                        </>
                                    )}
                                </>
                            )}

                        </View>
                        {selectedKurir != null && selectedKurir != {} && getSubsidi() != 0 && (
                            <View style={styles.rowItem}>
                                <Text style={styles.rowItemRegular}>Subsidi Ongkir</Text>
                                <Text style={styles.rowItemRegular}>- {Rupiah.format(getSubsidi() ?? 0, false)}</Text>
                            </View>
                        )}
                        <View style={styles.rowItem}>
                            <Text style={styles.rowItemBold}>Total</Text>
                            <Text style={styles.rowItemBold}>{Rupiah.format((getTotalBelanja + (selectedKurir?.cost?.price?.value ?? 0)) - (getSubsidi() ?? 0), false)}</Text>
                        </View>
                    </View>
                </View>
            </ScrollView >

            <FooterItem
                onPress={() => {
                    checkout()
                }}
                isLoading={isLoadingCheckout}
                totalPrice={Rupiah.format((getTotalBelanja + (selectedKurir?.cost?.price?.value ?? 0)) - (getSubsidi() ?? 0), false)}
                totalItem={getTotalItem + " Item"}
                labelButton="Checkout Pesanan"
            />
        </View >
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    title: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },
    imageStatus: {
        width: 100,
        height: 40
    },
    address: {
        marginLeft: 5, paddingRight: 25,
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    rowItemRegular: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        color: color.black
    },
    rowItemBold: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    }
})

export default Checkout