import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, RefreshControl, ActivityIndicator } from 'react-native';
import { Header } from '../components/Header';
import TabBottom from '../components/TabBottom';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { HttpRequest } from '../utils/http';
import { useDispatch } from 'react-redux';
import { setAgenRedux } from '../store/actions';
import Placeholder from '../components/Placeholder';

function Agen(props) {

    const navigation = useNavigation()
    const dispatch = useDispatch()
    const [agen, setAgen] = useState([])
    const [isRefresing, setIsRefresing] = useState(false)
    const [isLoading, setIsLoading] = useState(true)

    const getAgenPusat = useCallback(async () => {
        try {
            let data = await HttpRequest.getAgenPusat()
            if (data.data) {
                setAgen(data.data.data)
                setIsLoading(false)
            }
            console.log("data pusat", data)
        } catch (error) {
            console.log("err", err)
        }
    })

    useEffect(() => {
        getAgenPusat()
    }, [])

    return (
        <View style={styles.main}>
            <Header />

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={isRefresing}
                        onRefresh={() => {
                            getAgenPusat()
                            setIsLoading(true)
                        }}
                    />
                }
            >
                <View style={{ padding: 15 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}}>
                        <Text style={styles.title}>Agen Kami</Text>
                        {/* <Text>Filter</Text> */}
                    </View>

                    {isLoading && (
                        <View style={{ marginTop: 15 }}>
                            <Placeholder.Agen />
                        </View>
                    )}

                    <View style={{ marginTop: 15 }} />
                    {!isLoading && (
                        agen?.map((item, key) => {
                            return (
                                <TouchableOpacity key={key} activeOpacity={0.6} onPress={() => {
                                    dispatch(setAgenRedux(item))
                                    navigation.navigate("AgenStore", { item })
                                }} style={styles.item}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.type}>{item.store_label}</Text>
                                    </View>
                                    <Text style={styles.name}>{item.store_name}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <Ionicons name='location-sharp' size={20} color={color.grayText} />
                                        <Text>{item.store_city}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    )}
                </View>
            </ScrollView>

            <TabBottom
                selected={1}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    title: {
        fontSize: AppConfig.font.size.exLarge,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },

    item: {
        padding: 10,
        // borderWidth: 1.5,
        // borderColor: '#E0E0E0',
        borderRadius: 12,
        marginBottom: 10,
        backgroundColor: color.white,
        elevation:2
    },
    type: {
        backgroundColor: '#E9CCFF',
        borderRadius: 100,
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: color.black,
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.medium,
    },
    name: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        marginVertical: 8, marginHorizontal: 8
    }
})

export default Agen