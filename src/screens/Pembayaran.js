import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, Modal, Alert, ActivityIndicator } from 'react-native';
import { HeaderBack } from '../components/Header';
import InputLabel from '../components/InputLabel';
import Rupiah from '../components/Rupiah';
import { SelesaiPembayaran } from '../components/Selesai';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
// import Clipboard from '@react-native-clipboard/clipboard';
import Toast from '../components/Toast';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import { HttpRequest } from '../utils/http';
import axios from 'axios';
import Button from '../components/Button';

function Pembayaran({ navigation, route }) {

    const [visible, setVisible] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [isUploading, setIsUploading] = useState(false)
    const [modalPemesan, setModalPemesan] = useState(false)
    const [buktiPembayaran, setBuktiPembayaran] = useState(null)
    const [fileBuktiPembayaran, setFileBuktiPembayaran] = useState(null)

    const transaksi = route?.params?.transaksi
    useEffect(() => {
        // console.log(route)
    }, [])

    const copyClipboard = async (string) => {
        // await Clipboard.setString(string)
        Toast.showSuccess("Berhasil disalin", 300)
    }

    const pickImage = useCallback(() => {
        ImagePicker.openPicker({
            includeBase64: true
        }).then(image => {
            console.log(image);

            setIsUploading(true)
            HttpRequest.uploadImage({ photo: image.data, path: "bukti-pembayaran" }).then((res) => {
                console.log("res", res)
                setFileBuktiPembayaran(res.data.data)
                Toast.showSuccess("Upload Bukti Pembayaran Berhasil, Silahkan klik tombol kirim bukti pembayaran", 4000)
                setIsUploading(false)
            }).catch((err) => {
                console.log("err", err, err.response)
                Toast.showError(err.response.data.message)
                setIsUploading(false)
            })
            setBuktiPembayaran(image)
        });
    })

    const konfirmasiPembayaran = useCallback(async () => {
        try {
            let param = {
                transaksi_id: transaksi.id,
                pembayaran: fileBuktiPembayaran?.filename
            }

            console.log("param", param)
            if (!fileBuktiPembayaran?.filename) {
                Toast.showError("Bukti Pembayaran tidak boleh kosong")
                return false
            }

            setIsLoading(true)
            let res = await HttpRequest.konfirmasiPembayaran(param)
            console.log('res', res)
            setIsLoading(true)
            Toast.showSuccess(res.data.message, 3000)
            navigation.navigate("Transaksi")
        } catch (error) {
            setIsLoading(true)
            Toast.showSuccess(error?.response?.data?.message ?? "Mohon ulangi, \nGagal upload bukti pembayaran", 3000)
            console.log("err", error, error.response)
        }
    })

    return (
        <View style={styles.main}>

            <HeaderBack
                title="Menunggu Pembayaran"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <Modal
                animationType='slide'
                visible={modalPemesan}
                transparent={false}
                onRequestClose={() => setModalPemesan(false)}
            >

                <View style={{ flex: 1 }}>
                    <HeaderBack
                        title="Data Pemesan"
                        onPress={() => setModalPemesan(false)}
                    />
                    <ScrollView>
                        <View style={{ padding: 15 }}>
                            <InputLabel
                                label="Nama"
                                placeholder="Masukkan nama kamu"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Nomor whatsapp"
                                placeholder="Masukkan nomor kamu"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Kirim ke alamat"
                                placeholder="Masukkan alamat lengkap"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Catatan untuk agen"
                                placeholder="Tulis catatan disini"
                                multiline={true}
                                numberOfLines={5}
                                containerStyle={{ marginBottom: 20 }}
                                inputStyle={{
                                    borderWidth: 1,
                                    borderColor: '#C6C6C6',
                                    paddingVertical: 3,
                                    color: color.black,
                                    borderRadius: 8,
                                    marginTop: 10,
                                    padding: 10
                                }}
                            />
                        </View>
                    </ScrollView>

                    <TouchableOpacity onPress={() => setModalPemesan(false)} activeOpacity={0.6}>
                        <Text style={styles.buttonSimpanPemesan}>Simpan</Text>
                    </TouchableOpacity>
                </View>
            </Modal>

            <SelesaiPembayaran
                visible={visible}
                onRequestClose={() => {
                    setVisible(false)
                }}
            />

            <ScrollView>
                {/* <Text style={styles.headerInfo}>
                    Lakukan pembayaran dalam waktu
                    <Text style={styles.headerInfoBold}> 59h 11m</Text>
                </Text> */}

                <View style={{ padding: 15 }}>
                    <Text style={styles.title}>Metode pembayaran</Text>
                    <View style={styles.rowItem}>
                        <Text style={styles.itemValue}>Transfer {transaksi?.rekening_bank}</Text>
                    </View>

                    <Text style={styles.title}>Nomor Rekening</Text>
                    <View style={styles.rowItem}>
                        <Text style={styles.itemValue}>{transaksi?.rekening_nomor}</Text>
                        <TouchableOpacity disabled onPress={() => copyClipboard(transaksi?.rekening_nomor)}>
                            <Text style={styles.itemSecondary}>Salin</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.title}>Atas Nama</Text>
                    <View style={styles.rowItem}>
                        <Text style={styles.itemValue}>{transaksi?.rekening_atas_nama}</Text>
                    </View>

                    <Text style={styles.title}>Total pembayaran</Text>
                    <View style={styles.rowItem}>
                        <Text style={styles.itemValue}>{Rupiah.format(transaksi?.total_tagihan)}</Text>
                        <TouchableOpacity disabled onPress={() => copyClipboard(transaksi?.total_tagihan)}>
                            <Text style={styles.itemSecondary}>Salin</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.title}>Bukti Pembayaran</Text>
                    {buktiPembayaran == null && (
                        <TouchableOpacity onPress={() => pickImage()} style={{ padding: 15, marginVertical: 15, alignItems: 'center', justifyContent: 'center', height: 150, borderRadius: 5, borderWidth: 1, borderColor: color.border }}>
                            <Ionicons name='camera' size={30} color={color.black} />
                        </TouchableOpacity>
                    )}
                    {buktiPembayaran != null && (
                        <TouchableOpacity onPress={() => pickImage()} style={{ padding: 15, marginVertical: 15, alignItems: 'center', justifyContent: 'center', height: 200, borderRadius: 5, borderWidth: 1, borderColor: color.border }}>
                            <Image
                                source={{ uri: buktiPembayaran.path }}
                                resizeMode="cover"
                                style={{
                                    width: '100%',
                                    height: '100%',
                                }}
                            />
                        </TouchableOpacity>
                    )}
                </View>
            </ScrollView>

            <View style={styles.border} />

            {/* <TouchableOpacity onPress={() => {
                // setVisible(true)
                Alert.alert("Konfirmasi Pembayaran", "Apakah anda yakin melakukan konfirmasi pembayaran ini?", [
                    {
                        text: "Tidak",
                        style: 'cancel'
                    },
                    {
                        text: "Ok",
                        onPress: () => {
                            konfirmasiPembayaran()
                        }
                    }
                ], { cancelable: true })
            }} activeOpacity={0.6}>
                <Text style={styles.button}>Kirim Bukti Pembayaran</Text>
            </TouchableOpacity> */}

            {isUploading && (
                <View style={[styles.button, {flexDirection : 'row', alignItems : 'center'}]}>
                    <ActivityIndicator size={"small"} color={color.white} />
                    <Text style={{marginLeft : 8, color : color.white, fontWeight : 'bold'}}>Uploading ....</Text>
                </View>
            )}

            {!isUploading && (
                <Button
                    onPress={() => {
                        Alert.alert("Konfirmasi Pembayaran", "Apakah anda yakin melakukan konfirmasi pembayaran ini?", [
                            {
                                text: "Tidak",
                                style: 'cancel'
                            },
                            {
                                text: "Ok",
                                onPress: () => {
                                    konfirmasiPembayaran()
                                }
                            }
                        ], { cancelable: true })
                    }}
                    loading={isLoading}
                    style={{ margin: 10 }}>
                    Kirim Bukti Pembayaran
                </Button>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    headerInfo: {
        color: color.black,
        fontFamily: AppConfig.font.family.regular,
        padding: 15,
        backgroundColor: '#E9CCFF'
    },
    headerInfoBold: {
        fontFamily: AppConfig.font.family.bold
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'space-between',
        marginTop: 3,
        marginBottom: 15
    },
    title: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        color: color.black
    },
    itemValue: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.primary
    },
    itemSecondary: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.medium,
        color: color.primary
    },
    border: {
        borderTopColor: color.gray,
        borderTopWidth: 1
    },
    button: {
        padding: 15,
        backgroundColor: color.primary,
        color: color.white,
        textAlign: 'center',
        borderRadius: 8,
        margin: 15,
        fontFamily: AppConfig.font.family.semiBold
    }
})

export default Pembayaran