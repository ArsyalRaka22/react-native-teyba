import React, { useCallback, useEffect, useState, useMemo } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ActivityIndicator, ScrollView, FlatList } from 'react-native';
import { HeaderBack } from '../components/Header';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';
import Constant from '../utils/Constant';
import { useDispatch, useSelector } from 'react-redux';

export default function ReferralAgen({ navigation }) {


    const [dataReferral, setDataReferral] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const user = useSelector((state) => state.user)

    const getDataReferral = useCallback(async () => {
        try {
            let res = await HttpRequest.getReferral()
            console.log('res', res)
            setDataReferral(res.data.data)
            setIsLoading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    })


    useEffect(() => {
        getDataReferral()
    }, [])

    const getMenu = useMemo(() => {
        if (user?.user?.role_id != Constant.Role.AGEN) {
            return PROFILE_MENU
        }
    })

    return (
        <View style={styles.main}>
            <HeaderBack
                // theme="light"
                title="Referral Agen"
                onPress={() => navigation.goBack()}
            />

            {!isLoading && (
                <>
                    {
                        user?.user?.role_id == Constant.Role.AGEN && (
                            dataReferral.length == 0 && (
                                <NoData>Data tidak ditemukan</NoData>
                            )
                        )
                    }
                </>
            )}

            <ScrollView>

                {isLoading && (
                    <>
                        {
                            user?.user?.role_id == Constant.Role.AGEN && (
                                <View style={{ padding: 15 }}>
                                    <Placeholder.ReferralAgen />
                                </View>
                            )
                        }
                    </>
                )}

                {!isLoading && (
                    <>
                        {
                            user?.user?.role_id == Constant.Role.AGEN && (
                                <>
                                    {
                                        dataReferral?.map((item, index) => {
                                            return (
                                                <View key={index} style={styles.item}>
                                                    <Image source={{ uri: item.photo }} style={{ height: 60, width: 60, borderRadius: 100, backgroundColor: color.primary, borderWidth: 2, borderColor: color.primary }} />

                                                    <View style={{ marginHorizontal: 10 }}>
                                                        {item.nama && (
                                                            <Text>{item.nama}</Text>
                                                        )}
                                                        {item.phone && (
                                                            <Text>{item.phone}</Text>
                                                        )}
                                                        {item.kode && (
                                                            <Text>Kode Member : {item.kode}</Text>
                                                        )}
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                </>
                            )
                        }
                    </>

                )}

                {
                    getMenu?.map((item, index) => {
                        return (
                            <View key={index} style={styles.item}>
                                <Image source={{ uri: item.photo }} style={{ height: 60, width: 60, borderRadius: 100, backgroundColor: color.primary, borderWidth: 2, borderColor: color.primary }} />

                                <View style={{ marginHorizontal: 10 }}>
                                    {item.nama && (
                                        <Text>{item.nama}</Text>
                                    )}
                                    {item.phone && (
                                        <Text>{item.phone}</Text>
                                    )}
                                    {item.kode && (
                                        <Text>Kode Member : {item.kode}</Text>
                                    )}
                                </View>
                            </View>
                        )
                    })
                }
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: color.border,
        padding: 15
    }
})

const PROFILE_MENU = [
    {
        nama: "dummy nama",
        phone: "dummy phone",
        photo: "",
        kode: "dummy kode"
    },
    {
        nama: "dummy nama",
        phone: "dummy phone",
        photo: "dummy photo",
        kode: "dummy kode"
    },
    {
        nama: "dummy nama",
        phone: "dummy phone",
        photo: "dummy photo",
        kode: "dummy kode"
    },
]