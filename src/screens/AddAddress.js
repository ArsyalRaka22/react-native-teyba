import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Button from '../components/Button';
import Combobox from '../components/Combobox';
import { HeaderBack } from '../components/Header';
import InputLabel from '../components/InputLabel';
import Toast from '../components/Toast';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

export default function AddAddress({ navigation }) {

    const [dataPropinsi, setPropinsi] = useState([])
    const [dataKota, setDataKota] = useState([])
    const [dataKecamatan, setDataKecamatan] = useState([])
    const [dataDesa, setDataDesa] = useState([])
    const [selectedPropinsi, setSelectedPropinsi] = useState(null)
    const [selectedKota, setSelectedKota] = useState(null)
    const [selectedKecamatan, setSelectedKecamatan] = useState(null)
    const [selectedDesa, setSelectedDesa] = useState(null)
    const [label, setLabel] = useState('')
    const [penerima, setPenerima] = useState('')
    const [phone, setPhone] = useState('')
    const [alamatLengkap, setAlamatLengkap] = useState('')
    const [rt, setRt] = useState('')
    const [rw, setRw] = useState('')
    const [kodePos, setKodePos] = useState('')

    const getDataKota = useCallback(() => {
        console.log("selectedPropinsi", selectedPropinsi)
        HttpRequest.kabupaten(selectedPropinsi).then((res) => {
            console.log("res", res)
            setDataKota(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })
    const getDataPropinsi = useCallback(() => {
        HttpRequest.propinsi().then((res) => {
            console.log("res", res)
            setPropinsi(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })
    const getDataKecamatan = useCallback(() => {
        HttpRequest.kecamatan(selectedKota).then((res) => {
            console.log("res", res)
            setDataKecamatan(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })

    const getDataDesa = useCallback(() => {
        HttpRequest.desa(selectedKecamatan).then((res) => {
            console.log("res", res)
            setDataDesa(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })

    // useEffect(() => {
    //     getDataKota()
    // }, [selectedPropinsi])

    // useEffect(() => {
    //     getDataKecamatan()
    // }, [selectedKota])

    // useEffect(() => {
    //     getDataDesa()
    // }, [selectedKecamatan])

    useEffect(() => {
        getDataPropinsi()
    }, [])

    const saveAddress = useCallback(async () => {

        let error = []
        if (label == "") {
            error.push("Label alamat belum di isi")
        }
        if (penerima == "") {
            error.push("Penerima belum di isi")
        }
        if (phone == "") {
            error.push("Hp Penerima belum di isi")
        }
        if (alamatLengkap == "") {
            error.push("Alamat Lengkap belum di isi")
        }
        if (rt == "") {
            error.push("RT belum di isi")
        }
        if (rw == "") {
            error.push("RW belum di isi")
        }
        if (kodePos == "") {
            error.push("Kode Pos belum di isi")
        }
        if (selectedPropinsi == "" || selectedPropinsi == null) {
            error.push("Propinsi belum di isi")
        }
        if (selectedKota == "" || selectedKota == null) {
            error.push("Kabupaten / Kota belum di isi")
        }
        if (selectedKecamatan == "" || selectedKecamatan == null) {
            error.push("Kecamatan belum di isi")
        }
        if (selectedDesa == "" || selectedDesa == null) {
            error.push("Desa belum di isi")
        }

        if (error.length > 0) {
            let messages = error.join("\n")
            Toast.showError(messages, error.length * 1000)
            return
        }

        try {
            let param = {
                label, penerima,
                alamat_lengkap: alamatLengkap,
                hp_penerima: phone,
                rt_rw: rt + "/" + rw,
                kode_pos: kodePos,
                wilayah_desa_id: selectedDesa,
                wilayah_kecamatan_id: selectedKecamatan,
                wilayah_kabupaten_id: selectedKota,
                wilayah_propinsi_id: selectedPropinsi,
            }
            console.log("param", param)
            let res = await HttpRequest.saveAddress(param)
            console.log("res", res)
            Toast.showSuccess(res.data.message, 5000)
            navigation.goBack()
        } catch (error) {
            console.log("err", error, error.response)
            Toast.showError(error.response.data.message, 5000)
        }
    })

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Tambah Alamat"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView>
                <View style={{padding : 15}}>
                    <InputLabel
                        label="Label Alamat"
                        placeholder="Label"
                        value={label}
                        onChangeText={(e) => setLabel(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Nama Penerima"
                        placeholder="Penerima"
                        value={penerima}
                        onChangeText={(e) => setPenerima(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Nomor Handphone Penerima"
                        placeholder="08xxxxx"
                        value={phone}
                        keyboardType="numeric"
                        onChangeText={(e) => setPhone(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Alamat Lengkap"
                        placeholder="alamat lengkap"
                        value={alamatLengkap}
                        onChangeText={(e) => setAlamatLengkap(e)}
                        containerStyle={styles.input}
                        multiline={true}
                        numberOfLines={5}
                    />
                    <View style={{ flexDirection: 'row', alignItems : 'center', flex : 1, marginBottom: 10}}>
                        <InputLabel
                            label="RT"
                            placeholder="01"
                            value={rt}
                            onChangeText={(e) => setRt(e)}
                            containerStyle={{flex : 1}}
                        />
                        <Text style={{marginHorizontal : 10}}>/</Text>
                        <InputLabel
                            label="RW"
                            placeholder="01"
                            value={rw}
                            onChangeText={(e) => setRw(e)}
                            containerStyle={{ flex: 1 }}
                        />
                    </View>
                    <InputLabel
                        label="Kode Pos"
                        placeholder="kode pos"
                        value={kodePos}
                        onChangeText={(e) => setKodePos(e)}
                        containerStyle={styles.input}
                    />

                    <View>
                        <Text style={styles.label}>{"Propinsi"}</Text>
                        <Combobox
                            data={dataPropinsi ?? []}
                            labelKey="nama"
                            selectedValue={selectedPropinsi}
                            onValueChange={(e) => {
                                console.log("propinsi", e)
                                setSelectedPropinsi(e)
                                getDataKota()
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Kabupaten"}</Text>
                        <Combobox
                            data={dataKota ?? []}
                            labelKey="nama"
                            selectedValue={selectedKota}
                            onValueChange={(e) => {
                                console.log("kabupaten", e)
                                setSelectedKota(e)
                                getDataKecamatan()
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Kecamatan"}</Text>
                        <Combobox
                            data={dataKecamatan ?? []}
                            labelKey="nama"
                            selectedValue={selectedKecamatan}
                            onValueChange={(e) => {
                                console.log("kecamatan", e)
                                setSelectedKecamatan(e)
                                getDataDesa()
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Desa"}</Text>
                        <Combobox
                            data={dataDesa ?? []}
                            labelKey="nama"
                            selectedValue={selectedDesa}
                            onValueChange={(e) => {
                                setSelectedDesa(e)
                                console.log("desa", e)
                            }}
                        />
                    </View>
                </View>
            </ScrollView>

            <Button onPress={() => saveAddress()} style={{ margin: 15 }}>Simpan</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    label: {
        fontFamily: AppConfig.font.family.medium,
        color: color.black
    },
    input: {
        marginBottom: 15
    }
})