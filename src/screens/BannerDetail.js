import React, { useCallback, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, ScrollView, Image } from 'react-native';
import RenderHTML from 'react-native-render-html';
import { HeaderBack } from '../components/Header';

function BannerDetail({route, navigation}) {

    const detail = route.params?.item
    useEffect(() => {
        // let param = route.params
        // console.log("detail", param)
    }, [])

    const {width, height} = Dimensions.get('window')
    return (
        <View style={styles}>
            <HeaderBack
                title={detail?.judul ?? "-"}
                onPress={() => navigation.goBack()}
            />

            <ScrollView>
                <View style={{ padding: 15 }}>
                    <Image source={{uri : detail?.image}} style={{width : '100%', height : 200, marginBottom: 8, borderRadius : 4}} />
                    <RenderHTML
                        contentWidth={width - 30}
                        source={{ html: detail?.deskripsi }}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex : 1
    }
})

export default BannerDetail