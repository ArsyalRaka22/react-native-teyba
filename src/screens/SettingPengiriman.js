import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Switch , ScrollView, ActivityIndicator} from 'react-native';
import Button from '../components/Button';
import { HeaderBack } from '../components/Header';
import NoData from '../components/NoData';
import Toast from '../components/Toast';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

function SettingPengiriman({navigation}) {

    const [courier, setCourier] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [isSubmit, setIsSubmit] = useState(false)

    useEffect(() => {
        getList()
    },[])

    const getList = useCallback(async () => {
        setIsLoading(true)
        try {
            let res = await HttpRequest.pengirimanList();
            console.log("res", res)
            setCourier([...res.data.data])
            setIsLoading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    }, [])
    
    const simpanPengirimanUser = useCallback(async () => {
        setIsSubmit(true)
        try {
            let res = await HttpRequest.simpanPengirimanUser({ courier })
            console.log("res",res)
            Toast.showSuccess("Simpan pengiriman berhasil")
            getList()
            setIsSubmit(false)
        } catch (error) {
            console.log("err", error, error.response)
            Toast.showError("Simpan pengiriman gagal")
            setIsSubmit(false)
        }
    }, [courier])

    return (
        <View style={styles.main}>
            <HeaderBack
                onPress={() => navigation.goBack()}
                theme="light"
                title = "Setting Pengiriman"
            />

            {isLoading && (
                <ActivityIndicator size={"small"} color={color.primary} style={{ margin: 20 }} />
            )}

            <ScrollView>
                {courier.length == 0 && (
                    <NoData>Kurir pengiriman tidak tersedia</NoData>
                )}
                {courier.length > 0 && (
                    courier.map((item, index) => {
                        return (
                            <View key={index} style={{ padding: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: color.white, marginTop: 8 }}>
                                <Text>{item.nama}</Text>
                                <Switch
                                    onChange={() => {
                                        item.value = !item.value
                                        courier[index] = item
                                        setCourier([...courier])
                                    }}
                                    value={item.value} />
                            </View>
                        )
                    })
                )}
            </ScrollView>

            <Button 
                loading={isSubmit}
                onPress={() => {
                    simpanPengirimanUser()
                }}
                style={{ margin: 15 }}>
                Simpan
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex : 1
    }
})

export default SettingPengiriman