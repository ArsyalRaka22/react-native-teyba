import { useNavigation } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Combobox from '../components/Combobox';
import InputLabel from '../components/InputLabel';
import TabBottom from '../components/TabBottom';
import { setUser } from '../store/actions';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';
import moment from 'moment';
import Toast from '../components/Toast';
import { HeaderBack } from '../components/Header';
import Octicons from 'react-native-vector-icons/Octicons';
import ImagePicker from 'react-native-image-crop-picker';

function ProfileDetail() {

    // const [dataPropinsi, setPropinsi] = useState([])
    // const [dataKota, setDataKota] = useState([])
    // const [dataKecamatan, setDataKecamatan] = useState([])
    // const [dataDesa, setDataDesa] = useState([])
    // const [selectedPropinsi, setSelectedPropinsi] = useState(null)
    // const [selectedKota, setSelectedKota] = useState(null)
    // const [selectedKecamatan, setSelectedKecamatan] = useState(null)
    // const [selectedDesa, setSelectedDesa] = useState(null)
    const user = useSelector((state) => state.user)
    const navigation = useNavigation()
    const dispatch = useDispatch()
    const [nama, setNama] = useState(user?.user.name)
    const [email, setEmail] = useState(user?.user.email)
    const [phone, setPhone] = useState(user?.user.phone)
    const [noIdentitas, setNoIdentitas] = useState(user?.user.no_identitas)
    const [tanggal, setTanggal] = useState(moment(new Date).format("DD"))
    const [bulan, setBulan] = useState(moment(new Date).format("MM"))
    const [tahun, setTahun] = useState(moment(new Date).format("YYYY"))
    const [isLoadingSave, setLoadingSave] = useState(false)
    const [gender, setGender] = useState({ id: user?.user?.jenis_kelamin })
    const [profileImg, setProfileImg] = useState(user?.user?.userImage)

    // const getDataKota = () => {
    //     console.log("selectedPropinsi", selectedPropinsi)
    //     HttpRequest.kabupaten(selectedPropinsi).then((res) => {
    //         console.log("res", res)
    //         setDataKota(res.data.data)
    //     }).catch((err) => {
    //         console.log("err", err, err.response)
    //     })
    // }
    // const getDataPropinsi = () => {
    //     HttpRequest.propinsi().then((res) => {
    //         console.log("res", res)
    //         setPropinsi(res.data.data)
    //     }).catch((err) => {
    //         console.log("err", err, err.response)
    //     })
    // }
    // const getDataKecamatan = () => {
    //     HttpRequest.kecamatan(selectedKota).then((res) => {
    //         console.log("res", res)
    //         setDataKecamatan(res.data.data)
    //     }).catch((err) => {
    //         console.log("err", err, err.response)
    //     })
    // }

    // const getDataDesa = () => {
    //     HttpRequest.desa(selectedKecamatan).then((res) => {
    //         console.log("res", res)
    //         setDataDesa(res.data.data)
    //     }).catch((err) => {
    //         console.log("err", err, err.response)
    //     })
    // }

    // useEffect(() => {
    //     getDataKota()
    // }, [selectedPropinsi])

    // useEffect(() => {
    //     getDataKecamatan()
    // }, [selectedKota])

    // useEffect(() => {
    //     getDataDesa()
    // }, [selectedKecamatan])

    // useEffect(() => {
    //     getDataPropinsi()
    // }, [])

    useEffect(() => {
        console.log("user", user)
        getMe()
    }, [])

    const actionSimpan = () => {

        if (tanggal == "") {
            Toast.showError("Periksa isian tanggal dengan benar")
            return
        }
        if (tanggal > 31) {
            Toast.showError("Periksa isian tanggal dengan benar")
            return
        }
        if (tanggal.length < 2) {
            Toast.showError("Isian tanggal harus 2 digit")
            return
        }
        if (bulan == "") {
            Toast.showError("Periksa isian bulan dengan benar")
            return
        }
        if (bulan > 12) {
            Toast.showError("Periksa isian bulan dengan benar")
            return
        }
        if (bulan.length < 2) {
            Toast.showError("Isian bulan harus 2 digit")
            return
        }
        if (tahun == "") {
            Toast.showError("Periksa isian tahun dengan benar")
            return
        }
        if (tahun.length < 4) {
            Toast.showError("Isian tahun harus 4 digit")
            return
        }

        let obj = {
            name: nama,
            email, phone, no_identitas: noIdentitas,
            tanggal_lahir: tahun + "-" + bulan + "-" + tanggal,
            jenis_kelamin: gender?.id,
            photo_url : profileImg?.filename
        }

        HttpRequest.profileSave(obj).then((res) => {
            console.log("res", res)
            Toast.showSuccess(res.data.message)
            getMe()
        }).catch((err) => {
            console.log("err", err, err.response)
            Toast.showError(err.response.message)
        })
    }

    const getMe = async () => {
        let res = await HttpRequest.me()
        if (res) {
            let u = user
            u.user = res.data.data
            console.log(res.data.data)
            dispatch(setUser(u))

            if (res.data?.data?.tanggal_lahir) {
                let tanggal_lahir = res.data?.data?.tanggal_lahir?.split("-")
                setTanggal(tanggal_lahir[2])
                setBulan(tanggal_lahir[1])
                setTahun(tanggal_lahir[0])
            }
        }
    }

    useEffect(() => {
        let tanggal_lahir = user?.user?.tanggal_lahir?.split("-")
        if (user?.user?.tanggal_lahir) {
            setTanggal(tanggal_lahir[2])
            setBulan(tanggal_lahir[1])
            setTahun(tanggal_lahir[0])
        }
    }, [user])

    const _gender = [
        { id: 1, name: "Laki-Laki" },
        { id: 2, name: "Perempuan" },
    ]

    const pickImage = useCallback(() => {
        ImagePicker.openPicker({
            includeBase64: true
        }).then(image => {
            console.log(image);

            HttpRequest.uploadImage({ photo: image.data, path: "user" }).then((res) => {
                let data = res.data.data
                setProfileImg(data)
                console.log("res", res)
                Toast.showSuccess("Upload Profile Berhasil, Silahkan klik tombol simpan", 5000)
            }).catch((err) => {
                console.log("err", err, err.response)
            })
        });
    })

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Data Profile"
                onPress={() => navigation.goBack()}
            />

            <ScrollView>
                <View style={styles.container}>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => {pickImage()}}>
                            <Image style={styles.userImg} source={{ uri: profileImg?.preview }} />
                        </TouchableOpacity>
                    </View>

                    <InputLabel
                        label="Nama"
                        placeholder="Nama kamu.."
                        value={nama}
                        onChangeText={(e) => setNama(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Email"
                        placeholder="email.."
                        value={email}
                        onChangeText={(e) => setEmail(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Nomor Hp"
                        placeholder="Nomor Hp..."
                        value={phone}
                        onChangeText={(e) => setPhone(e)}
                        containerStyle={styles.input}
                    />

                    <Text style={styles.label}>{"Tanggal Lahir"}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 15, paddingTop: 10 }}>
                        <InputLabel
                            placeholder="Tanggal"
                            keyboardType="numeric"
                            maxLength={2}
                            value={tanggal}
                            onChangeText={(e) => setTanggal(e)}
                            containerStyle={{ flex: 1, }}
                        />
                        <Text style={{ fontSize: 25, marginHorizontal: 10 }}>/</Text>
                        <InputLabel
                            placeholder="Bulan"
                            keyboardType="numeric"
                            maxLength={2}
                            value={bulan}
                            onChangeText={(e) => setBulan(e)}
                            containerStyle={{ flex: 1, }}
                        />
                        <Text style={{ fontSize: 25, marginHorizontal: 10 }}>/</Text>
                        <InputLabel
                            placeholder="Tahun"
                            keyboardType="numeric"
                            maxLength={4}
                            value={tahun}
                            onChangeText={(e) => setTahun(e)}
                            containerStyle={{ flex: 1, }}
                        />
                    </View>
                    <View style={{ marginBottom: 15 }}>
                        <Text style={styles.label}>{"Jenis Kelamin"}</Text>
                        {_gender.map((i) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        setGender(i)
                                    }}
                                    activeOpacity={0.6}
                                    style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                    <Octicons name={i.id == gender?.id ? "check-circle-fill" : "circle"} size={15} color={i.id == gender?.id ? color.primary : color.black} style={{ marginRight: 5 }} />
                                    <Text>{i.name}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                    <InputLabel
                        label="Nomor Identitas"
                        placeholder="Nomor KTP/SIM..."
                        value={noIdentitas}
                        onChangeText={(e) => setNoIdentitas(e)}
                        containerStyle={styles.input}
                    />

                    {/* <View>
                        <Text style={styles.label}>{"Propinsi"}</Text>
                        <Combobox
                            data={dataPropinsi ?? []}
                            labelKey="nama"
                            selectedValue={selectedPropinsi}
                            onValueChange={(e) => {
                                console.log("propinsi", e)
                                setSelectedPropinsi(e)
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Kabupaten"}</Text>
                        <Combobox
                            data={dataKota ?? []}
                            labelKey="nama"
                            selectedValue={selectedKota}
                            onValueChange={(e) => {
                                console.log("kabupaten", e)
                                setSelectedKota(e)
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Kecamatan"}</Text>
                        <Combobox
                            data={dataKecamatan ?? []}
                            labelKey="nama"
                            selectedValue={selectedKecamatan}
                            onValueChange={(e) => {
                                console.log("kecamatan", e)
                                setSelectedKecamatan(e)
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>{"Desa"}</Text>
                        <Combobox
                            data={dataDesa ?? []}
                            labelKey="nama"
                            selectedValue={selectedDesa}
                            onValueChange={(e) => {
                                setSelectedDesa(e)
                                console.log("desa", e)
                            }}
                        />
                    </View> */}
                </View>
            </ScrollView>
            <TouchableOpacity onPress={() => actionSimpan()} activeOpacity={0.6} style={styles.btn}>
                <Text style={{ color: color.white, fontSize: 16, fontWeight: 'bold' }}>Simpan</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    label: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black
    },
    userImg: {
        width: 140,
        height: 140,
        borderRadius: 100,
        backgroundColor: color.white,
        borderWidth: 2, borderColor: color.primary,
        marginBottom: 15
    },
    container: {
        marginVertical: 40,
        marginHorizontal: 20
    },
    input: {
        marginBottom: 20
    },
    bold: {
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputPhoneNumber: {
        borderBottomWidth: 1,
        borderBottomColor: '#C4C4C4',
        flex: 1,
        paddingVertical: 3,
        marginBottom: 5,
        fontSize: 16
    },
    btn: {
        paddingHorizontal: 15,
        paddingVertical: 12,
        backgroundColor: color.primary,
        alignItems: 'center',
        borderRadius: 8,
        marginTop: 20,
        marginBottom: 15,
        marginHorizontal: 20
    }
})

export default ProfileDetail