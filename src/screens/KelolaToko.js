import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet , Image} from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

export default function KelolaToko({ navigation }) {

    return (
        <View style={styles.main}>
            <HeaderBack
                theme="light"
                title="Kelola Toko"
                onPress={() => navigation.goBack()}
            />

            <View style={{ padding: 15 }}>
                {MENU.map((item, index) => {
                    return (
                        <TouchableOpacity
                            onPress={() => navigation.navigate(item.page)}
                            activeOpacity={0.6} key={index} style={styles.rowItem}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={styles.rowItemImage} source={item.img} />
                                <Text style={[styles.rowItemLabel]}>{item.label}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 25,
        borderBottomColor: color.gray,
        borderBottomWidth: 1, paddingBottom : 15
    },
    rowItemImage: {
        width: 30,
        height: 30
    },
    rowItemLabel: {
        fontFamily: AppConfig.font.family.semiBold,
        fontSize: AppConfig.font.size.default,
        marginLeft: 10
    },
})
const COLOR_DEFAULT = "#616161"
const MENU = [
    {
        label: "Profile Toko",
        img: AppConfig.PROFILE_USER,
        page: "ProfileToko",
        color: COLOR_DEFAULT
    },
    {
        label: "Pesanan",
        img: AppConfig.PROFILE_PESANAN,
        page: "DaftarPesanan",
        color: COLOR_DEFAULT
    },
    {
        label: "Kelola Produk",
        img: AppConfig.PROFILE_KELOLA_PRODUK,
        page: "KelolaProduk",
        color: COLOR_DEFAULT
    },
    {
        label: "Setting Pembayaran",
        img: AppConfig.PROFILE_KELOLA_PRODUK,
        page: "SettingPembayaran",
        color: COLOR_DEFAULT
    },
    {
        label: "Setting Pengiriman",
        img: AppConfig.PROFILE_KELOLA_PRODUK,
        page: "SettingPengiriman",
        color: COLOR_DEFAULT
    },
]