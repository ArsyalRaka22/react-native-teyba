import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Modal } from 'react-native';
import { FooterItem } from '../components/Footer';
import { HeaderBack } from '../components/Header';
import ProdukCart from '../components/ProdukCart';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import InputLabel from '../components/InputLabel';
import Rupiah from '../components/Rupiah';
import { useDispatch, useSelector } from 'react-redux';
import { store } from '../store';
import { setCartRedux } from '../store/actions';
import Toast from '../components/Toast';

function PesanProduk({navigation, route}) {

    const dispatch = useDispatch()
    const params = route.params
    const [modalPemesan, setModalPemesan] = useState(false)
    const [pemesan, setPemesan] = useState({})
    const cartRedux = useSelector((state) => state.cartRedux)
    const [cart, setCart] = useState([...cartRedux])

    useEffect(() => {
    }, [])

    const getTotalBelanja = useMemo(() => {
        let total = 0
        params?.cart.forEach((i) => {
            if (i.qty != 0) {
                total += i.qty * i.harga_jual
            }
        })

        return total
    })

    const carting = useCallback((i) => {
        console.log("carting")
        let index = cart.findIndex((item) => item.id == i.id)
        if (index == -1) {
            if (i.qty != 0) {
                cart.push(i)
                setCart([...cart])
            }
        } else {
            if (i.qty != 0) {
                cart[index] = i
                setCart([...cart])
            } else {
                cart.splice(index, 1)
                setCart([...cart])
            }
        }
    }, [])

    useEffect(() => {
        dispatch(setCartRedux([...cart]))
    }, [cart])
    
    const gotoCheckout = useCallback(() => {
        console.log("cart", cart)
        let errors = []

        let produkKarton = []
        cart.forEach((i) => {
            if (i.tipe === "karton") { 
                produkKarton.push(i)
            }
        })
        
        if (produkKarton.length > 0) {
            produkKarton.forEach((i) => {
                let total_pcs = 0

                i.variant.forEach((v) => {
                    total_pcs += v.qty
                })

                if (total_pcs != i.max_pcs) {
                    errors.push("Jumlah QTY Variant produk " + i.nama + " tidak sesuai")
                }
            })
        }

        if (errors.length > 0) {
            let messages = errors.join("\n")
            Toast.showError(messages, 3000)
        } else {
            navigation.navigate("Checkout")
        }
    })

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Keranjang Pesanan"
                onPress={() => navigation.goBack()}
            />

            <Modal
                animationType='slide'
                visible={modalPemesan}
                transparent={false}
                onRequestClose={() => setModalPemesan(false)}
            >

                <View style={{ flex: 1 }}>
                    <HeaderBack
                        title="Data Pemesan"
                        onPress={() => setModalPemesan(false)}
                    />
                    <ScrollView>
                        <View style={{ padding: 15 }}>
                            <InputLabel
                                label="Nama"
                                placeholder="Masukkan nama kamu"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Nomor whatsapp"
                                placeholder="Masukkan nomor kamu"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Kirim ke alamat"
                                placeholder="Masukkan alamat lengkap"
                                containerStyle={{ marginBottom: 20 }}
                            />
                            <InputLabel
                                label="Catatan untuk agen"
                                placeholder="Tulis catatan disini"
                                multiline={true}
                                numberOfLines={5}
                                containerStyle={{ marginBottom: 20 }}
                                inputStyle={{
                                    borderWidth: 1,
                                    borderColor: '#C6C6C6',
                                    paddingVertical: 3,
                                    color: color.black,
                                    borderRadius: 8,
                                    marginTop: 10,
                                    padding : 10
                                }}
                            />
                        </View>
                    </ScrollView>

                    <TouchableOpacity onPress={() => setModalPemesan(false)} activeOpacity={0.6}>
                        <Text style={styles.buttonSimpanPemesan}>Simpan</Text>
                    </TouchableOpacity>
                </View>
            </Modal>

            <ScrollView>
                <View style={{ padding: 15 }}>
                    {/* <Text style={styles.title}>Detail Pemesan</Text> */}
                    {/* <TouchableOpacity onPress={() => setModalPemesan(true)} style={styles.cardPemesan}>
                        <Text style={{ flex: 1, paddingRight: 20 }}>iqbal taufiqurrochman, ngampel tanjungsari rt.16 rw.02 , 085850943784</Text>
                        <MaterialIcons name='keyboard-arrow-right' size={20} color={color.primary} />
                    </TouchableOpacity> */}

                    {/* <Text style={styles.title}>Detail Pesanan</Text> */}

                    {cart?.map((item, index) => {
                        return (
                            <ProdukCart
                                key={index}
                                nama={item.nama}
                                gambar={item.gambar}
                                harga={Rupiah.format(item.harga_jual)}
                                qty={item.qty}
                                item={item}
                                variant={item.variant}
                                onChange={(e) => {
                                    console.log("e", e)
                                    item.qty = e
                                    carting(item)
                                }}
                                onChangeVariant={(e) => {
                                    console.log("e", e)
                                    item.variant = e
                                    carting(item)
                                }}
                            />
                        )
                    })}
                </View>
            </ScrollView>

            <FooterItem
                onPress={() => gotoCheckout()}
                totalPrice={Rupiah.format(getTotalBelanja, false)}
                totalItem={params?.cart?.length + " Item"}
                labelButton="Lanjutkan"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    title: {
        fontSize: AppConfig.font.size.large,
        fontFamily: AppConfig.font.family.bold,
        color: color.black,
        marginBottom: 10
    },
    cardPemesan: {
        backgroundColor: color.white,
        elevation: 2,
        padding: 10,
        borderRadius: 8,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonSimpanPemesan: {
        backgroundColor: color.primary,
        color: color.white,
        margin: 15,
        padding: 13,
        borderRadius: 8,
        textAlign: 'center',
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.semiBold,
        elevation: 2
    }
})

export default PesanProduk