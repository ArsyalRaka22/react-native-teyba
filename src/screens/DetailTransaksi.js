import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, ActivityIndicator, Alert } from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ProdukCart from '../components/ProdukCart';
import Rupiah from '../components/Rupiah';
import Button from '../components/Button';
import { HttpRequest } from '../utils/http';
import Toast from '../components/Toast';
import ImageModal from '../components/ImageModal';

function ItemRow(props) {
    let { label1, label2 } = props
    return (
        <View style={styles.rowItem}>
            <Text style={[styles.rowItemRegular, { flex: 1 }]}>{label1 ?? "-"}</Text>
            <Text style={[styles.rowItemRegular, { flex: 1, textAlign: 'right' }]}>{label2 ?? "-"}</Text>
        </View>
    )
}

export default function DetailTransaksi({ navigation, route }) {

    const [detailTransaksi, setDetailTransaki] = useState(null)
    const [modalImage, setModalImage] = useState(false)
    const [modalImageUri, setModalImageUri] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    const getTotalItem = useMemo(() => {
        let total = 0
        detailTransaksi?.details?.forEach((i) => {
            if (i.qty != 0) {
                total += i.jumlah
            }
        })

        return total
    }, [detailTransaksi])

    const getCamelcaseString = (string) => {
        string = string.charAt(0).toUpperCase() + string.slice(1)
        return string.replace("_", " ")
    }

    const getDetailTransaksi = useCallback(async (id) => {
        console.log("id", id)
        try {
            let res = await HttpRequest.getDetailTransaksi({ id: id, is_penjual: false })
            setDetailTransaki(res.data.data)
            console.log('res getDetailTransaksi', res)
        } catch (error) {
            console.log('err', error, error.response)
        }
    }, [])

    const terimaPesanan = useCallback(async () => {
        try {
            let id = detailTransaksi?.transaksi?.id
            let res = await HttpRequest.konfirmasiTerimaPesanan({ transaksi_id: id })
            Toast.showSuccess(res.data.message, 3000)
            navigation.goBack()
            console.log("res", res)
        } catch (error) {
            console.log("err", error, error.response)
            Toast.showSuccess(error.response.data.message, 3000)
        }
    })

    const actionBatalkanPesanan = useCallback(async () => {
        try {
            setIsLoading(true)
            let id = detailTransaksi?.transaksi?.id
            let res = await HttpRequest.batalkanPesanan({ transaksi_id: id })

            Toast.showSuccess(res.data.message)
            navigation.goBack()
        } catch (error) {
            console.log("err", error, error.response)
            setIsLoading(false)
        }
    })

    useEffect(() => {
        getDetailTransaksi(route.params.id);
    }, [])

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Detail Transaksi"
                onPress={() => navigation.goBack()}
            />

            <ImageModal
                visible={modalImage}
                onRequestClose={() => setModalImage(false)}
                uri={detailTransaksi?.bukti_pembayaran}
                title={"Pembayaran - " + detailTransaksi?.transaksi?.no_transaksi}
            />

            <ScrollView>
                <View style={{ padding: 15 }}>

                    {detailTransaksi == null && (
                        <ActivityIndicator size={"small"} color={color.primary} />
                    )}
                    {detailTransaksi != null && (
                        <>
                            <View style={{ marginBottom: 10 }}>
                                <Text style={styles.title}>Status Pesanan</Text>

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    {detailTransaksi?.status?.image != null && (
                                        <Image style={styles.imageStatus} resizeMode="contain" source={{ uri: detailTransaksi?.status?.image }} />
                                    )}
                                    {detailTransaksi?.status?.image == null && (
                                        <Text style={{ color: detailTransaksi?.status?.color, marginTop: 6, marginBottom: 10 }}>{detailTransaksi?.status?.label}</Text>
                                    )}

                                    {detailTransaksi?.transaksi?.is_cod == 1 && (
                                        <Text style={{ marginLeft: 10, backgroundColor: color.green, paddingHorizontal: 8, paddingVertical: 2, borderRadius: 100, color: color.white }}>COD</Text>
                                    )}
                                </View>
                            </View>

                            <View style={{ marginBottom: 15, }}>
                                <Text style={[styles.title, { marginBottom: 10 }]}>Alamat Pengiriman</Text>
                                {detailTransaksi?.address && (
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <Ionicons name='location-sharp' size={20} color={color.primary} />
                                        <View style={{ flex: 1, marginHorizontal: 15 }}>
                                            <Text style={styles.address}>{detailTransaksi?.address?.alamat_lengkap} {detailTransaksi?.address?.kecamatan}, {detailTransaksi?.address?.kabupaten}, {detailTransaksi?.address?.propinsi} {detailTransaksi?.address?.kode_pos}</Text>
                                        </View>
                                    </View>
                                )}

                                <View style={{ marginTop: 20 }}>
                                    {detailTransaksi?.details?.map((item, index) => {
                                        return (
                                            <ProdukCart
                                                key={index}
                                                gambar={item.gambar}
                                                nama={item.produk}
                                                harga={Rupiah.format(item.harga_jual, false) + " x " + item.jumlah}
                                                showButton={false}
                                                item={item}
                                                variant={item.variant}
                                            />
                                        )
                                    })}
                                </View>
                                
                                <Text style={[styles.title, { marginVertical: 15 }]}>Detail Pesanan</Text>

                                <View style={styles.rowItem}>
                                    <Text style={styles.rowItemRegular}>{getTotalItem} Item</Text>
                                    <Text style={styles.rowItemRegular}>{Rupiah.format(detailTransaksi?.transaksi?.total_transaksi ?? 0, false)}</Text>
                                </View>
                                {detailTransaksi?.status?.id == 1 && (
                                    <View style={styles.rowItem}>
                                        <Text style={styles.rowItemRegular}>Ongkos Kirim</Text>
                                        <Text style={styles.rowItemRegular}>{Rupiah.format(detailTransaksi?.pengiriman?.ongkos_kirim ?? 0, false)}</Text>
                                    </View>
                                )}
                                {detailTransaksi?.status?.id != 1 && detailTransaksi?.transaksi?.is_cod != 1 && (
                                    <View style={styles.rowItem}>
                                        <Text style={styles.rowItemRegular}>Ongkos Kirim</Text>
                                        <Text style={styles.rowItemRegular}>{Rupiah.format(detailTransaksi?.transaksi?.ongkos_kirim ?? 0, false)}</Text>
                                    </View>
                                )}

                                {detailTransaksi?.transaksi?.subsidi_ongkir != 0 && detailTransaksi?.transaksi?.subsidi_ongkir != null && (
                                    <View style={styles.rowItem}>
                                        <Text style={styles.rowItemRegular}>Subsidi Ongkir</Text>
                                        <Text style={styles.rowItemRegular}>- {Rupiah.format(detailTransaksi?.transaksi?.subsidi_ongkir ?? 0, false)}</Text>
                                    </View>
                                )}

                                <View style={styles.rowItem}>
                                    <Text style={styles.rowItemBold}>Total</Text>
                                    <Text style={styles.rowItemBold}>{Rupiah.format(detailTransaksi?.transaksi?.total_tagihan ?? 0, false)}</Text>
                                </View>
                                <View style={{ height: 1, backgroundColor: color.border }} />

                                {detailTransaksi?.transaksi?.is_dropshipper == 1 && (
                                    <View>
                                        <Text style={[styles.title, { marginVertical: 15 }]}>Dropshipper</Text>
                                        <ItemRow label1={"Nama Pengirim"} label2={detailTransaksi?.transaksi?.dropshipper_name ?? "-"} />
                                        <ItemRow label1={"Telepon Pengirim"} label2={detailTransaksi?.transaksi?.dropshipper_phone ?? "-"} />
                                        <View style={{ height: 1, backgroundColor: color.border }} />
                                    </View>
                                )}

                                {detailTransaksi?.transaksi?.is_cod != 1 && (
                                    detailTransaksi?.pengiriman && (
                                        <>
                                            <Text style={[styles.title, { marginVertical: 15 }]}>Pengiriman</Text>
                                            <View style={{}}>
                                                {detailTransaksi != null && (
                                                    Object.keys(detailTransaksi?.pengiriman)?.map((item, index) => {
                                                        if (item == "ongkos_kirim") {
                                                            return (
                                                                <ItemRow label1={getCamelcaseString(item)} label2={Rupiah.format(detailTransaksi?.pengiriman[item] ?? 0, false)} />
                                                            )
                                                        } else {
                                                            return (
                                                                <ItemRow label1={getCamelcaseString(item)} label2={detailTransaksi?.pengiriman[item ?? 0]} />
                                                            )
                                                        }
                                                    })
                                                )}
                                            </View>

                                            <View style={{ height: 1, backgroundColor: color.border }} />
                                        </>
                                    )
                                )}

                                {detailTransaksi?.status?.id == 1 && (
                                    <View>
                                        <Text style={[styles.title, { marginVertical: 15 }]}>Pembayaran</Text>
                                        <View style={{}}>

                                            {detailTransaksi != null && (
                                                Object.keys(detailTransaksi?.pembayaran)?.map((item, index) => {
                                                    return (
                                                        <ItemRow label1={getCamelcaseString(item)} label2={detailTransaksi?.pembayaran[item]} />
                                                    )
                                                })
                                            )}
                                        </View>
                                        <View style={{ height: 1, backgroundColor: color.border }} />
                                    </View>
                                )}
                                {detailTransaksi?.status?.id == 2 && (
                                    <View>
                                        <Text style={[styles.title, { marginVertical: 15 }]}>Pembayaran</Text>
                                        <View style={{}}>

                                            {detailTransaksi != null && (
                                                Object.keys(detailTransaksi?.pembayaran)?.map((item, index) => {
                                                    return (
                                                        <ItemRow label1={getCamelcaseString(item)} label2={detailTransaksi?.pembayaran[item]} />
                                                    )
                                                })
                                            )}
                                        </View>
                                        <View style={{ height: 1, backgroundColor: color.border }} />
                                    </View>
                                )}

                                {detailTransaksi?.bukti_pembayaran != null && (
                                    <>
                                        <Text style={[styles.title, { marginVertical: 15 }]}>Bukti Pembayaran</Text>
                                        <TouchableOpacity onPress={() => {
                                            setModalImage(true)
                                        }}>
                                            <Image source={{ uri: detailTransaksi?.bukti_pembayaran }} style={{ width: '100%', height: 250 }} />
                                        </TouchableOpacity>
                                    </>
                                )}
                            </View>
                        </>
                    )}

                </View>
            </ScrollView>

            {detailTransaksi?.transaksi.status_transaksi_id == 2 && detailTransaksi?.bukti_pembayaran == null && (
                <Button
                    onPress={() => navigation.navigate("Pembayaran", { transaksi: detailTransaksi?.transaksi })}
                    style={{ margin: 15 }}
                >Konfirmasi Pembayaran</Button>
            )}
            {detailTransaksi?.transaksi.status_transaksi_id == 1 && (
                <Button
                    loading={isLoading}
                    onPress={() => {
                        Alert.alert("Batalkan Pesanan", "Apakah anda yakin membatalkan pesanan ini?", [
                            {
                                text: "Tidak",
                                style: 'cancel'
                            },
                            {
                                text: "Ya",
                                onPress: () => actionBatalkanPesanan()
                            }
                        ])
                    }}
                    style={{ margin: 15, backgroundColor: color.danger }}
                >Batalkan Pesanan</Button>
            )}
            {detailTransaksi?.transaksi.status_transaksi_id == 3 && (
                <Button
                    onPress={() => {
                        Alert.alert("Konfirmasi Terima Pesanan", "Apakah anda yakin sudah menerima pesanan ini?", [
                            {
                                text: "tidak",
                                style: 'cancel'
                            },
                            {
                                text: "Ok",
                                onPress: () => { terimaPesanan() }
                            }
                        ], { cancelable: true })
                    }}
                    style={{ margin: 15 }}
                >Konfirmasi Terima Pesanan</Button>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    title: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },
    imageStatus: {
        width: 100,
        height: 40
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    rowItemRegular: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        color: color.black
    },
    rowItemBold: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    }
})