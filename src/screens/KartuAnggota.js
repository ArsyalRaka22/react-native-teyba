import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image } from 'react-native';
import { HeaderBack } from '../components/Header';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';

function KartuAnggota({ navigation, route }) {

    useEffect(() => {
        console.log(route.params.profile)
    }, [])
    return (
        <View style={styles.main}>
            <HeaderBack
                title="Kartu Anggota"
                // theme="light"
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView>
                <View style={{ padding: 20 }}>
                    <View>
                        <Image tintColor={color.primary} resizeMode='contain' source={AppConfig.KARTU_ANGGOTA_IMG} style={styles.imgTemplate} />
                        <View style={styles.absoluteContent}>
                            <View style={{flex : 1}}>
                                <Text style={styles.name}>{route.params.profile.profile.name}</Text>
                                <Text style={styles.kode}>{route.params.profile.kode_member}</Text>
                            </View>

                            {/* <Text style={{color : color.white}}>QRCODE</Text> */}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white,
    },
    imgTemplate: {
        width: '100%',
        height: 250,
    },
    name: {
        color: color.white,
        fontFamily: AppConfig.font.family.bold,
        fontSize: AppConfig.font.size.large,
    },
    kode: {
        color: color.white,
        fontFamily: AppConfig.font.family.bold,
        fontSize: AppConfig.font.size.default,
        marginVertical: 6
    },
    absoluteContent: {
        position: 'absolute',
        bottom: 0,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})

export default KartuAnggota