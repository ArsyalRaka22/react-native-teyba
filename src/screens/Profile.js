import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Header } from '../components/Header';
import TabBottom from '../components/TabBottom';
import { setUser } from '../store/actions';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import Constant from '../utils/Constant';
import { HttpRequest } from '../utils/http';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useIsFocused } from '@react-navigation/native';

function Profile({ navigation }) {

    const user = useSelector((state) => state.user)
    const dispatch = useDispatch();
    const [userDetail, setUserDetail] = useState(null)
    const isFocus = useIsFocused()

    const actionLogout = useCallback(() => {
        Alert.alert("Logout", "Apakah anda yakin akan melakukan logout?", [
            {
                text: "Tidak",
                style: 'cancel'
            },
            {
                text: "Oke",
                onPress: () => {
                    dispatch(setUser(null))
                }
            }
        ], { cancelable: true })
    })

    const getProfileDetail = useCallback(async () => {
        try {
            let res = await HttpRequest.profileDetail()
            console.log("res", res)
            setUserDetail(res.data.data)
        } catch (error) {
            console.log("err", error, error.response)
        }
    })

    useEffect(() => {
        if (isFocus) {
            getProfileDetail()
        }
    }, [isFocus])

    const getMenu = useMemo(() => {

        if (user?.user?.role_id == Constant.Role.AGEN) {
            return PROFILE_MENU
        } else {
            return PROFILE_MENU_RESELLER
        }
    })

    return (
        <View style={styles.main}>
            <Header />
            <ScrollView>
                <Image tintColor={color.primary} resizeMode="cover" style={styles.background} source={AppConfig.BACKGROUND_IMG} />
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                    <Image style={styles.userImg} source={{ uri: userDetail?.profile_photo }} />

                    {user?.user.role_id == Constant.Role.RESELLER && (
                        <Text style={styles.greeting}>Reseller</Text>
                    )}
                    {user?.user.role_id == Constant.Role.AGEN && (
                        <Text style={styles.greeting}>Agen</Text>
                    )}
                    <Text style={styles.userName}>{user?.user.name}</Text>
                </View>

                <View style={{ padding: 15 }}>

                    <View style={styles.boxContainer}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 12 }}>Jumlah Point</Text>
                            <Text style={{ fontSize: 18 }}>{userDetail?.total_point}</Text>
                        </View>
                        {/* {userDetail?.profile.role_id == Constant.Role.AGEN && (
                            <>
                                <View style={styles.seperator} />
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 12 }}>Point Beku</Text>
                                    <Text style={{ fontSize: 18 }}>{userDetail?.point_beku}</Text>
                                </View>
                            </>
                        )} */}
                        {userDetail?.profile.role_id == Constant.Role.AGEN && (
                            <>
                                <View style={styles.seperator} />
                                <TouchableOpacity onPress={() => navigation.navigate("KelolaToko")} activeOpacity={0.4} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                    <View>
                                        <Text style={{ fontSize: 12 }}>Kelola Toko</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                            <MaterialIcons name='storefront' size={16} />
                                            <Text style={{ marginLeft: 5 }}>{userDetail?.toko?.nama ?? "-"}</Text>
                                        </View>
                                    </View>
                                    <MaterialIcons name='keyboard-arrow-right' size={20} />
                                </TouchableOpacity>
                            </>
                        )}
                    </View>

                    <View style={{ marginTop: 15 }}>
                        {getMenu.map((item, index) => {
                            return (
                                <TouchableOpacity onPress={() => {
                                    navigation.navigate(item.page, { profile: userDetail })
                                }} activeOpacity={0.6} key={index} style={styles.rowItem}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image style={styles.rowItemImage} source={item.img} />
                                        <Text style={[styles.rowItemLabel, { color: item.color }]}>{item.label}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}

                        <TouchableOpacity onPress={() => {
                            actionLogout()
                        }} activeOpacity={0.6} style={styles.rowItem}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={styles.rowItemImage} source={AppConfig.PROFILE_LOGOUT} />
                                <Text style={[styles.rowItemLabel, { color: COLOR_RED }]}>{"Logout"}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>

            <TabBottom
                selected={3}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    background: {
        width: '100%',
        height: 70
    },
    userImg: {
        width: 100,
        height: 100,
        borderRadius: 100,
        marginTop: -50,
        backgroundColor: color.white,
        borderWidth: 2, borderColor: color.primary
    },
    greeting: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.regular,
        marginVertical: 8
    },
    userName: {
        fontSize: AppConfig.font.size.large,
        fontFamily: AppConfig.font.family.bold,
        color: color.primary
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 25,
        borderBottomColor: color.gray,
        borderBottomWidth: 1, paddingBottom: 15
    },
    rowItemImage: {
        width: 30,
        height: 30
    },
    rowItemLabel: {
        fontFamily: AppConfig.font.family.semiBold,
        fontSize: AppConfig.font.size.default,
        marginLeft: 10
    },
    boxContainer: {
        backgroundColor: color.white,
        elevation: 2,
        borderRadius: 5,
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent : 'space-between'
    },
    seperator: {
        height: '100%',
        width: 1,
        marginHorizontal: 10,
        backgroundColor: color.gray,
        borderRadius: 100
    }
})

const COLOR_DEFAULT = "#616161"
const COLOR_RED = color.danger
const PROFILE_MENU_RENDER = PROFILE_MENU_RESELLER
const PROFILE_MENU = [
    {
        label: "Data Profile",
        img: AppConfig.PROFILE_USER,
        page: "ProfileDetail",
        color: COLOR_DEFAULT
    },
    {
        label: "Pesanan",
        img: AppConfig.PROFILE_PESANAN,
        page: "TransaksiStack",
        color: COLOR_DEFAULT
    },
    // {
    //     label: "Kelola Produk",
    //     img: AppConfig.PROFILE_KELOLA_PRODUK,
    //     page: "KelolaProduk",
    //     color: COLOR_DEFAULT
    // },
    {
        label: "Daftar referal",
        img: AppConfig.PROFILE_KODE_REFERRAL,
        page: "ReferralAgen",
        color: COLOR_DEFAULT
    },
    // {
    //     label: "Jumlah point",
    //     img: AppConfig.PROFILE_JUMLAH_POINT,
    //     page: "",
    //     color: COLOR_DEFAULT
    // },
    {
        label: "History point",
        img: AppConfig.PROFILE_HISTORY_POINT,
        page: "HistoryPoint",
        color: COLOR_DEFAULT
    },
    {
        label: "Kartu Anggota",
        img: AppConfig.PROFILE_KARTU_ANGGOTA,
        page: "KartuAnggota",
        color: COLOR_DEFAULT
    },
]
const PROFILE_MENU_RESELLER = [
    {
        label: "Data Profile",
        img: AppConfig.PROFILE_USER,
        page: "ProfileDetail",
        color: COLOR_DEFAULT
    },
    {
        label: "Pesanan",
        img: AppConfig.PROFILE_PESANAN,
        page: "TransaksiStack",
        color: COLOR_DEFAULT
    },
    // {
    //     label: "Jumlah point",
    //     img: AppConfig.PROFILE_JUMLAH_POINT,
    //     page: "",
    //     color: COLOR_DEFAULT
    // },
    {
        label: "Daftar referal",
        img: AppConfig.PROFILE_KODE_REFERRAL,
        page: "ReferralAgen",
        color: COLOR_DEFAULT
    },
    {
        label: "History point",
        img: AppConfig.PROFILE_HISTORY_POINT,
        page: "HistoryPoint",
        color: COLOR_DEFAULT
    },
    {
        label: "Kartu Anggota",
        img: AppConfig.PROFILE_KARTU_ANGGOTA,
        page: "KartuAnggota",
        color: COLOR_DEFAULT
    },
]

export default Profile