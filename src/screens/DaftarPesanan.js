import { useIsFocused } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, ActivityIndicator, RefreshControl } from 'react-native';
import { HeaderBack } from '../components/Header';
import NoData from '../components/NoData';
import Placeholder from '../components/Placeholder';
import TabBottom from '../components/TabBottom';
import TabHeader from '../components/TabHeader';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';

function DaftarPesanan({ navigation }) {

    const [selected, setSelected] = useState()
    const [statuses, setStatuses] = useState([])
    const [riwayat, setRiwayat] = useState([])
    const [isLoading, setIsloading] = useState(false)
    const isFocus = useIsFocused()

    const getStatusTransaksi = useCallback(async () => {
        try {
            let res = await HttpRequest.getStatusTransaksi({ is_penjual: true })
            console.log("res", res)
            setStatuses(res.data.data)
            setSelected(res.data.data[0])
        } catch (error) {
            console.log("err", error, error.response)
        }
    }, [])

    const getRiwayatTransaksi = useCallback(async () => {
        setIsloading(true)
        try {
            let param = {
                is_penjual: true,
                status: selected?.id
            }
            console.log("selected", selected)
            console.log("param", param)
            let res = await HttpRequest.getRiwayatTransaksi(param)
            console.log("res", res)
            setRiwayat([...res.data.data])
            setIsloading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsloading(false)
        }
    }, [selected])

    useEffect(() => {
        getStatusTransaksi()
    }, [])

    useEffect(() => {
        if (isFocus) {
            getStatusTransaksi()
        }
    }, [isFocus])

    useEffect(() => {
        getRiwayatTransaksi()
        setIsloading(true)
    }, [selected])

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Pesanan Toko"
                theme="light"
                containerStyle={{ backgroundColor: color.white }}
                onPress={() => {
                    navigation.goBack()
                }}
            />

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={isLoading}
                        onRefresh={() => getRiwayatTransaksi()}
                    />
                }
            >

                {isLoading && (
                    <View style={{ padding: 15 }}>
                        <Placeholder.KategoriProduk />
                        <View style={{ marginTop: 10 }}>
                            <Placeholder.DaftarPesanan />
                        </View>
                    </View>
                )}

                {!isLoading && (
                    <>
                        <View style={{ padding: 15 }}>
                            <TabHeader
                                items={statuses}
                                selected={selected}
                                onChangeSelected={(e) => {
                                    console.log("e", e)
                                    setSelected(e)
                                }}
                            />

                        </View>

                        {riwayat.length == 0 && (
                            <NoData>Tidak ada transaksi</NoData>
                        )}

                        {riwayat?.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} activeOpacity={0.6} onPress={() => {
                                    navigation.navigate("DetailPesanan", { transaksi_id: item?.transaksi?.id })
                                }} style={styles.item}>

                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        {item?.status?.image != null && (
                                            <Image resizeMode='contain' style={styles.itemImageStatus} source={{ uri: item?.status?.image }} />
                                        )}
                                        {item?.status?.image == null && (
                                            <Text style={{ color: item?.status?.color ?? color.primary }}>{item?.status?.label ?? "-"}</Text>
                                        )}

                                        {item?.transaksi?.is_cod == 1 && (
                                            <Text style={{ marginLeft: 10, backgroundColor: color.green, paddingHorizontal: 8, paddingVertical: 2, borderRadius: 100, color: color.white }}>COD</Text>
                                        )}
                                    </View>

                                    <Text style={{ marginTop: 10 }}>{item?.transaksi?.no_transaksi}</Text>
                                    <Text style={styles.productName}>{item?.details[0]?.produk}</Text>
                                    <Text style={styles.address}>{item?.address?.formatted_address}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </>
                )}
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    item: {
        borderBottomColor: color.border,
        borderBottomWidth: 1,
        padding: 15,
    },
    itemImageStatus: {
        width: 110,
        height: 30,
        marginBottom: 4
    },
    productName: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
        color: color.black,
        marginVertical: 4
    },
    address: {
        fontSize: AppConfig.font.size.small,
        fontFamily: AppConfig.font.family.regular,
        paddingRight: 30
    }
})

export default DaftarPesanan