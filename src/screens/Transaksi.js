import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, RefreshControl, ActivityIndicator } from 'react-native';
import { Header } from '../components/Header';
import { ModalDefault } from '../components/Selesai';
import AppConfig from '../utils/AppConfig';
import color from '../utils/color';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { HttpRequest } from '../utils/http';
import NoData from '../components/NoData';
import Rupiah from '../components/Rupiah';
import { useIsFocused } from '@react-navigation/native';
import TabBottom from '../components/TabBottom';
import Placeholder from '../components/Placeholder';

function Transaksi({ navigation }) {

    const [visible, setVisible] = useState(false)
    const [selected, setSelected] = useState({})
    const [statuses, setStatuses] = useState([])
    const [riwayat, setRiwayat] = useState([])
    const [isLoading, setIsloading] = useState(true)
    const isFocus = useIsFocused()

    const getStatusTransaksi = useCallback(async () => {
        setIsloading(true)
        try {
            let res = await HttpRequest.getStatusTransaksi({ is_penjual: false })
            console.log("res", res)
            setStatuses(res.data.data)
            setSelected(res.data.data[0])
            setIsloading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsloading(false)
        }
    }, [])

    const getRiwayatTransaksi = useCallback(async () => {
        setIsloading(true)
        try {
            let param = {
                is_penjual: false,
                status: selected?.id
            }
            console.log("selected", selected)
            console.log("param", param)
            let res = await HttpRequest.getRiwayatTransaksi(param)
            console.log("res", res)
            setRiwayat([...res.data.data])
            setIsloading(false)
        } catch (error) {
            console.log("err", error, error.response)
            setIsloading(false)
        }
    }, [selected])

    useEffect(() => {
        getStatusTransaksi()
    }, [])

    useEffect(() => {
        if (isFocus) {
            getStatusTransaksi()
        }
    }, [isFocus])

    useEffect(() => {
        getRiwayatTransaksi()
    }, [selected])

    const renderItem = (item) => {
        return (
            <View style={styles.item}>

                {item?.details[0]?.gambar == null && (
                    <Image style={styles.itemImage} source={{ uri: "https://images.tokopedia.net/img/cache/900/VqbcmM/2021/5/1/2d70239d-c36a-49c8-802f-0e0083ff3f42.jpg" }} />
                )}
                {item?.details[0]?.gambar != null && (
                    <Image style={styles.itemImage} source={{ uri: item?.details[0]?.gambar }} />
                )}

                <View style={{ marginLeft: 8, alignItems: 'flex-start', justifyContent: 'space-between', flex: 1 }}>

                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        {item?.status?.image != null && (
                            <Image resizeMode='contain' style={styles.statusImage} source={{ uri: item.status.image }} />
                        )}
                        {item?.status?.image == null && (
                            <Text style={{ color: item?.status?.color }}>{item?.status?.label}</Text>
                        )}

                        {item?.transaksi?.is_cod == 1 && (
                            <Text style={{ marginLeft: 10, backgroundColor: color.green, paddingHorizontal: 8, paddingVertical: 2, borderRadius: 100, color: color.white }}>COD</Text>
                        )}
                    </View>

                    <Text style={{ marginVertical: 5 }}>{item?.transaksi?.no_transaksi}</Text>
                    <Text style={styles.productName}>{item?.details[0]?.produk}</Text>
                    <View style={styles.rowItem}>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.totalLabel}>Total</Text>
                            <Text style={styles.totalPrice}>{Rupiah.format(item?.transaksi?.total_tagihan)}</Text>
                        </View>

                        {/* <TouchableOpacity style={styles.buttonBeliLagi} activeOpacity={0.6}>
                            <Text style={styles.buttonBeliLagiLabel}>Beli Lagi</Text>
                        </TouchableOpacity> */}
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.main}>
            <Header />

            <ModalDefault
                visible={visible}
                onRequestClose={() => {
                    setVisible(false)
                }}
            >
                {statuses.map((item, index) => {
                    return (
                        <TouchableOpacity key={index} onPress={() => {
                            setVisible(false)
                            setSelected(item)
                            // getRiwayatTransaksi()
                        }} activeOpacity={0.6}>
                            <Text style={[styles.itemStatusChooser, {
                                fontFamily: selected.id == item.id ? AppConfig.font.family.bold : AppConfig.font.family.regular
                            }]}>{item.label}</Text>
                        </TouchableOpacity>
                    )
                })}
            </ModalDefault>

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={isLoading}
                        onRefresh={() => getStatusTransaksi()}
                    />
                }
            >

                <View style={{ padding: 15 }}>
                    <TouchableOpacity onPress={() => {
                        setVisible(true)
                    }} activeOpacity={0.6} style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.labelKategori}>{selected.label}</Text>
                        <MaterialIcons name="keyboard-arrow-down" size={20} color={color.black} />
                    </TouchableOpacity>
                </View>

                {isLoading && (
                    <View style={{ padding: 15 }}>
                        <Placeholder.Transaksi />
                    </View>
                )}

                {!isLoading && (
                    riwayat.length == 0 && (
                        <NoData>Tidak ada riwayat</NoData>
                    )
                )}
                {!isLoading && (
                    riwayat.map((i, index) => {
                        return (
                            <TouchableOpacity onPress={() => navigation.navigate("DetailTransaksi", { id: i?.transaksi?.id })} activeOpacity={0.6} key={index}>
                                {renderItem(i)}
                            </TouchableOpacity>
                        )
                    })
                )}
            </ScrollView>

            <TabBottom
                selected={2}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    itemStatusChooser: {
        paddingVertical: 10,
        marginBottom: 8,
        borderBottomColor: color.border,
        borderBottomWidth: 1,
        color: color.black
    },
    labelKategori: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black,
        marginRight: 5
    },
    item: {
        padding: 15,
        borderBottomColor: color.border,
        borderBottomWidth: 2,
        flexDirection: 'row',
        // alignItems: 'center'
    },
    itemImage: {
        width: 95,
        height: 95,
        borderRadius: 8
    },
    statusImage: {
        width: 110,
        height: 28,
    },
    productName: {
        fontSize: AppConfig.font.size.default,
        fontFamily: AppConfig.font.family.medium,
        color: color.black,
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    totalLabel: {
        fontFamily: AppConfig.font.family.semiBold
    },
    totalPrice: {
        fontFamily: AppConfig.font.family.bold,
        color: color.black
    },
    buttonBeliLagi: {
        backgroundColor: color.primary,
        borderRadius: 8,
        paddingHorizontal: 12,
        paddingVertical: 7,
        elevation: 2,
    },
    buttonBeliLagiLabel: {
        color: color.white,
        fontFamily: AppConfig.font.family.medium
    }
})

export default Transaksi