import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView , Modal, TextInput} from 'react-native';
import Button from '../components/Button';
import { HeaderBack } from '../components/Header';
import InputLabel from '../components/InputLabel';
import color from '../utils/color';
import { HttpRequest } from '../utils/http';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AppConfig from '../utils/AppConfig';
import Toast from '../components/Toast';

export default function ProfileToko({ navigation }) {

    const [dataKota, setDataKota] = useState([])
    const [selectedKota, setSelectedKota] = useState(null)
    const [cariKota, setCariKota] = useState("")
    const [modalKota, setModalKota] = useState(false)
    const [namaToko, setNamaToko] = useState('')
    const [alamatToko, setAlamatToko] = useState('')
    const [kotaString, setKotaString] = useState('')

    const getDataKota = useCallback(() => {
        console.log("search", cariKota)
        HttpRequest.listKota(cariKota).then((res) => {
            console.log("res list kota", res)
            setDataKota(res.data.data)
        }).catch((err) => {
            console.log("err", err, err.response)
        })
    })

    const getProfileToko = useCallback(async () => {
        try {
            let res = await HttpRequest.getProfileToko()
            console.log('getProfileToko', res)
            let data = res.data.data
            setNamaToko(data.nama_toko)
            setAlamatToko(data.alamat_toko)
            setKotaString(data.kota)
        } catch (error) {
            console.log('err', error, error.response)
        }
    })

    const simpanProfileToko = useCallback(async () => {
        try {
            let param = {
                nama_toko: namaToko,
                alamat_toko: alamatToko,
                kota_id : selectedKota?.nama
            }
            let res = await HttpRequest.updateProfileToko(param)
            Toast.showSuccess(res.data.message)
        } catch (error) {
            console.log('err', error, error.response)
            Toast.showError(error.response.data.message)
        }
    })

    useMemo(() => {
        if (cariKota.length >= 3) {
            getDataKota()
        }
    }, [cariKota])

    useEffect(() => {
        getDataKota()
        getProfileToko()
    }, [])

    return (
        <View style={styles.main}>
            <HeaderBack
                title="Profile Toko"
                theme="light"
                onPress={() => navigation.goBack()}
            />

            <Modal
                animationType='slide'
                visible={modalKota}
                transparent={false}
                onRequestClose={() => {
                    setModalKota(false)
                }}
            >

                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15, backgroundColor: color.white, elevation: 2 }}>
                        <TouchableOpacity onPress={() => setModalKota(false)} activeOpacity={0.6}>
                            <MaterialCommunityIcons name='arrow-left' size={20} color={color.black} />
                        </TouchableOpacity>
                        <Text style={{ color: color.black, fontSize: 16, marginHorizontal: 10 }}>Pilih Kota</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: color.white, elevation: 2, borderRadius: 5, padding: 5, marginHorizontal: 20, marginVertical: 10 }}>
                        <MaterialIcons name='search' size={20} color={color.black} />
                        <TextInput
                            style={{ paddingVertical: 0 }}
                            placeholder="Cari Kota.."
                            value={cariKota}
                            onChangeText={(e) => setCariKota(e)}
                        />
                    </View>

                    <ScrollView>
                        {dataKota.map((i, key) => {
                            return (
                                <TouchableOpacity onPress={() => {
                                    setSelectedKota(i)
                                    setModalKota(false)
                                    setKotaString(i.nama)
                                }} key={key} style={{ borderBottomColor: color.gray, borderBottomWidth: 1.5, padding: 15 }}>
                                    <Text style={{ color: color.black }}>{i.nama}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>
                </View>
            </Modal>

            <ScrollView>
                <View style={{ padding: 15 }}>

                    <InputLabel
                        label="Nama Toko"
                        placeholder="Nama Toko"
                        value={namaToko}
                        onChangeText={(e) => setNamaToko(e)}
                        containerStyle={styles.input}
                    />
                    <InputLabel
                        label="Alamat lengkap toko"
                        multiline={true}
                        numberOfLines={4}
                        value={alamatToko}
                        onChangeText={(e) => setAlamatToko(e)}
                        placeholder="Alamat lengkap toko"
                        containerStyle={styles.input}
                    />

                    <TouchableOpacity onPress={() => setModalKota(true)}>
                        <Text style={styles.label}>{"Kota"}</Text>
                        {kotaString == "" && (
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingVertical: 10, borderBottomColor: color.border, borderBottomWidth: 1}}>
                                <Text style={{ }}>Pilih Kota Toko</Text>
                                <MaterialIcons name='keyboard-arrow-right' size={20} />
                            </View>
                        )}
                        {kotaString != "" && (
                            <Text style={{ paddingVertical: 10, borderBottomColor: color.border, borderBottomWidth: 1 }}>{kotaString}</Text>
                        )}
                    </TouchableOpacity>
                </View>
            </ScrollView>

            <Button onPress={() => simpanProfileToko()} style={{margin: 15}}>Simpan</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: color.white
    },
    input: {
        marginBottom: 15
    },
    label: {
        fontSize: AppConfig.font.size.medium,
        fontFamily: AppConfig.font.family.semiBold,
        color: color.black
    },
})